/*
 * TIndividuals.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: caduffm
 */

#include "TIndividuals.h"

void TIndividuals::addIndividual(TIndividual individual) {
	individuals.push_back(individual);
}

void TIndividuals::initializeUi(int indexIndividual, std::vector<double> u) {
	individuals.at(indexIndividual).initializeU(u);
}

void TIndividuals::addUik(int indexIndividual, double u) {
	individuals.at(indexIndividual).addUk(u);
}

void TIndividuals::setUik(int indexIndividual, int k, double u_ik) {
	individuals[indexIndividual].setUik(k, u_ik);
}

std::vector<float> TIndividuals::getXi(int indexIndividual) {
	return (individuals.at(indexIndividual).getXi());
}

float TIndividuals::getXid(int indexIndividual, int indexEnvVar) {
	return (individuals[indexIndividual].getXid(indexEnvVar));
}

std::vector<double> TIndividuals::getUi(int indexIndividual) {
	return individuals.at(indexIndividual).getUi();
}

double TIndividuals::getUik(int indexIndividual, int indexLatFac) {
	return individuals[indexIndividual].getUik(indexLatFac);
}

std::string TIndividuals::getName_i(int indexIndividual) {
	return individuals[indexIndividual].getName();
}

