/*
 * TLoci.h
 *
 *  Created on: Oct 25, 2018
 *      Author: caduffm
 */

#ifndef TLOCI_H_
#define TLOCI_H_

#include <vector>
#include <string>
#include <armadillo>
#include <math.h>

class TBeta {
private:
	long numLoci;
	int numEnvVar;
	bool theBetasInitialized;
	bool gammaInitialized;
	void initializeStorage();

	void clean();

public:
	float ** theBetas;

	unsigned short * gammas;
	std::vector<int> indexLociWhereGammaIsOne;

	arma::mat MrrAndMrs;
	double pi;
	double logPi;
	double m;

	TBeta(long, int);
	~TBeta() {
		clean();
	}
	float& operator()(int &, long &);

	// betas and gammas
	void initializeGammas(unsigned short, int);
	void initializeIndexLociGammaOne(int);
	void setOneBetadl(float, int, long);

	// hyperparameters on beta
	void setPi(double);
	void setOneMrrOrMrs(double, int, int);
	void setM(double);

	// get functions
	float getMrrOrMrs(int r, int s);
	unsigned short getGamma(int l);
};

class TLoci {
private:
	long numLoci;
	int numLatFac;
	int numEnvVar;
	bool muInitialized = false;
	bool vInitialized = false;
	bool betaInitialized = false;

	void clean();

public:
	std::vector<std::string> names;

	float * mus;
	float ** vs;
	TBeta * betas;

	// hyperparameters
	double alphaF;
	double betaF;
	double logAlphaF;
	double logBetaF;

	TLoci();
	~TLoci() {
		clean();
	}

	// number of loci
	void initializeStorage(long, int, int);

	// parameters
	void setOneMu(float, long);
	void setOneVkl(float, int, long);
	void initializeNames(std::string);

	// hyperparameters
	void setAlphaF(double);
	void setBetaF(double);
	void setLogAlphaF(double);
	void setLogBetaF(double);

	// get functions
	float getMu(int);
	std::string getName(int);
	std::vector<std::string> getNames();
	float getOneVkl(int kCounter, long locusCounter);
};

#endif /* TLOCI_H_ */
