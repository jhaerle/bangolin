/*
 * TTask.h
 *
 *  Created on: Mar 31, 2019
 *      Author: phaentu
 */

#ifndef ALLTASKS_H_
#define ALLTASKS_H_

#include "TTask.h"

//---------------------------------------------------------------------------
// all includes necessary for the application
//---------------------------------------------------------------------------
#include "TBangolinCore.h"
#include "TSimulator.h"
// #include "gperftools/profiler.h"

//---------------------------------------------------------------------------
// TTask class specific to this application (optional)
//---------------------------------------------------------------------------
class TTask_bangolin: public TTask {
public:
	TTask_bangolin() {
		_citations.push_back("Caduff et al. (2019) SOMEWHERE");
	}
	;
};

//---------------------------------------------------------------------------
// Function to create map of tasks (adjust in allTasks.cpp file)
//---------------------------------------------------------------------------
void fillTaskMaps(std::map<std::string, TTask*> & taskMap_regular,
		std::map<std::string, TTask*> & taskMap_debug);

//---------------------------------------------------------------------------
// Create a class for each task, as shown in the example below
//---------------------------------------------------------------------------
//	class TTask_NAME:public TTask{ //or derive from class specific task
//		public:
//		TTask_NAME(){ _explanation = "SAY WHAT THIS TASK IS DOING"; };

//		void run(TParameters & parameters, TLog* logfile){
//			SPECIFY HOW TO EXECUTE THIS TASK
//		};
//	};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Read groups
//---------------------------------------------------------------------------
class TTask_simulate: public TTask_bangolin {
public:
	TTask_simulate() {
		_explanation = "Simulating under Bangolin model";
	}
	;

	void run(TParameters & parameters, TLog* logfile) {
		TSimulationManager simulationManager;
		simulationManager.manageSimulations(logfile, randomGenerator,
				parameters);
	}
	;
};

class TTask_infer: public TTask_bangolin {
public:
	TTask_infer() {
		_explanation = "Inferring genotype-environment associations";
	}
	;

	void run(TParameters & parameters, TLog* logfile) {
		TBangolinCore bangolin(logfile);
		bangolin.runAnalysis(parameters, randomGenerator);
	}
	;
};

#endif /* ALLTASKS_H_ */
