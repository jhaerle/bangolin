/*
 * TBangolinCore.h
 *
 *  Created on: Oct 24, 2018
 *      Author: caduffm
 */

#include "TData.h"
#include "TMCMC.h"

#ifndef TBANGOLINCORE_H_
#define TBANGOLINCORE_H_

class TBangolinCore {
private:
	TLog* logfile;

	// main constants
	int numIndiv;
	long numLoci;
	int numEnvVar;
	int numLatFac;

	bool linkageTrue;

	// envVar names
	std::vector<std::string> envVarNames;

	// loci
	TLoci* loci;

	// individuals
	TIndividuals* individuals;

	// HMM
	THMMUpdater * hmmUpdater;

	// MCMC
	void runMCMC(TParameters& Parameters, TData * data,
			TPhredToGTLMap * phredtoGTLMap, TRandomGenerator * randomGenerator);

public:
	TBangolinCore(TLog* Logfile);
	void getData(TParameters& Parameters, TData *);
	void runAnalysis(TParameters& Parameters,
			TRandomGenerator * randomGenerator);

};

#endif /* TBANGOLINCORE_H_ */
