/*
 * TSimulator.h
 *
 *  Created on: Oct 16, 2018
 *      Author: caduffm
 */

#ifndef TSIMULATOR_H_
#define TSIMULATOR_H_

#include "stringFunctions.h"
#include "TRandomGenerator.h"
#include "TParameters.h"
#include <armadillo>
#include "utils.h"
#include "TSimulationVCF.h"
#include <numeric>
#include "commonutilities/TFile.h"
#include "THMM.h"

class TVarianceExplained {
public:
	TRandomGenerator * randomGenerator;
	TLog * logfile;

	double varianceU;
	int K;
	int NInner;
	int NOuter;
	double * gridPointsInner;
	double * gridPointsOuter;

	void quasiMonteCarloOfF(double mu, double sigma2, double & E_F,
			double & E_F2);
	void initializeGridPointsNormalDistrQuasiMC(double * gridPoints, int N,
			double mu, double sigma2);
	void clean();

	TVarianceExplained(double varU, int numLatFac, TRandomGenerator * rg,
			TParameters& Parameters, TLog * Logfile);
	~TVarianceExplained() {
		clean();
	}
	;
};

class TVarianceExplainedBeta: public TVarianceExplained {
private:
	double varianceX;
	int D;
	double * gridPointsThisZ;

	double computeSigma2_z(arma::vec & beta_l);
	double computeSigma2_yGivenZ(arma::vec v_l);
	double compute_VarExpl(double mu_0, double sigma2_yGivenZ);

public:
	TVarianceExplainedBeta(double varU, double numLatFac,
			TRandomGenerator * RandomGenerator, TParameters& Parameters,
			TLog * Logfile, double varX, int numEnvVar);
	double fillVarExplForOneLocus(double & mu_l, arma::vec & beta_l,
			arma::vec & v_l);
};

class TVarianceExplainedUV: public TVarianceExplained {
private:
	double varianceV;
	double sigma2_y;

	void initializeGridPointsBetaDistrQuasiMC(double * gridPoints, int N,
			double alpha, double beta);

public:
	TVarianceExplainedUV(double varU, double numLatFac,
			TRandomGenerator * RandomGenerator, TParameters& Parameters,
			TLog * Logfile, double varV, double alpha_f, double beta_f);
	double compute_VarExpl();
};

class TFileReader {
private:
	TLog* logfile;
protected:
	TFileReader(TLog * Logfile);
	void openFile(std::ifstream & file, TParameters & parameters,
			std::string flag);
};

class TCsvReader: public TFileReader {
private:
	TLog* logfile;
	void parseLineOfCsv(std::vector<std::string> & vec, arma::umat & Genotypes,
			std::vector<double> & tmpEnvMeasurements,
			const int & curIndividual);
public:
	TCsvReader(TLog * Logfile);
	void readCsv(TParameters & parameters, arma::umat & Genotypes,
			arma::mat & envMeasurements, int & n, int & D, int & L);
};

class TFstatReader: public TFileReader {
private:
	TLog* logfile;
	void parseLineOfFstat(std::vector<std::string> & vec,
			arma::umat & Genotypes, const int & curIndividual,
			std::vector<std::string> & popNames);

public:
	TFstatReader(TLog * Logfile);
	void readFstat(TParameters & parameters, arma::umat & Genotypes,
			arma::mat & envMeasurements, std::vector<std::string> & popNames,
			int & n, int & D, int & L);
};

class TEnvironmentReader: public TFileReader {
private:
	TLog* logfile;
	void parseLineOfEnvPatchFile(std::vector<std::string> & vec,
			std::vector<double> & envVec,
			std::vector<std::string> & whichPopsFromEnv);

public:
	TEnvironmentReader(TLog * Logfile);
	void readEnvPatchFile(TParameters & parameters, arma::mat & envMeasurements,
			std::vector<std::string> & whichPops);
};

class TSimulationManager {
public:
	void manageSimulations(TLog* Logfile, TRandomGenerator * RandomGenerator,
			TParameters & Parameters);
};

class TSimulator {
protected:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	TVarianceExplainedBeta * varianceExplainedBeta;
	TVarianceExplainedUV * varianceExplainedUV;

	// parameters to run
	bool readGenoFromFile;
	bool boolFixVarExplBeta;
	double fixedVarExplBeta;
	double meanDepth;
	double varianceX;
	double varianceU;
	double varianceV;
	double gt_error;
	int budgetNumIndivs;
	double budgetMeanDepth;

	// samples
	int n; // number of individuals
	int L; // number of loci
	int K; // number of latent components
	int D; // number of environmental variables
	std::vector<std::string> sampleNames;

	// parameters of model (fixed)
	double pi;
	double lambda_m;
	double alpha_f;
	double beta_f;

	// parameters of model (simulated)
	double m;
	arma::mat M;
	arma::mat SigmaBeta; // D x D matrix
	std::vector<double> mus;
	arma::mat Us;
	arma::mat Vs;
	std::vector<int> gammas;
	arma::mat Betas;
	std::vector<int> gammaIsOne;
	arma::mat betaIsNonZero;
	arma::mat Xs; // n x D matrix

	arma::umat Genotypes; // n x L matrix
	arma::mat Fil;

	//output
	std::string prefix;
	TSimulationVCF vcf;
	bool boolWriteSimParamFiles;

	// prepare
	void readingCommonParameters(TParameters& Parameters);
	void readBangolinSimulParameters(TParameters & Parameters);
	void simulateGenoFromFile(TParameters & Parameters);
	void simulateGenoFromBangolinModel(TParameters& Parameters);

	// simulate parameters
	void simulateAllParameters(TParameters& Parameters);
	void simulateAllUVParams(TParameters& Parameters);
	void simulateBetaParams(TParameters & Parameters);
	void simulateFixedVarExplBeta(TParameters& Parameters);
	void proposeNewBetas(arma::vec & betas, double stdDevBeta);
	void simulateSigmaBetaAndBeta(TParameters & Parameters);
	void fillRandomGammas();
	void fillFixedGammas();
	void fillSigmaBeta();
	void fillFixedSigmaBeta(double fixedVarBeta, double fixedCovarBeta);
	void fillBetas();
	void simulateMus();
	void fillUs();
	void fillVs();
	void fillXs();
	void simulateTrueGenotypes();
	void simulateEnvMeasurements();
	double computeDepth(int n1, int n2, double s1);
	int computeNumIndiv(int n1, double s1, double s2);

	// simulate error
	void simulateAndWriteObsGenotype(int);
	int pickDepth();
	void fillGenotypeLikelihood(int, int, double *);
	void writePhredGTLToVCF(double *, int);
	int countNumRefReads(double, int);

	// files
	void writeToVcfFile();
	void createSampleNames();
	void openVCF();
	void writeFiles();
	void writeAllSimParamFiles();
	void writeFileUiVl();
	void writeToHyperFile();
	void writeFileMul();
	void writeBetaGammaFile();
	void getHeaderLoci(std::vector<std::string> & header);
	void writeToEnvFile(arma::mat);
	void writeFileFil();
	void writeVarExplToFile();
	void writeFileVarExplUV();
	void clean();

public:
	TSimulator(TLog* Logfile, TRandomGenerator * RandomGenerator);
	~TSimulator() {
		clean();
	}
	;
	void runSimulations(TParameters& Parameters);
};

class TSimulatorHMM: public TSimulator, THMM {
private:
	void fillLinkedGammas();
	void simulateDistances();
	void simulateAllParameters(TParameters& Parameters);
	void simulateBetaParams(TParameters & Parameters);

public:
	TSimulatorHMM(TLog* Logfile, TRandomGenerator * RandomGenerator);
	void runSimulations(TParameters& Parameters);
};

#endif /* TSIMULATOR_H_ */
