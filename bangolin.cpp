/*
 * bangolin.cpp
 *
 *  Created on: Nov 21, 2018
 *      Author: caduffm
 */

#include "TMain.h"
#include "allTasks.h"

//---------------------------------------------------------------------------
// Function to add existing tasks
// Use main.addRegularTask to add a regular task (shown in list of available tasks)
// Use main.adddebugTask to add a debug task (not shown in list of available tasks)
//
//---------------------------------------------------------------------------
void addTaks(TMain & main) {
	main.addRegularTask("simulate", new TTask_simulate());
	main.addRegularTask("infer", new TTask_infer());
}
;

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]) {
	TMain main("Bangolin", "0.1", "https://bitbucket.org/wegmannlab/bangolin");

	//add existing tasks
	addTaks(main);

	//now run program
	return main.run(argc, argv);
}
;

