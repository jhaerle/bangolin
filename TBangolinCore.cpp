/*
 * TBangolinCore.cpp
 *
 *  Created on: Oct 24, 2018
 *      Author: caduffm
 */

#include "TBangolinCore.h"

TBangolinCore::TBangolinCore(TLog* Logfile) {
	logfile = Logfile;

	numIndiv = 0;
	numLoci = 0;
	numEnvVar = 0;
	numLatFac = 0;
	linkageTrue = false;

	individuals = nullptr;
	loci = nullptr;
	hmmUpdater = nullptr;
}

void TBangolinCore::runAnalysis(TParameters& Parameters,
		TRandomGenerator * randomGenerator) {
	TPhredToGTLMap phredtoGTLMap;
	TData data(logfile, &phredtoGTLMap);

	getData(Parameters, &data);
	runMCMC(Parameters, &data, &phredtoGTLMap, randomGenerator);
}

void TBangolinCore::getData(TParameters& Parameters, TData * data) {
	data->readDataAndInitializeParams(Parameters);

	// get main constants
	numIndiv = data->getNumIndiv();
	numLoci = data->getNumLoci();
	numEnvVar = data->getNumEnvVar();
	numLatFac = data->getNumLatFac();

	// get envVarNames
	envVarNames = data->getEnvVarNames();

	// get individuals and loci objects
	individuals = data->getIndividuals();
	loci = data->getLoci();

	linkageTrue = data->linkageTrue();
	if (linkageTrue)
		hmmUpdater = data->getHMM();
}

void TBangolinCore::runMCMC(TParameters& Parameters, TData * data,
		TPhredToGTLMap * phredtoGTLMap, TRandomGenerator * randomGenerator) {
	if (linkageTrue) {
		TMCMC mcmc(Parameters, logfile, randomGenerator,
				data->genotypePhredScores, loci, individuals, numIndiv, numLoci,
				numEnvVar, numLatFac, envVarNames, phredtoGTLMap, hmmUpdater);
		mcmc.runMCMC(Parameters);
	} else {
		TMCMC mcmc(Parameters, logfile, randomGenerator,
				data->genotypePhredScores, loci, individuals, numIndiv, numLoci,
				numEnvVar, numLatFac, envVarNames, phredtoGTLMap);
		mcmc.runMCMC(Parameters);
	}
}
