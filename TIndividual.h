/*
 * TIndividual.h
 *
 *  Created on: Oct 23, 2018
 *      Author: caduffm
 */

#ifndef TINDIVIDUAL_H_
#define TINDIVIDUAL_H_

#include <vector>
#include <string>
#include <iostream>

class TIndividual {
private:
	std::vector<float> x;
	std::vector<double> u;
	std::string name;

public:
	TIndividual(std::string, std::vector<float>);
	void initializeU(std::vector<double>);
	void addUk(double u_k);
	void setUik(int, double);

	std::vector<float> getXi();
	float getXid(int);
	std::vector<double> getUi();
	double getUik(int);
	std::string getName();
};

#endif /* TINDIVIDUAL_H_ */
