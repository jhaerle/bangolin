/*
 * TBangolinTest.cpp
 *
 *  Created on: Nov 21, 2018
 *      Author: caduffm
 */

#include "TBangolinTest.h"

//------------------------------------------
//TBangolinTest
//------------------------------------------
TBangolinTest::TBangolinTest(TParameters & params, TLog* Logfile) {
	logfile = Logfile;
	_name = "empty";
	_testingPrefix = params.getParameterStringWithDefault("prefix",
			"Bangolin_testing_");
	logfile->list(
			"Will use prefix '" + _testingPrefix + "' for all testing files.");
}
;

bool TBangolinTest::runTGenomeFromInputfile(std::string task) {
	logfile->startIndent("Running task '" + task + "':");
	_testParams.addParameter("task", task);
	_testParams.addParameter("verbose", "");

	//open task switcher and run task
	TTaskList taskList;
	bool returnVal = true;
	try {
		taskList.run(task, _testParams, logfile);
	} catch (std::string & error) {
		logfile->conclude(error);
		returnVal = false;
	} catch (const char* error) {
		logfile->conclude(error);
		returnVal = false;
	} catch (std::exception & error) {
		logfile->conclude(error.what());
		returnVal = false;
	} catch (...) {
		logfile->conclude("unhandeled error!");
		returnVal = false;
	}
	logfile->endIndent();
	return returnVal;
}
;

// add the test functions here (as in TAtlasTest)

