/*
 * TIndividual.cpp
 *
 *  Created on: Oct 23, 2018
 *      Author: caduffm
 */

#include "TIndividual.h"

TIndividual::TIndividual(std::string name_i, std::vector<float> x_i) {
	name = name_i;
	x = x_i;
}

void TIndividual::initializeU(std::vector<double> us) {
	u = us;
}

void TIndividual::setUik(int k, double u_ik) {
	u[k] = u_ik;
}

void TIndividual::addUk(double u_k) {
	u.push_back(u_k);
}

std::vector<float> TIndividual::getXi() {
	return x;
}

float TIndividual::getXid(int d) {
	return x[d];
}

std::vector<double> TIndividual::getUi() {
	return u;
}

double TIndividual::getUik(int k) {
	return u[k];
}

std::string TIndividual::getName() {
	return name;
}

