/*
 * THMM.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: caduffm
 */

#include "THMM.h"

THMM::THMM() {
	maxIndex = 24;
	m_Lambda_1 = 0;
	m_Lambda_2 = 0;
	m_tau = nullptr;
	numLoci = 0;
}

void THMM::setLambda(double lambda_1, double lambda_2) {
	m_Lambda_1 = lambda_1;
	m_Lambda_2 = lambda_2;
}

void THMM::enterOneDelta_l(std::string chr_previous_l, std::string chr_this_l,
		unsigned int pos_previous_l, unsigned int pos_this_l) {
	// ok, debugged!
	if (chr_previous_l != chr_this_l) { // new chromosome
		m_index_delta.push_back(maxIndex); // last index in transition probability matrix
	} else {
		double delta_l = pos_this_l - pos_previous_l;
		int index = floor(log2(delta_l));
		if (index > (maxIndex - 1))
			index = maxIndex - 1; // cutoff at maxIndexMinOne
		m_index_delta.push_back(index); // index in transition probability matrix
	}
}

void THMM::setNumLoci(int L) { // ok
	numLoci = L;
}

void THMM::initializeTransProb() { // ok
	// build m_tau
	m_tau = new double **[maxIndex + 1];
	for (int index_delta_l = 0; index_delta_l <= maxIndex; index_delta_l++) {
		m_tau[index_delta_l] = new double *[2];
		for (int gamma_l_minOne = 0; gamma_l_minOne < 2; gamma_l_minOne++) {
			m_tau[index_delta_l][gamma_l_minOne] = new double[2];
		}
	}

	// initialize m_tau (= the current tau)
	fillTau(m_Lambda_1, m_Lambda_2, m_tau);
}

void THMM::initializeExpLambda(const double lambda_1, const double lambda_2,
		double *** tau) { // same as in R!
	double ePowerLambda1Lambda2 = exp(-lambda_1 - lambda_2);
	double div = 1.0 / (lambda_1 + lambda_2);
	tau[0][0][0] = div * (lambda_1 * ePowerLambda1Lambda2 + lambda_2);
	tau[0][0][1] = div * (-lambda_1 * ePowerLambda1Lambda2 + lambda_1);
	tau[0][1][0] = div * (-lambda_2 * ePowerLambda1Lambda2 + lambda_2);
	tau[0][1][1] = div * (lambda_2 * ePowerLambda1Lambda2 + lambda_1);
}

void THMM::fillTau(const double lambda_1, const double lambda_2,
		double *** tau) {
	initializeExpLambda(lambda_1, lambda_2, tau);

	for (int index_delta_l = 1; index_delta_l < maxIndex; index_delta_l++) { // same as in R
		tau[index_delta_l][0][0] = tau[index_delta_l - 1][0][0]
				* tau[index_delta_l - 1][0][0]
				+ tau[index_delta_l - 1][0][1] * tau[index_delta_l - 1][1][0];
		tau[index_delta_l][0][1] = tau[index_delta_l - 1][0][0]
				* tau[index_delta_l - 1][0][1]
				+ tau[index_delta_l - 1][0][1] * tau[index_delta_l - 1][1][1];
		tau[index_delta_l][1][0] = tau[index_delta_l - 1][1][0]
				* tau[index_delta_l - 1][0][0]
				+ tau[index_delta_l - 1][1][1] * tau[index_delta_l - 1][1][0];
		tau[index_delta_l][1][1] = tau[index_delta_l - 1][1][0]
				* tau[index_delta_l - 1][0][1]
				+ tau[index_delta_l - 1][1][1] * tau[index_delta_l - 1][1][1];
	}
	double pGamma0 = fillStationary(lambda_1, lambda_2); // I think this is ok, 80% sure
	tau[maxIndex][0][0] = pGamma0;
	tau[maxIndex][0][1] = 1.0 - pGamma0;
	tau[maxIndex][1][0] = pGamma0;
	tau[maxIndex][1][1] = 1.0 - pGamma0;
}

double THMM::fillStationary(const double lambda_1, const double lambda_2) {
	double ePowerLambda1Lambda2 = exp(-lambda_1 - lambda_2);
	return (lambda_2 * ePowerLambda1Lambda2 - lambda_2)
			/ (ePowerLambda1Lambda2 * (lambda_1 + lambda_2) - lambda_1
					- lambda_2);
}

double THMM::getTau(int l, int value_first_gamma, int value_second_gamma) {
	if (l >= numLoci)
		return 1;
	else
		return m_tau[m_index_delta[l]][value_first_gamma][value_second_gamma];
}

double THMM::getLambda1() {
	return m_Lambda_1;
}
double THMM::getLambda2() {
	return m_Lambda_2;
}
double THMM::getIndexDelta_l(int l) {
	return m_index_delta[l];
}

THMM::~THMM() {
	// delete m_tau
	if (m_tau != nullptr) {
		for (int index_delta_l = 0; index_delta_l <= maxIndex;
				index_delta_l++) {
			for (int gamma_l_minOne = 0; gamma_l_minOne < 2; gamma_l_minOne++) {
				delete[] m_tau[index_delta_l][gamma_l_minOne];
			}
			delete[] m_tau[index_delta_l];
		}
		delete[] m_tau;
	}
}

void THMM::printTau() {
	std::cout << "Tau: " << std::endl;
	for (int index_delta_l = 0; index_delta_l <= maxIndex; index_delta_l++) {
		for (int gamma_l_minOne = 0; gamma_l_minOne < 2; gamma_l_minOne++) {
			for (int gamma_l = 0; gamma_l < 2; gamma_l++) {
				std::cout << m_tau[index_delta_l][gamma_l_minOne][gamma_l]
						<< "    ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		std::cout << std::endl;
	}
}

void THMM::printIndexDelta() {
	for (std::vector<u_int8_t>::iterator it = m_index_delta.begin();
			it != m_index_delta.end(); it++) {
		std::cout << unsigned(*it) << "   ";
	}
	std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////////////////
// THMM updater 																//
//////////////////////////////////////////////////////////////////////////////////

THMMUpdater::THMMUpdater() :
		THMM() {
	initializeNewTau();
}

void THMMUpdater::initializeNewTau() { // ok
	// build m_new_tau
	m_new_tau = new double **[maxIndex + 1];
	for (int index_delta_l = 0; index_delta_l <= maxIndex; index_delta_l++) {
		m_new_tau[index_delta_l] = new double *[2];
		for (int gamma_l_minOne = 0; gamma_l_minOne < 2; gamma_l_minOne++) {
			m_new_tau[index_delta_l][gamma_l_minOne] = new double[2];
		}
	}
}

THMMUpdater::~THMMUpdater() {
	// delete m_new_tau
	if (m_tau != nullptr) {
		for (int index_delta_l = 0; index_delta_l <= maxIndex;
				index_delta_l++) {
			for (int gamma_l_minOne = 0; gamma_l_minOne < 2; gamma_l_minOne++) {
				delete[] m_tau[index_delta_l][gamma_l_minOne];
			}
			delete[] m_tau[index_delta_l];
		}
		delete[] m_tau;
	}
}

double THMMUpdater::getNewTau(int l, int value_first_gamma,
		int value_second_gamma) {
	if (l >= numLoci)
		return 1;
	else
		return m_new_tau[m_index_delta[l]][value_first_gamma][value_second_gamma];
}

void THMMUpdater::updateTransProbWithNewLambda1(double lambda_1) { // ok
	fillTau(lambda_1, m_Lambda_2, m_new_tau);
}

void THMMUpdater::updateTransProbWithNewLambda2(double lambda_2) { // ok
	// fill m_new_tau
	fillTau(m_Lambda_1, lambda_2, m_new_tau);
}

void THMMUpdater::acceptNewLambda1(double lambda_1) {
	m_Lambda_1 = lambda_1;
	std::swap(m_new_tau, m_tau); // update tau
	// printTau();
}

void THMMUpdater::acceptNewLambda2(double lambda_2) {
	m_Lambda_2 = lambda_2;
	std::swap(m_new_tau, m_tau); // update tau
	// printTau();
}

void THMMUpdater::printNewTau() {
	std::cout << "New Tau: " << std::endl;
	for (int index_delta_l = 0; index_delta_l <= maxIndex; index_delta_l++) {
		for (int gamma_l_minOne = 0; gamma_l_minOne < 2; gamma_l_minOne++) {
			for (int gamma_l = 0; gamma_l < 2; gamma_l++) {
				std::cout << m_new_tau[index_delta_l][gamma_l_minOne][gamma_l]
						<< "    ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		std::cout << std::endl;
	}
}

// TODO: meaningful values to initialize lambda_1 and lambda_2?
// TODO: with current values of lambda_1 and lambda_2, there is no difference in Tau between different delta's
