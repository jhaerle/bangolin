/*
 * TMCMC.cpp
 *
 *  Created on: Oct 26, 2018
 *      Author: caduffm
 */

#include "TMCMC.h"

TMCMC::TMCMC(TParameters& Parameters, TLog* Logfile,
		TRandomGenerator * RandomGenerator,
		std::vector<unsigned short *> &phredScores, TLoci* locus_pointer,
		TIndividuals* individual_pointer, int n, int L, int D, int K,
		std::vector<std::string> &dnames, TPhredToGTLMap * PhredToGTLMap) {

	hmmUpdater = nullptr;
	linkageTrue = false;
	init(Parameters, Logfile, RandomGenerator, phredScores, locus_pointer,
			individual_pointer, n, L, D, K, dnames, PhredToGTLMap);
}

// overloaded constructor (for HMM)
TMCMC::TMCMC(TParameters& Parameters, TLog* Logfile,
		TRandomGenerator * RandomGenerator,
		std::vector<unsigned short *> &phredScores, TLoci* locus_pointer,
		TIndividuals* individual_pointer, int n, int L, int D, int K,
		std::vector<std::string> &dnames, TPhredToGTLMap * PhredToGTLMap,
		THMMUpdater * HmmUpdater) {
	hmmUpdater = HmmUpdater;
	linkageTrue = true;
	init(Parameters, Logfile, RandomGenerator, phredScores, locus_pointer,
			individual_pointer, n, L, D, K, dnames, PhredToGTLMap);
}

void TMCMC::init(TParameters& Parameters, TLog* Logfile,
		TRandomGenerator * RandomGenerator,
		std::vector<unsigned short *> &phredScores, TLoci* locus_pointer,
		TIndividuals* individual_pointer, int n, int L, int D, int K,
		std::vector<std::string> &dnames, TPhredToGTLMap * PhredToGTLMap) {

	logfile = Logfile;
	genotypePhredScores = phredScores;
	randomGenerator = RandomGenerator;
	phredToGTLMap = PhredToGTLMap;
	weights = nullptr;
	logisticLookup = nullptr;
	numIndiv = n;
	individuals = individual_pointer;
	numLoci = L;
	loci = locus_pointer;
	initialNumLociWithGamma1 = 0;
	numEnvVar = D;
	envVarNames = dnames;
	numLatFac = K;
	minAlleleFreq = 0;
	oneMinusminAlleleFreq = 0;
	numIterations = 0;
	numBurnins = 0;
	burnin = 0;
	numBetaUpdatesPerIter = 0;
	numAcceptedFPerLocus = nullptr;
	numAcceptedAlphaF = 0;
	numAcceptedBetaF = 0;
	numAcceptedPi = 0;
	numAcceptedLambda1 = 0;
	numAcceptedLambda2 = 0;
	numAcceptedVkl = 0;
	numAcceptedUk = 0;
	numAcceptedLittleM = 0;
	numAcceptedMrr = 0;
	numAcceptedMrs = 0;
	numAcceptedBetaPerLD = nullptr;
	totalNumUpdatesBetaPerLocus = nullptr;
	numAcceptedGamma = 0;
	gammaTotalNumTrialsCounter = 0;
	betaTotalNumTrialsCounter = 0;
	widthProposalKernelF = 0;
	widthProposalKernelLogAlphaF = 0;
	widthProposalKernelLogBetaF = 0;
	widthProposalKernelPi = 0;
	widthProposalKernelLambda1 = 0;
	widthProposalKernelLambda2 = 0;
	widthProposalKernelVkl = 0;
	widthProposalKernelUk = 0;
	widthProposalKernelLittleM = 0;
	widthProposalKernelMrr = 0;
	widthProposalKernelMrs = 0;
	widthProposalKernelBetadl = nullptr;
	widthSdProposalKernelBetaL = 0;
	widthVarProposalKernelBetaL = 0;
	qOfgamma1Togamma0 = 0;
	logqOfgamma1Togamma0 = 0;
	lambdaM = 0;
	lambda_for_pi = 0;
	logit_f_il = nullptr;
	expValBetadl = nullptr;
	expValSquaredBetadl = nullptr;
	betaUpdateCounter = nullptr;
	gammaSum = nullptr;
	gammaCounter = nullptr;
	storageBetasInitialized = false;
	storageMusInitialized = false;
	expValMul = nullptr;
	expValSquaredMul = nullptr;
	sumV = nullptr;
	sumU = nullptr;
	piSum = 0;
	betaFSum = 0;
	lambda_1_sum = 0;
	lambda_2_sum = 0;
	alphaFSum = 0;
	ll = nullptr;
	posterior = nullptr;
	fixU = false;
	fixBetaGammaToZero = false;
	fixV = false;

	if (weights == nullptr)
		weights = new TWeights(randomGenerator, logfile, numLoci);
	if (logisticLookup == nullptr)
		logisticLookup = new TLogisticLookup();
}

void TMCMC::clean() {
	if (weights != nullptr)
		delete weights;
	if (logisticLookup != nullptr)
		delete logisticLookup;
	if (storageBetasInitialized) {
		for (int d = 0; d < numEnvVar; d++) {
			delete[] expValBetadl[d];
			delete[] expValSquaredBetadl[d];
			delete[] betaUpdateCounter[d];
		}
		delete[] expValBetadl;
		delete[] expValSquaredBetadl;
		delete[] betaUpdateCounter;
		delete[] gammaCounter;
		delete[] gammaSum;
	}
	if (logit_f_il != nullptr) {
		for (int i = 0; i < numIndiv; i++) {
			delete[] logit_f_il[i];
		}
		delete[] logit_f_il;
	}
	if (storageMusInitialized) {
		delete[] expValMul;
		delete[] expValSquaredMul;
	}
	if (numAcceptedBetaPerLD != nullptr) {
		for (int d = 0; d < numEnvVar; d++)
			delete[] numAcceptedBetaPerLD[d];
		delete[] numAcceptedBetaPerLD;
	}
	if (totalNumUpdatesBetaPerLocus != nullptr) {
		for (int d = 0; d < numEnvVar; d++)
			delete[] totalNumUpdatesBetaPerLocus[d];
		delete[] totalNumUpdatesBetaPerLocus;
	}
	if (widthProposalKernelBetadl != nullptr) {
		for (int d = 0; d < numEnvVar; d++)
			delete[] widthProposalKernelBetadl[d];
		delete[] widthProposalKernelBetadl;
	}
	if (files.writeUVFinal && sumU != nullptr && sumV != nullptr) {
		for (int k = 0; k < numLatFac; k++) {
			delete[] sumU[k];
			delete[] sumV[k];
		}
		delete[] sumU;
		delete[] sumV;
	}
	if (numAcceptedFPerLocus != nullptr)
		delete[] numAcceptedFPerLocus;
	if (widthProposalKernelF != nullptr)
		delete[] widthProposalKernelF;
	if (ll != nullptr)
		delete[] ll;
	if (posterior != nullptr)
		delete[] posterior;
}

void TMCMC::runMCMC(TParameters& Parameters) {
	initializeParameters(Parameters);
	weights->calculateCumulativeWeights(Parameters);
	prepareMCMC(Parameters);
	coordinateMCMC(Parameters);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize parameter estimates                                                               //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TMCMC::initializeParameters(TParameters& Parameters) {
	logfile->startIndent("Initializing parameters...");

	arma::mat U(numIndiv, numLatFac, arma::fill::zeros);

	if (Parameters.parameterExists("u")) {
		std::string usfile = Parameters.getParameterString("u");
		initializeUFromFile(U, usfile);
	} else
		initializeU(U);
	initializeBetaVGamma(U, Parameters);
	if (linkageTrue)
		initializeLambda();
	initializeHyperparams();
}

void TMCMC::initializeUFromFile(arma::mat &U, std::string usfile) {
	std::ifstream us(usfile.c_str());
	if (!us)
		throw "Failed to open file '" + usfile + "!";

	//parse file
	std::vector<std::string> vec;
	int i = 0;
	while (us.good() && !us.eof()) {
		fillVectorFromLineWhiteSpaceSkipEmpty(us, vec);
		if (!vec.empty()) {
			std::vector<double> doubleVec;
			std::transform(vec.begin(), vec.end(),
					std::back_inserter(doubleVec), stringToDouble);
			individuals->initializeUi(i, doubleVec);
			for (int k = 0; k < std::min(numLatFac, (int) doubleVec.size());
					k++)
				U(i, k) = doubleVec[k];
			i++;
		}
	}
	// fill rest of latent factors with random values
	for (int k = 2; k < numLatFac; k++) {
		for (int i = 0; i < numIndiv; i++) {
			U(i, k) = U(i, k - 1) + randomGenerator->getNormalRandom(0, 0.1); // add noise
		}
	}
	// make vector orthonormal
	double u_tmp_array[numIndiv];
	for (int k = 0; k < numLatFac; k++) {
		for (int i = 0; i < numIndiv; i++) {
			u_tmp_array[i] = U(i, k);
		}
		gramSchmidt(k, u_tmp_array, k);
		for (int i = 0; i < numIndiv; i++) {
			if (k < 2)
				individuals->setUik(i, k, u_tmp_array[i]);
			else
				individuals->addUik(i, u_tmp_array[i]);
			U(i, k) = u_tmp_array[i];
		}
	}
	// std::cout << U << std::endl;
	assertThatVectorsAreOrthogonal();
}

void TMCMC::initializeU(arma::mat &U) {
	calculateInitialU(U);
	for (int individual = 0; individual < numIndiv; individual++) {
		std::vector<double> ui = arma::conv_to<std::vector<double>>::from(
				U.row(individual));
		individuals->initializeUi(individual, ui);
	}
}

void TMCMC::calculateInitialU(arma::mat & U) {
	logfile->startIndent("Initializing values for u_i...");
	arma::mat Exy(numIndiv, numIndiv, arma::fill::zeros); // matrix with expected values
	arma::vec Ex(numIndiv, arma::fill::zeros); // vector with expected values
	arma::mat S(numIndiv, numIndiv, arma::fill::zeros); // variance-covariance matrix
	arma::fvec f_il(numIndiv);

	double logit0025 = logit(0.025);
	double logit0975 = logit(0.975);
	bool initializeMu = false;
	int counter = 0;
	for (std::vector<unsigned short *>::iterator locus =
			genotypePhredScores.begin(); locus < genotypePhredScores.end();
			locus++, counter++) {
		fillFil(f_il, locus, logit0025, logit0975, initializeMu, counter);
		addToExyAndEx(f_il, Exy, Ex, numIndiv);
	}

	// compute variance-covariance matrix S
	Exy /= (double) numLoci;
	Ex /= (double) numLoci;
	fillVarCovarMat(S, Exy, Ex, numIndiv);

	// perform eigen decomposition
	arma::vec eigval;
	arma::mat eigvec;
	eig_sym(eigval, eigvec, S); // the eigenvalues are in ascending order, eigenvectors are already normalized

	// get u's from eigenvectors and eigenvalues
	counter = 0;
	for (int latComponent = numIndiv - 1; latComponent >= numIndiv - numLatFac;
			latComponent--, counter++) { // go backwards
		U.col(counter) = eigvec.col(latComponent);
	}

	logfile->endIndent();
}

void TMCMC::addToExyAndEx(arma::fvec & vals, arma::mat &Exy, arma::mat &Ex,
		int dimension) {
	for (int i = 0; i < dimension; i++) {
		Exy(i, i) += vals(i) * vals(i);
		Ex(i) += vals(i);
		for (int j = i + 1; j < dimension; j++) {
			Exy(i, j) += vals(i) * vals(j);
		}
	}
}

void TMCMC::fillVarCovarMat(arma::mat &S, arma::mat &Exy, arma::vec &Ex,
		int dimension) {
	for (int i = 0; i < dimension; i++) {
		S(i, i) = Exy(i, i) - Ex(i) * Ex(i);
		for (int j = i + 1; j < dimension; j++) {
			S(i, j) = Exy(i, j) - Ex(i) * Ex(j);
			S(j, i) = S(i, j);
		}
	}
}

void TMCMC::fillFil(arma::fvec & f_il,
		std::vector<unsigned short *>::iterator locus, double logit0025,
		double logit0975, bool initializeMu, int locusCounter) {
	int individual = 0;
	for (int phredScore = 0; phredScore < 3 * numIndiv;
			phredScore += 3, individual++) {
		// 1) find mean posterior estimate for the allele frequency (= f_il) in logit-scale
		f_il(individual) = meanPosteriorGenotype(&(*locus)[phredScore]) / 2.0;
		if (f_il(individual) < 0.025) {
			f_il(individual) = logit0025;
		} else if (f_il(individual) > 0.975) {
			f_il(individual) = logit0975;
		} else
			f_il(individual) = logit(f_il(individual));
	}

	// compute mu_l (assuming beta_l*x_i = 0 and u_i * v_l = 0)
	float mu_l = sum(f_il) / (double) numIndiv;

	// store mu only once (as fillFil is called twice)
	if (initializeMu) {
		loci->setOneMu(mu_l, locusCounter);
	}

	// standardize y_il
	f_il -= mu_l;
}

void TMCMC::initializeBetaVGamma(arma::mat & U, TParameters& Parameters) {
	logfile->startIndent(
			"Initializing values for mu_l, v_l, gamma_l and beta_l...");

	arma::mat unrestrictedWtWiWt;
	arma::mat restrictedWtWiWt;

	// fill restrictedWtWiWt, unrestrictedWtWiWt and compute F-statistic for every locus
	compareResToUnresModel(U, restrictedWtWiWt, unrestrictedWtWiWt);

	// select loci where gamma = 1
	arma::uvec indicesLociWithGammaOne;
	selectLociWhereGammaIsOne(indicesLociWithGammaOne, Parameters);

	// initialize
	double logit0025 = logit(0.025);
	double logit0975 = logit(0.975);
	long locusCounter = 0;
	bool initializeMu = false;
	arma::fvec f_il(numIndiv);

	// loop over all phred scores again
	for (std::vector<unsigned short *>::iterator locus =
			genotypePhredScores.begin(); locus < genotypePhredScores.end();
			locus++, locusCounter++) {

		fillFil(f_il, locus, logit0025, logit0975, initializeMu, locusCounter);

		if (arma::any(indicesLociWithGammaOne == locusCounter)) { // if the locus has gamma one
			// initialize gamma
			loci->betas->initializeGammas(1, locusCounter);
			loci->betas->initializeIndexLociGammaOne(locusCounter);

			// initialize beta and V
			arma::vec betaV = unrestrictedWtWiWt * f_il;
			unsigned int elementInV = 0;
			for (int element = 0; element < static_cast<int>(betaV.size());
					element++) { // seperate beta's from v's
				if (element < numEnvVar) {
					loci->betas->setOneBetadl(betaV(element), element,
							locusCounter);
				} else {
					loci->setOneVkl(betaV(element), elementInV, locusCounter);
					elementInV++;
				}
			}
		} else {
			// initialize gamma
			(loci->betas)->initializeGammas(0, locusCounter);

			// initialize beta and V
			arma::vec V = restrictedWtWiWt * f_il;
			unsigned int elementInBeta = 0;
			for (unsigned int element = 0; element < V.size() + numEnvVar;
					element++) { // fill v's
				if (element < V.size()) {
					loci->setOneVkl(V(element), element, locusCounter);
				} else {
					loci->betas->setOneBetadl(0, elementInBeta, locusCounter);
					elementInBeta++;
				}
			}
		}
	}

	if (Parameters.parameterExists("v")) {
		std::string vsfile = Parameters.getParameterString("v");
		setVFromFile(Parameters, vsfile);
	}
}

void TMCMC::setVFromFile(TParameters& Parameters, std::string vsfile) {
	std::ifstream vs(vsfile.c_str());
	if (!vs)
		throw "Failed to open file '" + vsfile + "!";

	//parse file
	std::vector<std::string> vec;
	int l = 0;
	while (vs.good() && !vs.eof()) {
		fillVectorFromLineWhiteSpaceSkipEmpty(vs, vec);
		if (!vec.empty()) {
			std::vector<double> doubleVec;
			std::transform(vec.begin(), vec.end(),
					std::back_inserter(doubleVec), stringToDouble);
			for (int k = 0; k < numLatFac; k++)
				loci->setOneVkl(doubleVec[k], k, l);
			l++;
		}
	}
	for (int l = 0; l < numLoci; l++) {
		for (int k = 0; k < numLatFac; k++)
			std::cout << loci->vs[k][l] << "  ";
		std::cout << std::endl;
	}
}

void TMCMC::countTransitions(arma::mat & tau, double & counter) {
	// TODO: this approach is only valid if delta_l = 1! -> exclude all other loci?
	// count the transitions
	for (int l = 1; l < numLoci; l++) {
		if (hmmUpdater->getIndexDelta_l(l) == 0) { // only if delta_l = 1
			counter++;
			if (loci->betas->gammas[l - 1] == 0) { // l-1 = 0
				if (loci->betas->gammas[l] == 0)
					tau(0, 0) ++;else
					tau(0, 1) ++;}
			else {								// l-1 = 1
				if (loci->betas->gammas[l] == 0)
					tau(1, 0) ++;else
					tau(1, 1) ++;}
			}
		}
	}

void TMCMC::initializeLambda() {
	arma::mat tau = arma::zeros(2, 2);
	double counter = 0;
	countTransitions(tau, counter);
	tau(0, 0) /= counter;
	tau(1, 0) /= counter;
	tau(0, 1) /= counter;
	tau(1, 1) /= counter;

	double lambda_1, lambda_2;

	if (tau(0, 0) + tau(1, 1) > 1) { // avoid problems with log
		lambda_1 = ((tau(0, 0) - 1.) * log(tau(0, 0) + tau(1, 1) - 1.))
				/ (2. - tau(0, 0) - tau(1, 1)); // ok, same as in R!
		lambda_2 = ((tau(1, 1) - 1.) * log(tau(0, 0) + tau(1, 1) - 1.))
				/ (2. - tau(0, 0) - tau(1, 1)); // ok, same as in R!
	} else {
		lambda_1 = 0.05; // TODO: ok? stationary prob of Pi1 is then approx 0.01 (=initial pi)
		lambda_2 = 8;
	}

	hmmUpdater->setLambda(lambda_1, lambda_2);
	hmmUpdater->initializeTransProb();
}

void TMCMC::compareResToUnresModel(arma::mat & U, arma::mat &restrictedWtWiWt,
		arma::mat & unrestrictedWtWiWt) {
	// fill matrix X
	arma::mat X;
	fillXFromIndividuals(X);

	// restricted model
	arma::mat restrictedWt = U.t();
	arma::mat tmp1;
	try {
		tmp1 = (restrictedWt * U).i();
	} catch (const std::runtime_error &e) {
		for (long l = 0; l < numLoci; l++) {
			weights->ranks = 1.0;
		}
		logfile->warning("Could not take the inverse of matrix U'U.");
		logfile->list("All loci where initialized with weight one.");
		return;
	}
	restrictedWtWiWt = tmp1 * restrictedWt;

	// unrestricted model
	arma::mat unrestrictedW = join_rows(X, U);
	arma::mat unrestrictedWt = unrestrictedW.t();
	arma::mat tmp2;
	try {
		tmp2 = (unrestrictedWt * unrestrictedW).i();
	} catch (const std::runtime_error &e) {
		for (long l = 0; l < numLoci; l++) {
			weights->ranks = 1.0;
		}
		logfile->warning("Could not take the inverse of matrix (X|U)'(X|U).");
		logfile->list("All loci where initialized with weight one.");
		return;
	}
	unrestrictedWtWiWt = tmp2 * unrestrictedWt;

	float df1 = numEnvVar;
	float df2 = numIndiv - (numEnvVar + numLatFac);

	arma::fvec f_il(numIndiv);

	double logit0025 = logit(0.025);
	double logit0975 = logit(0.975);

	// now: loop over every locus again
	long locusCounter = 0;
	bool initializeMu = true;
	weights->FVals.set_size(numLoci);
	for (std::vector<unsigned short *>::iterator locus =
			genotypePhredScores.begin(); locus < genotypePhredScores.end();
			locus++, locusCounter++) {
		fillFil(f_il, locus, logit0025, logit0975, initializeMu, locusCounter);
		// compute F statistics to compare the restricted vs unrestricted model
		float F = computeF(restrictedWtWiWt, unrestrictedWtWiWt, U,
				unrestrictedW, f_il, df1, df2);
		weights->FVals(locusCounter) = F;
	}
	weights->ranks = arma::sort_index(weights->FVals, "descend"); // sort by F-value (the larger F -> the more significant -> the more in the front of "ranks" the index of the element will be)
}

void TMCMC::fillXFromIndividuals(arma::mat &X) {
	for (int i = 0; i < numIndiv; i++) {
		X.insert_rows(i,
				arma::conv_to<arma::rowvec>::from(individuals->getXi(i)));
	}
}

float TMCMC::meanPosteriorGenotype(unsigned short * phredScores) {
	float denumerator = ((*phredToGTLMap)[phredScores[0]]
			+ (*phredToGTLMap)[phredScores[1]]
			+ (*phredToGTLMap)[phredScores[2]]);
	return (*phredToGTLMap)[phredScores[1]] / denumerator
			+ 2.0 * (*phredToGTLMap)[phredScores[2]] / denumerator;
}

float TMCMC::computeF(arma::mat & restrictedWtWiWt,
		arma::mat & unrestrictedWtWiWt, arma::mat & U,
		arma::mat & unrestrictedW, arma::fvec & y_l, float df1, float df2) {
	float SSRr = arma::sum(arma::square(y_l - U * restrictedWtWiWt * y_l));
	float SSRu = arma::sum(
			arma::square(y_l - unrestrictedW * unrestrictedWtWiWt * y_l));
	if (SSRu == 0 || df1 <= 0 || df2 <= 0) {
		return 0.0;
	} else
		return ((SSRr - SSRu) / df1) / (SSRu / df2);
}

void TMCMC::selectLociWhereGammaIsOne(arma::uvec &indicesLociWithGammaOne,
		TParameters& Parameters) {
	loci->betas->setPi(0.99999); // initialize pi to almost 1 (for log not exactly 1)
	initialNumLociWithGamma1 = numLoci;
	indicesLociWithGammaOne = weights->ranks.head(initialNumLociWithGamma1); // initialize all gamma to 1
}

void TMCMC::initializeGammaAndPiInsideBurnin(TParameters& Parameters) {
	float pi = Parameters.getParameterDoubleWithDefault("initialPi", 0.01);
	loci->betas->setPi(pi);
	initialNumLociWithGamma1 = (int) numLoci * loci->betas->pi;
	arma::uvec indicesLociWithGammaOne = weights->ranks.head(
			initialNumLociWithGamma1); // only keep first pi percent elements

	loci->betas->indexLociWhereGammaIsOne.clear(); // first clear (is a vector)
	for (int l = 0; l < numLoci; l++) {
		if (arma::any(indicesLociWithGammaOne == l)) { // if the locus has gamma one
			// initialize gamma, leave beta as it is
			loci->betas->initializeGammas(1, l);
			loci->betas->initializeIndexLociGammaOne(l);
		} else { // set gamma and beta to zero and update logit_f_il
			loci->betas->initializeGammas(0, l);
			for (int d = 0; d < numEnvVar; d++) {
				for (int i = 0; i < numIndiv; i++)
					logit_f_il[i][l] -= loci->betas->theBetas[d][l]
							* individuals->getXid(i, d);
				loci->betas->theBetas[d][l] = 0;
			}
		}
	}

	logfile->list(
			"A total number of " + toString(initialNumLociWithGamma1)
					+ " loci were initialized with gamma = 1 in the middle of the burnins.");
}

void TMCMC::initializeHyperparams() {
	logfile->startIndent("Initializing hyperparameters...");
	initializeAlphaFBetaF();
	initializeMrrMrsAndM();
	logfile->endIndent();
}

void TMCMC::initializeAlphaFBetaF() {
	// estimate alpha_f and beta_f by method of moments
	double mean = 0;
	double sumXSquare = 0;
	float alleleFreq;

	for (int locus = 0; locus < numLoci; locus++) {
		alleleFreq = logistic(loci->getMu(locus));
		mean += alleleFreq;
		sumXSquare += alleleFreq * alleleFreq;

	}
	mean /= (double) numLoci;
	sumXSquare /= (double) numLoci;
	double var = sumXSquare - mean * mean;

	//now estimate alpha and beta
	double tmp = ((mean * (1.0 - mean)) / var) - 1.0;
	double alphaF = mean * tmp;
	if (alphaF < 0.0)
		alphaF = 0.01;
	loci->setAlphaF(alphaF);
	loci->setLogAlphaF(log(alphaF));

	double betaF = (1.0 - mean) * tmp;
	if (betaF < 0.0)
		betaF = 0.01;
	loci->setBetaF(betaF);
	loci->setLogBetaF(log(betaF));

	logfile->list("Initialized alpha_f and beta_f.");
}

void TMCMC::computeSigmaBeta(arma::mat &SigmaBeta) {
	// calculate variance-covariance matrix SigmaBeta from all beta where gamma = 1
	arma::mat Exy(numEnvVar, numEnvVar, arma::fill::zeros);
	arma::vec Ex(numEnvVar, arma::fill::zeros);
	arma::fvec betasFromOneLocus(numEnvVar);

	for (long l = 0; l < numLoci; l++) {
		if (loci->betas->gammas[l] == 1) { // only add if gamma = 1
			for (int d = 0; d < numEnvVar; d++) {
				betasFromOneLocus(d) = loci->betas->theBetas[d][l];
			}
			addToExyAndEx(betasFromOneLocus, Exy, Ex, numEnvVar);
		}
	}

	Exy /= (double) initialNumLociWithGamma1;
	Ex /= (double) initialNumLociWithGamma1;
	fillVarCovarMat(SigmaBeta, Exy, Ex, numEnvVar);
}

void TMCMC::computeM(arma::mat &M) {
	// first compute SigmaBeta (variance-covariance matrix of all beta where gamma=1)
	arma::mat SigmaBeta(numEnvVar, numEnvVar, arma::fill::zeros);
	computeSigmaBeta(SigmaBeta);
	// take inverse
	arma::mat inverseSigmaBeta = arma::inv(SigmaBeta);
	// perform cholesky factorization
	try {
		M = arma::chol(inverseSigmaBeta, "lower");
	} catch (std::runtime_error &e) {
		M.eye(numEnvVar, numEnvVar); // set elements along main diagonal to one and off-diagonal elements to zero
		logfile->warning(
				"Could not perform cholesky decomposition of inverseSigmaBeta.");
		logfile->list(
				"Will initialize all diagonal elements of M to 1.0 and all off-diagonal elements to zero.");
	}
}

void TMCMC::initializeMrrMrsAndM() {
	if (numEnvVar == 1) { // if D = 1: directly take variance
		if (initialNumLociWithGamma1 <= 1) {
			loci->betas->setOneMrrOrMrs(1, 0, 0); // initialize m to 1 (no effect)
			loci->betas->setM(1. / numLoci); // initialize with small value (shouldn't be 0 for log)
		} else {
			arma::mat M;
			computeM(M);
			loci->betas->setOneMrrOrMrs(1, 0, 0); // if D = 1: initialize m00 to 1 (no effect)
			loci->betas->setM(M(0, 0)); // initialize m
		}
		logfile->list("Initialized m, mrs and mrr.");
	} else if (initialNumLociWithGamma1 <= 1) {
		loci->betas->setM(1. / numLoci); // initialize with small value (shouldn't be 0 for log)
		for (int r = 0; r < numEnvVar; r++) {
			loci->betas->setOneMrrOrMrs(1. / numLoci, r, r); // initialize with small value (shouldn't be 0 for log)
			for (int s = 0; s < r; s++) {
				loci->betas->setOneMrrOrMrs(0, r, s); // initialize covariances to zero
			}
		}
		logfile->list("Initialized m, mrs and mrr to 0.");
	} else {
		// calculate lower triangular matrix MrrAndMrs (from SigmaBeta)
		arma::mat M;
		computeM(M);

		// initialize
		float E = 0;
		float ESquared = 0;
		float scaledmrr = 0;

		// calculate expected values
		for (int r = 0; r < numEnvVar; r++) {
			// elements on diagonal
			scaledmrr = M(r, r) * sqrt(1. / (2. * (numEnvVar - r)));
			E += scaledmrr;
			ESquared += scaledmrr * scaledmrr;
			for (int s = r + 1; s < numEnvVar; s++) {
				// elements off diagonal
				E += M(s, r);
				ESquared += M(s, r) * M(s, r);
			}
		}
		double total = ((1 + numEnvVar) * numEnvVar) / 2.; // number of elements in lower triangular matrix
		E /= total;
		ESquared /= total;

		// calculate variance
		float var = ESquared - E * E;
		float m = sqrt(var);

		// divide M by the scaling factor m
		M /= m;

		// initialize
		loci->betas->setM(m);

		for (int r = 0; r < numEnvVar; r++) {
			for (int s = 0; s <= r; s++) {
				loci->betas->setOneMrrOrMrs(M(r, s), r, s);
			}
		}
		logfile->list("Initialized m, mrs and mrr.");
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate weights of loci                                                                    //
//////////////////////////////////////////////////////////////////////////////////////////////////

TWeights::TWeights(TRandomGenerator* RandomGenerator, TLog* Logfile, int L) {
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	numLoci = L;
}

void TWeights::calculateCumulativeWeights(TParameters& Parameters) {
	logfile->startIndent("Calculating the weights for loci....");

	double effort = Parameters.getParameterDoubleWithDefault("effortForUpdates",
			0.8);
	if (effort < 0 || effort > 1)
		throw("Parameter 'effortForUpdates' should be in interval (0, 1)!");
	double proportionLoci = Parameters.getParameterDoubleWithDefault(
			"proportionLociUpdates", 0.2);
	if (proportionLoci < 0 || proportionLoci > 1)
		throw("Parameter 'proportionLociUpdates' should be in interval (0, 1)!");
	logfile->list(
			"Will invest " + toString(effort)
					+ " of the effort on updating the first "
					+ toString(proportionLoci * 100)
					+ "% of highest-ranking loci.");

	double p = calculateParamP(proportionLoci, effort);

	// calculate weight for every locus as Q_l = 0.3*U_l + 0.7*G_l where U_l is the uniform distribution
	// (and hence 1 / numLoci) and G_l is the geometric distribution
	arma::vec weights(numLoci, arma::fill::zeros);
	double uniform = 1.0 / (float) numLoci;
	for (int locus = 0; locus < numLoci; locus++) {
		// ranks is not ordered by index of the locus, but by the rank of the loci!
		int position = arma::conv_to<int>::from(arma::find(locus == ranks));
		weights(locus) = 0.3 * uniform + 0.7 * p * pow(1 - p, position);
	}
	arma::vec normalizedWeights = arma::normalise(weights, 1);
	cumWeights = arma::cumsum(normalizedWeights); // calculate cumulative weight
	logfile->endIndent();
}

double TWeights::calculateParamP(double t, double x) {
	// calculates the parameter p of the geometric distribution
	// x% of my effort should be in the first t% of my loci
	// parameter p can be computed from CDF of geometric distribution
	// by assuming that L is infinitely large (ok if L > 100)
	t *= numLoci;
	return 1 - exp((log(1 - x) / t));
}

long TWeights::pickRandomLocus() {
	double randomNumber = randomGenerator->getRand(0., 1.); // random number between 0 and 1

	// initialize
	long left = 0;
	long right = cumWeights.size();
	long middle;

	// find locus containing the random number in its cumulative weight (approximate binary search)
	while (left < right) {
		// divide
		middle = std::floor((left + right) / 2.0); // round down
		// conquer
		if (cumWeights(middle) < randomNumber)
			left = middle + 1;
		else
			right = middle;
	}
	return left;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Run MCMC                                                                                     //
//////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////
////// get main parameters ////////
///////////////////////////////////

void TMCMC::prepareMCMC(TParameters& Parameters) {
	logfile->startIndent("Preparing MCMC...");
	// MCMC parameters
	readMCMCParameters(Parameters);
	// width proposal kernels
	readProposalWidths(Parameters);
	// some update probabilities
	readOtherParameters(Parameters);
	// loci, individuals and environmental variables that should be written to file
	files.initialize(logfile, loci, linkageTrue, individuals);
	files.readFileParameters(Parameters, numLoci, numIterations);

	// prepare output files
	files.prepareTraceFiles(numEnvVar, numIndiv);

	// initialize some arrays
	if (files.writeLLTrace)
		ll = new double[numLoci];
	if (files.writePosteriorTrace)
		posterior = new double[numLoci];

	logfile->endIndent();
}

void TMCMC::readProposalWidths(TParameters& Parameters) {
	logfile->startIndent("Initial proposal widths:");
	float tmpKernel = Parameters.getParameterDoubleWithDefault(
			"widthPropKernelF_l", 0.15);
	if (tmpKernel > 0.25) {
		logfile->warning(
				"widthPropKernelF_l is too large, will set it to 0.25.");
		tmpKernel = 0.25;
	}
	logfile->list(
			"Will use a proposal kernel of width " + toString(tmpKernel)
					+ " for updates of f_l.");
	widthProposalKernelF = new float[numLoci];
	for (long l = 0; l < numLoci; l++) {
		widthProposalKernelF[l] = tmpKernel;
	}
	widthProposalKernelLogAlphaF = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelLogAlphaF", 0.35);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelLogAlphaF)
					+ " for updates of log(alphaF).");
	widthProposalKernelLogBetaF = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelLogBetaF", 0.35);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelLogBetaF)
					+ " for updates of log(betaF).");
	widthProposalKernelVkl = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelVkl", 25.);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelVkl)
					+ " for updates of v_kl.");
	widthProposalKernelUk = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelUik", 0.01);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelUk)
					+ " for updates of u_ik.");
	tmpKernel = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelBetald", 0.5);
	logfile->list(
			"Will use a proposal kernel of width " + toString(tmpKernel)
					+ " for updates of beta_ld.");
	widthProposalKernelBetadl = new float *[numEnvVar];
	for (int d = 0; d < numEnvVar; d++) {
		widthProposalKernelBetadl[d] = new float[numLoci];
		for (long l = 0; l < numLoci; l++) {
			widthProposalKernelBetadl[d][l] = tmpKernel;
		}
	}
	widthProposalKernelPi = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelLogPi", 0.001);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelPi) + " for updates of pi.");
	widthProposalKernelLambda1 = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelLambda1", 1);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelLambda1)
					+ " for updates of lambda_1");
	widthProposalKernelLambda2 = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelLambda2", 1);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelLambda2)
					+ " for updates of lambda_2");
	widthProposalKernelLittleM = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelLittleM", 0.2);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelLittleM)
					+ " for updates of m.");
	widthProposalKernelMrr = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelMrr", 1);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelMrr)
					+ " for updates of mrr.");
	widthProposalKernelMrs = Parameters.getParameterDoubleWithDefault(
			"widthProposalKernelMrs", 2);
	logfile->list(
			"Will use a proposal kernel of width "
					+ toString(widthProposalKernelMrs)
					+ " for updates of mrs.");
	widthSdProposalKernelBetaL = Parameters.getParameterDoubleWithDefault(
			"widthSdProposalKernelBetaL", 0.1);
	logfile->list(
			"Will use a normal proposal kernel with standard deviation "
					+ toString(widthSdProposalKernelBetaL)
					+ " for updates of beta_l when updating gamma_l to one.");
	widthVarProposalKernelBetaL = widthSdProposalKernelBetaL
			* widthSdProposalKernelBetaL;
	logfile->endIndent();
}

void TMCMC::readMCMCParameters(TParameters& Parameters) {
	// MCMC parameters
	logfile->startIndent("Parameters of MCMC algorithm:");
	numBurnins = Parameters.getParameterIntWithDefault("numBurnins", 10);
	burnin = Parameters.getParameterIntWithDefault("burnin", 200);
	logfile->list(
			"Will run " + toString(numBurnins) + " burnin(s) of "
					+ toString(burnin) + " iterations each.");
	numIterations = Parameters.getParameterIntWithDefault("iterations", 10000);
	logfile->list(
			"Will run MCMC for " + toString(numIterations) + " iterations.");
	float proportionBetaUpdatesPerIter =
			Parameters.getParameterDoubleWithDefault("propBetaUpdatesPerIter",
					1);
	numBetaUpdatesPerIter = numLoci * proportionBetaUpdatesPerIter;
	lambda_for_pi = Parameters.getParameterDoubleWithDefault("lambdaPi", 10);
	logfile->list(
			"Will use lambda = " + toString(lambda_for_pi)
					+ " as an exponential prior for pi.");

	std::string tmp = Parameters.getParameterStringWithDefault("betaZero",
			"false");
	if (tmp == "true") {
		fixBetaGammaToZero = true;
		logfile->list("Will fix beta and gamma to zero.");
	}
	tmp = Parameters.getParameterStringWithDefault("fixU", "false");
	if (tmp == "true") {
		fixU = true;
		logfile->list("Will not update U.");
	}
	tmp = Parameters.getParameterStringWithDefault("fixV", "false");
	if (tmp == "true") {
		fixV = true;
		logfile->list("Will not update V.");
	}

	logfile->endIndent();
}

void TMCMC::readOtherParameters(TParameters& Parameters) {
	// some update probabilities
	logfile->startIndent("Other parameters:");
	qOfgamma1Togamma0 = Parameters.getParameterDoubleWithDefault(
			"qOfgamma1Togamma0", 0.5);
	if (qOfgamma1Togamma0 < 0 || qOfgamma1Togamma0 > 1)
		throw "qOfgamma1Togamma0 must be between 0 and 1!";
	logfile->list(
			"Will use a probability of " + toString(qOfgamma1Togamma0)
					+ " to change gamma_l 1 -> 0 if gamma = 1 (and a probability of "
					+ toString(1 - qOfgamma1Togamma0)
					+ " to update beta_dl instead.)");
	logqOfgamma1Togamma0 = log(qOfgamma1Togamma0);

	lambdaM = Parameters.getParameterDoubleWithDefault("lambdaM", 1);
	logfile->list(
			"Will use lambda_m = " + toString(lambdaM)
					+ " as an exponential prior on m.");
	logfile->endIndent();
}

///////////////////////////////////
///////// MCMC framework //////////
///////////////////////////////////

void TMCMC::coordinateMCMC(TParameters & Parameters) {
	if (fixBetaGammaToZero) {
		for (int l = 0; l < numLoci; l++) {
			loci->betas->theBetas[0][l] = 0.0;
			loci->betas->gammas[l] = 0;
		}
	}

	// need to write files already? (values from initialization)
	if (files.writeAfterInit) {
		if (files.writeLLTrace)
			computeFullLogLikelihood();
		if (files.writePosteriorTrace)
			computePosterior();
		files.writeTraceFiles(numEnvVar, hmmUpdater, numLoci, numIndiv, ll,
				posterior);
	}

	// first run burnin
	prepareBurnin();
	int numBurninsWithoutRJMCMC = floor(numBurnins / 2.);
	bool RJMCMC = false;

	// start time measurements
	struct timeval start;
	gettimeofday(&start, nullptr);

	for (int curBurnin = 0; curBurnin < numBurnins; curBurnin++) {
		if (burnin > 0) {
			if (curBurnin == numBurninsWithoutRJMCMC) { // now switch to RJMCMC
				RJMCMC = true;
				initializeGammaAndPiInsideBurnin(Parameters);
			}

			int oldProg = 0;
			std::string progressString = "Running burnin number "
					+ toString(curBurnin + 1) + " of length " + toString(burnin)
					+ " ...";
			logfile->listFlush(progressString + "(0%)");
			for (int iter = 0; iter < burnin; ++iter) {
				oneMCMCIteration(RJMCMC);

				// print to file
				if (iter % files.thinning == 0 && files.writeDuringBurnin) {
					if (files.writeLLTrace)
						computeFullLogLikelihood();
					if (files.writePosteriorTrace)
						computePosterior();
					files.writeTraceFiles(numEnvVar, hmmUpdater, numLoci,
							numIndiv, ll, posterior);
				}

				// print progress
				int prog = (double) iter / (double) burnin * 100.0;
				if (prog > oldProg) {
					oldProg = prog;
					logfile->listOverFlush(
							progressString + "(" + toString(oldProg) + "%)");
				}
			}
			logfile->overList(progressString + "done!   ");
			files.writeBurninFiles(numLoci, widthProposalKernelBetadl,
					numAcceptedBetaPerLD, totalNumUpdatesBetaPerLocus);
			concludeBurnin(RJMCMC);
		}
	}

	// now run chain with sampling
	prepareChainWithSampling();

	int oldProg = 0;
	std::string progressString = "Running MCMC chain of length "
			+ toString(numIterations) + " ...";
	logfile->listFlush(progressString + "(0%)");
	for (int iter = 0; iter < numIterations; ++iter) {
		oneMCMCIteration(RJMCMC);

		// print to file
		if (iter % files.thinning == 0) {
			if (files.writeLLTrace)
				computeFullLogLikelihood();
			if (files.writePosteriorTrace)
				computePosterior();
			files.writeTraceFiles(numEnvVar, hmmUpdater, numLoci, numIndiv, ll,
					posterior);

			if (files.writeHyperFinal)
				addToHypers(); // to compute mean posterior sigmaBeta
			if (files.writeUVFinal)
				addToUV();
		}

		// print progress
		int prog = (double) iter / (double) numIterations * 100.0;
		if (prog > oldProg) {
			oldProg = prog;
			logfile->listOverFlush(
					progressString + " (" + toString(oldProg) + "%)");
		}
	}
	logfile->overList(progressString + " done!   ");
	concludeChain(start);

	logfile->endIndent();
}

void TMCMC::oneMCMCIteration(bool RJMCMC) { // core function that is called in every MCMC iteration!
	updateLoci(RJMCMC);
	if (!fixU)
		updateIndividuals();

	numAcceptedAlphaF += updateAlphaF();
	numAcceptedBetaF += updateBetaF();
	if (!fixBetaGammaToZero) {
		numAcceptedLittleM += updateLittleM();
		if (numEnvVar > 1) {
			for (int r = 0; r < numEnvVar; r++) {
				numAcceptedMrr += updateOneMrr(r);
				for (int s = 0; s < r; s++) {
					numAcceptedMrs += updateOneMrs(r, s);
				}
			}
		}

		if (linkageTrue) {
			numAcceptedLambda1 += updateLambda1();
			numAcceptedLambda2 += updateLambda2();
		} else {
			if (RJMCMC)
				numAcceptedPi += updatePi();
		}
	}
}

void TMCMC::updateLoci(bool RJMCMC) {
	for (long l = 0; l < numLoci; l++) { // go over all loci and update v and mu
		double sum = initializeSumOverIndividuals(l);
		updateF(l, sum);
		if (!fixV) {
			for (int k = 0; k < numLatFac; k++)
				updateVkl(k, l, sum);
		}
	}
	if (!fixBetaGammaToZero)
		updateBetaGamma(RJMCMC); // can't use the sum anymore, as we update different loci now
}

void TMCMC::updateIndividuals() {
	double sum = initializeSumOverAll();
	for (int k = 0; k < numLatFac; k++) {
		updateUk(k, sum);
		assertThatVectorsAreOrthogonal();
	}
}

void TMCMC::prepareBurnin() {
	minAlleleFreq = 1.0 / (double) numLoci / 10.0;
	oneMinusminAlleleFreq = 1.0 - minAlleleFreq;

	initializeStorageAcceptanceRates();
	initializeLogitFil();
	clearCounters();
	files.prepareBurninFiles();
}

void TMCMC::concludeBurnin(bool RJMCMC) {
	concludeAcceptanceRates(burnin);
	adjustAllProposalWidth(burnin, RJMCMC); // adjust proposal width to get acceptance rate = 0.3
	clearCounters();
}

void TMCMC::prepareChainWithSampling() {
	initializeStorageBetas();
	if (files.writeMuFinal)
		initializeStorageMus();
	if (files.writeHyperFinal)
		initializeStorageHypers();
	if (files.writeUVFinal)
		initializeStorageUV();

	clearCounters();
}

void TMCMC::concludeChain(struct timeval start) {
	struct timeval end;
	gettimeofday(&end, nullptr);
	float runtime = (end.tv_sec - start.tv_sec) / 60.0;
	logfile->conclude("MCMC took " + toString(runtime) + " minutes!");

	concludeAcceptanceRates(numIterations);

	// write final betas
	concludeExpAndVarBetas();
	files.writeToBetaGammaFile(numEnvVar, envVarNames, numLoci, gammaSum,
			gammaCounter, expValBetadl, expValSquaredBetadl, betaUpdateCounter);
	// write final mu
	if (files.writeMuFinal) {
		concludeExpAndVarMus();
		files.writeFinalMuToFile(numLoci, expValMul, expValSquaredMul);
	}
	// write final hyperparameters
	if (files.writeHyperFinal) {
		processPostMeanSigmaBeta();
		concludeExpValHypers();
		files.writeFinalHyperToFile(numEnvVar, envVarNames, postMeanSigmaBeta,
				alphaFSum, betaFSum, piSum, lambda_1_sum, lambda_2_sum);
	}
	// write final U and V
	if (files.writeUVFinal) {
		concludeExpValUV();
		files.writeFinalUVToFile(numLatFac, numIndiv, numLoci, individuals,
				loci, sumU, sumV);
	}
	// write final iteration
	files.writeLastIterToFile(numEnvVar, numLoci, numLatFac, numIndiv,
			loci->betas->pi, hmmUpdater, loci->betas->m, loci->betas->MrrAndMrs,
			loci->alphaF, loci->betaF, loci->betas->gammas,
			loci->betas->theBetas, loci->mus, loci->vs, individuals,
			linkageTrue);
}

///////////////////////////////////
/////// update "skeletons" ////////
///////////////////////////////////

bool TMCMC::coreUpdateParamLocus(long l, double diffNewOld, double prior,
		double & sumOld) {
	// compute log likelihood
	double sumNew = 0;
	for (int i = 0; i < numIndiv; i++) {
		sumNew += getLogLikelihood(i, l,
				logisticLookup->approxLogistic(logit_f_il[i][l] + diffNewOld));
	}

	// compute log hastings ratio
	double logH = prior + sumNew - sumOld;

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		// update f_il
		for (int i = 0; i < numIndiv; i++) {
			logit_f_il[i][l] += diffNewOld;
		}
		sumOld = sumNew;
		return true;
	} else
		return false;
}

bool TMCMC::coreUpdateParamLocus(long l, double * diffNewOld, double prior,
		double & sumOld) { // accepts pointer to array
	// compute log likelihood
	double sumNew = 0;
	for (int i = 0; i < numIndiv; i++) {
		sumNew += getLogLikelihood(i, l,
				logisticLookup->approxLogistic(
						logit_f_il[i][l] + diffNewOld[i]));
	}

	// compute log hastings ratio
	double logH = prior + sumNew - sumOld;

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		// update f_il
		for (int i = 0; i < numIndiv; i++) {
			logit_f_il[i][l] += diffNewOld[i];
		}
		sumOld = sumNew;
		return true;
	} else
		return false;
}

///////////////////////////////////
/// updates related to f_l/mu_l ///
///////////////////////////////////

void TMCMC::updateF(long l, double & sum) {
	// propose (uniform proposal kernel, mirrored at 0 and 1)
	double oldF = logisticLookup->approxLogistic(loci->mus[l]);
	double newF = oldF + randomGenerator->getRand() * widthProposalKernelF[l]
			- widthProposalKernelF[l] / 2.0;
	if (newF < minAlleleFreq) { // mirror
		newF = 2.0 * minAlleleFreq - newF;
	} else if (newF > oneMinusminAlleleFreq) {
		newF = 2.0 * oneMinusminAlleleFreq - newF;
	}

	// compute difference
	float newMu = logit(newF);
	double diffNewOld = newMu - loci->mus[l];

	// compute prior term
	double prior = (loci->alphaF - 1) * (log(newF) - log(oldF))
			+ (loci->betaF - 1) * (log(1 - newF) - log(1 - oldF));
	if (coreUpdateParamLocus(l, diffNewOld, prior, sum)) {
		loci->setOneMu(newMu, l);
		numAcceptedFPerLocus[l]++;
	}
	if (storageMusInitialized) {
		expValMul[l] += loci->mus[l];
		expValSquaredMul[l] += loci->mus[l] * loci->mus[l];
	}
}

bool TMCMC::updateAlphaF() {
	// propose new log(alphaF) (uniform proposal kernel)
	double newLogAlphaF = loci->logAlphaF
			+ randomGenerator->getRand() * widthProposalKernelLogAlphaF
			- widthProposalKernelLogAlphaF / 2.0;
	double newAlphaF = exp(newLogAlphaF);

	// compute sum
	double sumF = 0;
	for (int l = 0; l < numLoci; l++) {
		sumF += log(logisticLookup->approxLogistic(loci->mus[l]));
	}

	// compute log hastings ratio
	double logH = numLoci
			* (randomGenerator->gammaln(newAlphaF + loci->betaF)
					- randomGenerator->gammaln(loci->alphaF + loci->betaF)
					+ randomGenerator->gammaln(loci->alphaF)
					- randomGenerator->gammaln(newAlphaF))
			+ (newAlphaF - loci->alphaF) * sumF;

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		loci->setAlphaF(newAlphaF); // set new alphaF and logAlphafF
		loci->setLogAlphaF(newLogAlphaF);
		return true;
	} else
		return false;
}

bool TMCMC::updateBetaF() {
	// compute sum
	double sumF = 0;
	for (int l = 0; l < numLoci; l++) {
		sumF += log(1 - logisticLookup->approxLogistic(loci->mus[l]));
	}

	// propose new log(betaF) (uniform proposal kernel)
	double newLogBetaF = loci->logBetaF
			+ randomGenerator->getRand() * widthProposalKernelLogBetaF
			- widthProposalKernelLogBetaF / 2.0;
	double newBetaF = exp(newLogBetaF);

	// compute log hastings ratio
	double logH = numLoci
			* (randomGenerator->gammaln(newBetaF + loci->alphaF)
					- randomGenerator->gammaln(loci->betaF + loci->alphaF)
					+ randomGenerator->gammaln(loci->betaF)
					- randomGenerator->gammaln(newBetaF))
			+ (newBetaF - loci->betaF) * sumF;

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		loci->setBetaF(newBetaF); // set new betaF and logBetaF
		loci->setLogBetaF(newLogBetaF);
		return true;
	} else
		return false;
}

///////////////////////////////////
////// updates related to UV //////
///////////////////////////////////

void TMCMC::updateVkl(int k, long l, double & sum) {
	// propose (uniform proposal kernel)
	double newVkl = loci->vs[k][l]
			+ randomGenerator->getRand() * widthProposalKernelVkl
			- widthProposalKernelVkl / 2.0;

	// compute difference
	double diffNewOld[numIndiv];
	for (int i = 0; i < numIndiv; i++) {
		diffNewOld[i] = (newVkl - loci->vs[k][l]) * individuals->getUik(i, k);
	}

	// compute prior term
	double prior = 0; // uniform prior cancels out
	if (coreUpdateParamLocus(l, diffNewOld, prior, sum)) {
		loci->setOneVkl(newVkl, k, l); // set new vkl
		numAcceptedVkl++;
	}
}

void TMCMC::updateUk(int k, double & sumOld) {
	// propose new vector u_k (add random vector eta)
	double newUk[numIndiv];
	for (int i = 0; i < numIndiv; i++) {
		newUk[i] = individuals->getUik(i, k)
				+ randomGenerator->getRand() * widthProposalKernelUk
				- widthProposalKernelUk / 2.0;
	}

	// make vector orthogonal
	gramSchmidt(k, newUk, numLatFac);

	// compute log likelihood
	double sumNew = 0;
	for (int i = 0; i < numIndiv; i++) {
		for (int l = 0; l < numLoci; l++) {
			sumNew += getLogLikelihood(i, l,
					logisticLookup->approxLogistic(
							logit_f_il[i][l]
									+ (newUk[i] - individuals->getUik(i, k))
											* loci->vs[k][l]));
		}
	}

	// compute log hastings ratio
	double logH = sumNew - sumOld;

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		// update u_k and logit_f_il
		for (int i = 0; i < numIndiv; i++) {
			for (int l = 0; l < numLoci; l++) {
				logit_f_il[i][l] += (newUk[i] - individuals->getUik(i, k))
						* loci->vs[k][l];
			}
			individuals->setUik(i, k, newUk[i]);
		}
		numAcceptedUk++;
		sumOld = sumNew;
	}
}

///////////////////////////////////
//// updates related to beta's ////
///////////////////////////////////

void TMCMC::updateBetaGamma(bool RJMCMC) {
	// calculate sum over hyperparameters (these don't change during these updates)
	double sumHypersForGammaToZeroUpdate;
	double sumHypersForGammaToOneUpdate;
	calcSumOverHyperParamsBeta(sumHypersForGammaToZeroUpdate,
			sumHypersForGammaToOneUpdate);

	// update betas and gammas
	for (int update = 0; update < numBetaUpdatesPerIter; update++) { // pick loci according to weight and update beta (locus can be updated >1 per iteration)
		updateSwitcherBetaGamma(weights->pickRandomLocus(),
				sumHypersForGammaToZeroUpdate, sumHypersForGammaToOneUpdate,
				RJMCMC);  // update gamma and beta
	}
}

void TMCMC::updateSwitcherBetaGamma(long l,
		double sumHypersForGammaToZeroUpdate,
		double sumHypersForGammaToOneUpdate, bool RJMCMC) {
	double sum = initializeSumOverIndividuals(l);
	if (RJMCMC) {
		if (loci->betas->gammas[l] == 0) {
			gammaTotalNumTrialsCounter++;
			gammaToOne(l, sum, sumHypersForGammaToOneUpdate);
		} else if (randomGenerator->getRand() < qOfgamma1Togamma0) {
			gammaTotalNumTrialsCounter++;
			gammaToZero(l, sum, sumHypersForGammaToZeroUpdate);
		} else {
			for (int d = 0; d < numEnvVar; d++) {
				betaTotalNumTrialsCounter++;
				sum = updateBetadl(d, l, sum);
			}
		}
	} else {
		for (int d = 0; d < numEnvVar; d++) {
			betaTotalNumTrialsCounter++;
			sum = updateBetadl(d, l, sum);
		}
	}
	if (storageBetasInitialized) {
		gammaCounter[l]++;
		if (loci->betas->gammas[l] == 1) { // gamma posterior
			gammaSum[l]++;
		}
	}
}

double TMCMC::updateBetadl(int d, long l, double sum) {
	// propose (uniform proposal kernel)
	double newBetadl = loci->betas->theBetas[d][l]
			+ randomGenerator->getRand() * widthProposalKernelBetadl[d][l]
			- widthProposalKernelBetadl[d][l] / 2.0;

	// compute difference
	double diffNewOld[numIndiv];
	for (int i = 0; i < numIndiv; i++) {
		diffNewOld[i] = (newBetadl - loci->betas->theBetas[d][l])
				* individuals->getXid(i, d);
	}

	// compute prior
	double sumInnerOld;
	double sumInnerNew;
	double sumOuter = 0;
	for (int s = 0; s <= d; s++) { // needs to be <= d, because only s>d is equal in both hastings ratios!
		sumInnerOld = 0;
		sumInnerNew = 0;
		for (int r = s; r < numEnvVar; r++) {
			sumInnerOld += loci->betas->MrrAndMrs(r, s)
					* loci->betas->theBetas[r][l];
			if (r == d) {
				sumInnerNew += loci->betas->MrrAndMrs(r, s) * newBetadl;
			} else {
				sumInnerNew += loci->betas->MrrAndMrs(r, s)
						* loci->betas->theBetas[r][l];
			}
		}
		sumOuter += sumInnerOld * sumInnerOld - sumInnerNew * sumInnerNew;
	}

	double prior = 0.5 * 1. / loci->betas->m * 1. / loci->betas->m * sumOuter;

	// accept of reject
	if (coreUpdateParamLocus(l, diffNewOld, prior, sum)) {
		loci->betas->setOneBetadl(newBetadl, d, l); // set new beta_ld
		if (storageBetasInitialized) {
			expValBetadl[d][l] += newBetadl;
			expValSquaredBetadl[d][l] += newBetadl * newBetadl;
			betaUpdateCounter[d][l]++;
		}
		numAcceptedBetaPerLD[d][l]++;
	}
	totalNumUpdatesBetaPerLocus[d][l]++;
	return sum;
}

void TMCMC::gammaToOne(long l, double sum,
		double sumHypersForGammaToOneUpdate) {
	// propose new beta's
	double newBetaL[numEnvVar];
	for (int d = 0; d < numEnvVar; d++) {
		newBetaL[d] = randomGenerator->getNormalRandom(0,
				widthSdProposalKernelBetaL);
	}

	// compute difference
	double diffNewOld[numIndiv];
	double innerSum;
	for (int i = 0; i < numIndiv; i++) {
		innerSum = 0;
		for (int d = 0; d < numEnvVar; d++) {
			innerSum += newBetaL[d] * individuals->getXid(i, d);
		}
		diffNewOld[i] = innerSum;
	}

	// tweak v' = v - U^T * X * Beta_l; where X * Beta_l is simply diffNewOld
	double newV[numLatFac];
	for (int k = 0; k < numLatFac; k++) {
		innerSum = 0;
		for (int i = 0; i < numIndiv; i++) {
			innerSum += individuals->getUik(i, k) * diffNewOld[i];
		}
		newV[k] = loci->getOneVkl(k, l) - innerSum;
	}

	// adjust difference accordingly: logit(f_il_new) = logit(f_il_old) - beta_l*x_i + u_i(v_l_new - v_l_old)
	for (int i = 0; i < numIndiv; i++) {
		innerSum = 0;
		for (int k = 0; k < numLatFac; k++) {
			innerSum += individuals->getUik(i, k)
					* (newV[k] - loci->getOneVkl(k, l));
		}
		diffNewOld[i] += innerSum;
	}

	// compute prior
	double sumBeta_ldSquared = 0;
	double sumMrsBetalrInner;
	double sumMrsBetalrOuter = 0;
	for (int s = 0; s < numEnvVar; s++) {
		sumMrsBetalrInner = 0;
		sumBeta_ldSquared += newBetaL[s] * newBetaL[s];
		for (int r = s; r < numEnvVar; r++) {
			sumMrsBetalrInner += loci->betas->MrrAndMrs(r, s) * newBetaL[r];
		}
		sumMrsBetalrOuter += sumMrsBetalrInner * sumMrsBetalrInner;
	}

	double prior = sumHypersForGammaToOneUpdate
			+ 0.5
					* ((1.0 / widthVarProposalKernelBetaL) * sumBeta_ldSquared
							- 1. / loci->betas->m * 1. / loci->betas->m
									* sumMrsBetalrOuter);

	if (linkageTrue) {
		double transProbSum = hmmUpdater->getTau(l,
				loci->betas->getGamma(l - 1), 1)
				+ hmmUpdater->getTau(l + 1, 1, loci->betas->getGamma(l + 1))
				- hmmUpdater->getTau(l, loci->betas->getGamma(l - 1), 0)
				- hmmUpdater->getTau(l + 1, 0, loci->betas->getGamma(l + 1));
		prior += transProbSum;
	}

	// accept of reject
	if (coreUpdateParamLocus(l, diffNewOld, prior, sum)) {
		for (int d = 0; d < numEnvVar; d++) {
			loci->betas->setOneBetadl(newBetaL[d], d, l); // set new beta_ld
			if (storageBetasInitialized) { // store expected value and E(X^2)
				expValBetadl[d][l] += newBetaL[d];
				expValSquaredBetadl[d][l] += newBetaL[d] * newBetaL[d];
				betaUpdateCounter[d][l]++;
			}
		}
		for (int k = 0; k < numLatFac; k++) // set new v
			loci->setOneVkl(newV[k], k, l);
		loci->betas->gammas[l] = 1; // set new gamma
		loci->betas->indexLociWhereGammaIsOne.push_back(l);
		numAcceptedGamma++;
	}
}

void TMCMC::gammaToZero(long l, double sum,
		double sumHypersForGammaToZeroUpdate) {
	// compute difference
	double diffNewOld[numIndiv];
	for (int i = 0; i < numIndiv; i++) {
		diffNewOld[i] = 0;
		for (int d = 0; d < numEnvVar; d++)
			diffNewOld[i] += loci->betas->theBetas[d][l]
					* individuals->getXid(i, d);
		diffNewOld[i] *= -1;
	}

	double newV[numLatFac];
	for (int k = 0; k < numLatFac; k++) {
		newV[k] = 0;
		for (int i = 0; i < numIndiv; i++) {
			newV[k] += individuals->getUik(i, k) * diffNewOld[i];
		}
		newV[k] = loci->getOneVkl(k, l) - newV[k]; // - newV[k], because I have a minus when computing diffNewOld[i] = -(innerSum);
	}

	// adjust difference accordingly: logit(f_il_new) = logit(f_il_old) - beta_l*x_i + u_i(v_l_new - v_l_old)
	for (int i = 0; i < numIndiv; i++) {
		for (int k = 0; k < numLatFac; k++)
			diffNewOld[i] += individuals->getUik(i, k)
					* (newV[k] - loci->getOneVkl(k, l));
	}

	// compute prior
	double sumBeta_ldSquared = 0;
	double sumMrsBetalrInner;
	double sumMrsBetalrOuter = 0;
	for (int s = 0; s < numEnvVar; s++) {
		sumMrsBetalrInner = 0;
		sumBeta_ldSquared += loci->betas->theBetas[s][l]
				* loci->betas->theBetas[s][l];
		for (int r = s; r < numEnvVar; r++) {
			sumMrsBetalrInner += loci->betas->MrrAndMrs(r, s)
					* loci->betas->theBetas[r][l];
		}
		sumMrsBetalrOuter += sumMrsBetalrInner * sumMrsBetalrInner;
	}

	double prior = sumHypersForGammaToZeroUpdate
			+ 0.5
					* (1. / loci->betas->m * 1. / loci->betas->m
							* sumMrsBetalrOuter
							- (1.0 / widthVarProposalKernelBetaL)
									* sumBeta_ldSquared);

	if (linkageTrue) {
		double transProbSum = hmmUpdater->getTau(l,
				loci->betas->getGamma(l - 1), 0)
				+ hmmUpdater->getTau(l + 1, 0, loci->betas->getGamma(l + 1))
				- hmmUpdater->getTau(l, loci->betas->getGamma(l - 1), 1)
				- hmmUpdater->getTau(l + 1, 1, loci->betas->getGamma(l + 1));
		prior += transProbSum;
	}

	// accept of reject
	if (coreUpdateParamLocus(l, diffNewOld, prior, sum)) {
		//std::cout << "accepted!" << std::endl;
		for (int d = 0; d < numEnvVar; d++)
			loci->betas->setOneBetadl(0, d, l); // set beta_ld = 0
		for (int k = 0; k < numLatFac; k++) // set v to new value
			loci->setOneVkl(newV[k], k, l);
		loci->betas->gammas[l] = 0; // update gamma
		// remove index of that locus from indexLociWhereGammaIsOne
		loci->betas->indexLociWhereGammaIsOne.erase(
				std::remove(loci->betas->indexLociWhereGammaIsOne.begin(),
						loci->betas->indexLociWhereGammaIsOne.end(), l),
				loci->betas->indexLociWhereGammaIsOne.end());
		numAcceptedGamma++;
	}
}

bool TMCMC::updatePi() {
	// propose (uniform proposal kernel, mirrored at zero (can't be negative))
	double newPi = fabs(
			loci->betas->pi + randomGenerator->getRand() * widthProposalKernelPi
					- widthProposalKernelPi / 2.0);

	if (newPi > 1)
		newPi = 2 - newPi;
	if (newPi == 1)
		newPi = 1 - 1. / numLoci;
	if (newPi == 0)
		newPi = 1. / numLoci;
	double newLogPi = log(newPi);

	// compute log hastings ratio
	double logH = lambda_for_pi * (loci->betas->pi - newPi)
			+ loci->betas->indexLociWhereGammaIsOne.size()
					* (newLogPi - loci->betas->logPi)
			+ (numLoci - loci->betas->indexLociWhereGammaIsOne.size())
					* (log(1 - newPi) - log(1 - loci->betas->pi));

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		loci->betas->logPi = newLogPi;
		loci->betas->pi = newPi; // set new pi
		return true;
	} else
		return false;
}

bool TMCMC::updateLambda1() {
	// propose (uniform proposal kernel)
	double newLambda1 = fabs(
			hmmUpdater->getLambda1()
					+ randomGenerator->getRand() * widthProposalKernelLambda1
					- widthProposalKernelLambda1 / 2.0); // must be positive

					// update tau with new lambda1
	hmmUpdater->updateTransProbWithNewLambda1(newLambda1);

	// hastings ratio
	double logH = 0;
	for (int l = 0; l < numLoci; l++) {
		logH += log(
				hmmUpdater->getNewTau(l, loci->betas->getGamma(l - 1),
						loci->betas->getGamma(l)))
				- log(
						hmmUpdater->getTau(l, loci->betas->getGamma(l - 1),
								loci->betas->getGamma(l)));
	}

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		hmmUpdater->acceptNewLambda1(newLambda1);
		return true;
	} else
		return false;
}

bool TMCMC::updateLambda2() {
	// propose (uniform proposal kernel)
	double newLambda2 = fabs(
			hmmUpdater->getLambda2()
					+ randomGenerator->getRand() * widthProposalKernelLambda2
					- widthProposalKernelLambda2 / 2.0); // must be positive

					// update tau with new lambda2
	hmmUpdater->updateTransProbWithNewLambda2(newLambda2);

	// hastings ratio
	double logH = 0;
	for (int l = 0; l < numLoci; l++) {
		logH += log(
				hmmUpdater->getNewTau(l, loci->betas->getGamma(l - 1),
						loci->betas->getGamma(l)))
				- log(
						hmmUpdater->getTau(l, loci->betas->getGamma(l - 1),
								loci->betas->getGamma(l)));
	}

	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		hmmUpdater->acceptNewLambda2(newLambda2);
		return true;
	} else
		return false;

}

bool TMCMC::updateLittleM() {
	// propose (uniform proposal kernel, mirrored at 0)
	double newM = fabs(
			loci->betas->m
					+ randomGenerator->getRand() * widthProposalKernelLittleM
					- widthProposalKernelLittleM / 2.0);
	if (newM == 0)
		newM = 0.00000000001; // avoid problems with log

	double outerSum = 0;
	double innerSum;
	for (std::vector<int>::iterator locus =
			loci->betas->indexLociWhereGammaIsOne.begin();
			locus < loci->betas->indexLociWhereGammaIsOne.end(); locus++) { // go over all loci where gamma = 1
		for (int s = 0; s < numEnvVar; s++) { // cols of M
			innerSum = 0;
			for (int r = s; r < numEnvVar; r++) { // rows of M
				innerSum += loci->betas->MrrAndMrs(r, s)
						* loci->betas->theBetas[r][*locus];
			}
			outerSum += innerSum * innerSum;
		}
	}
	// compute log hastings ratio
	double logH = lambdaM * (loci->betas->m - newM)
			+ loci->betas->indexLociWhereGammaIsOne.size() * numEnvVar
					* (log(loci->betas->m) - log(newM))
			+ 0.5
					* (1. / loci->betas->m * 1. / loci->betas->m
							- 1. / newM * 1. / newM) * outerSum;
	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		loci->betas->m = newM; // set new m
		return true;
	} else
		return false;
}

bool TMCMC::updateOneMrr(int r) {
	// propose (uniform proposal kernel, mirrored at 0)
	double newMrr = fabs(
			loci->betas->MrrAndMrs(r, r)
					+ randomGenerator->getRand() * widthProposalKernelMrr
					- widthProposalKernelMrr / 2.0);
	if (newMrr == 0)
		newMrr = 0.000000001; // avoid problems with log

	double outerSum = 0;
	double innerSum;
	double tmp1;
	double tmp2;
	for (std::vector<int>::iterator locus =
			loci->betas->indexLociWhereGammaIsOne.begin();
			locus < loci->betas->indexLociWhereGammaIsOne.end(); locus++) { // go over all loci where gamma = 1
		innerSum = 0;
		for (int t = r + 1; t < numEnvVar; t++) {
			innerSum += loci->betas->MrrAndMrs(t, r)
					* loci->betas->theBetas[t][*locus];
		}
		tmp1 = loci->betas->MrrAndMrs(r, r) * loci->betas->theBetas[r][*locus]
				+ innerSum;
		tmp2 = newMrr * loci->betas->theBetas[r][*locus] + innerSum;
		outerSum += tmp1 * tmp1 - tmp2 * tmp2;
	}

	// compute log hastings ratio
	double logNewMinusLogOld = log(newMrr) - log(loci->betas->MrrAndMrs(r, r));
	double logH = (numEnvVar - (r + 1) - 1) / 2. * logNewMinusLogOld
			+ 0.5 * (loci->betas->MrrAndMrs(r, r) - newMrr)
			+ loci->betas->indexLociWhereGammaIsOne.size() * logNewMinusLogOld
			+ 0.5 * 1. / loci->betas->m * 1. / loci->betas->m * outerSum;
	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		loci->betas->setOneMrrOrMrs(newMrr, r, r); // set new mrr
		return true;
	} else
		return false;
}

bool TMCMC::updateOneMrs(int r, int s) {
	// propose (uniform proposal kernel)
	double newMrs = loci->betas->MrrAndMrs(r, s)
			+ randomGenerator->getRand() * widthProposalKernelMrs
			- widthProposalKernelMrs / 2.0;

	double outerSum = 0;
	double innerSum;
	double tmp1;
	double tmp2;
	for (std::vector<int>::iterator locus =
			loci->betas->indexLociWhereGammaIsOne.begin();
			locus < loci->betas->indexLociWhereGammaIsOne.end(); locus++) { // go over all loci where gamma = 1
		innerSum = 0;
		for (int t = s; t < numEnvVar; t++) {
			if (t != r)
				innerSum += loci->betas->MrrAndMrs(t, s)
						* loci->betas->theBetas[t][*locus];
		}
		tmp1 = loci->betas->MrrAndMrs(r, s) * loci->betas->theBetas[r][*locus]
				+ innerSum;
		tmp2 = newMrs * loci->betas->theBetas[r][*locus] + innerSum;
		outerSum += tmp1 * tmp1 - tmp2 * tmp2;
	}

	// compute log hastings ratio
	double logH = 0.5
			* (loci->betas->MrrAndMrs(r, s) * loci->betas->MrrAndMrs(r, s)
					- newMrs * newMrs)
			+ 0.5 * 1. / loci->betas->m * 1. / loci->betas->m * outerSum;
	// accept or reject
	if (log(randomGenerator->getRand()) < logH) {
		loci->betas->setOneMrrOrMrs(newMrs, r, s); // set new mrs
		return true;
	} else
		return false;
}

///////////////////////////////////
///// small helper functions///////
///////////////////////////////////

void TMCMC::initializeLogitFil() {
	logit_f_il = new float*[numIndiv];
	for (int i = 0; i < numIndiv; i++) {
		logit_f_il[i] = new float[numLoci];
		for (long l = 0; l < numLoci; l++) {
			logit_f_il[i][l] = loci->mus[l] + sumBetalXi(i, l) + sumUiVl(i, l);
		}
	}
}

double TMCMC::initializeSumOverAll() {
	double sum = 0;
	for (int i = 0; i < numIndiv; i++) {
		for (long l = 0; l < numLoci; l++) {
			sum += getLogLikelihood(i, l,
					logisticLookup->approxLogistic(logit_f_il[i][l]));
		}
	}
	return sum;
}

double TMCMC::initializeSumOverIndividuals(long l) {
	double sum = 0;
	for (int i = 0; i < numIndiv; i++) {
		sum += getLogLikelihood(i, l,
				logisticLookup->approxLogistic(logit_f_il[i][l]));
	}
	return sum;
}

void TMCMC::computeFullLogLikelihood() {
	for (int l = 0; l < numLoci; l++) {
		ll[l] = 0;
		for (int i = 0; i < numIndiv; i++) {
			double f_il = logistic(
					loci->mus[l] + sumBetalXi(i, l) + sumUiVl(i, l));
			ll[l] += getLogLikelihood(i, l, f_il);
		}
	}
}

double TMCMC::computePriorBeta(int l) {
	// gamma = 0
	if (loci->betas->gammas[l] == 0)
		return log(1 - loci->betas->pi);
	// gamma = 1
	double sumlogMrr = 0;
	for (int d = 0; d < numEnvVar; d++)
		sumlogMrr += log(loci->betas->MrrAndMrs(d, d));

	double sumMrsBetalrInner = 0;
	double sumMrsBetalrOuter = 0;
	for (int s = 0; s < numEnvVar; s++) {
		sumMrsBetalrInner = 0;
		for (int r = s; r < numEnvVar; r++) {
			sumMrsBetalrInner += loci->betas->MrrAndMrs(r, s)
					* loci->betas->theBetas[r][l];
		}
		sumMrsBetalrOuter += sumMrsBetalrInner * sumMrsBetalrInner;
	}

	double prior = numEnvVar * log(1. / loci->betas->m) + sumlogMrr
			- ((float) numEnvVar / 2.0) * log(2 * 3.14158)
			- 0.5 * 1. / loci->betas->m * 1. / loci->betas->m
					* sumMrsBetalrOuter;
	prior += log(loci->betas->pi);
	prior += log(lambdaM) - lambdaM * loci->betas->m;
	prior += log(lambda_for_pi) - lambda_for_pi * loci->betas->pi;
	// TODO: implement prior on mrr and mrs for multi-dimensional cases!
	return prior;
}

double TMCMC::computePriorMu(int l) {
	double priorMu = log(
			randomGenerator->getBetaDensity(logistic(loci->mus[l]),
					loci->alphaF, loci->betaF));
	double priorAlphaF = -loci->alphaF;
	double priorBetaF = -loci->betaF;
	return priorMu + priorAlphaF + priorBetaF;
}

void TMCMC::computePosterior() {
	for (int l = 0; l < numLoci; l++) {
		// likelihood
		double curLL = 0;
		for (int i = 0; i < numIndiv; i++) {
			double f_il = logistic(
					loci->mus[l] + sumBetalXi(i, l) + sumUiVl(i, l));
			curLL += getLogLikelihood(i, l, f_il);
		}
		// prior
		double logBetaPrior = computePriorBeta(l);
		double logMuPrior = computePriorMu(l);
		posterior[l] = logBetaPrior + logMuPrior + curLL;
	}
}

void TMCMC::calcSumOverHyperParamsBeta(double & sumHypersForGammaToZeroUpdate,
		double & sumHypersForGammaToOneUpdate) {
	double Dlogm = numEnvVar * log(1. / loci->betas->m);
	double sumlogMrr = 0;
	for (int d = 0; d < numEnvVar; d++) {
		sumlogMrr += log(loci->betas->MrrAndMrs(d, d));
	}

	double DDiv2logVarPropKernel = ((float) numEnvVar / 2.0)
			* log(widthVarProposalKernelBetaL);

	sumHypersForGammaToZeroUpdate = (-logqOfgamma1Togamma0) - Dlogm - sumlogMrr
			- DDiv2logVarPropKernel;
	sumHypersForGammaToOneUpdate = logqOfgamma1Togamma0 + Dlogm + sumlogMrr
			+ DDiv2logVarPropKernel;

	if (!linkageTrue) { // if no linkage: add the pi to the prior
		double logPi = loci->betas->logPi;
		double logOneMinusPi = log(1 - loci->betas->pi);
		sumHypersForGammaToZeroUpdate += (-logPi) + logOneMinusPi;
		sumHypersForGammaToOneUpdate += logPi - logOneMinusPi;
	}
}

double TMCMC::sumBetalXi(int curIndiv, long curLocus) {
	double sum = 0;
	for (int d = 0; d < numEnvVar; d++) {
		sum += loci->betas->theBetas[d][curLocus]
				* individuals->getXid(curIndiv, d);
	}
	return sum;
}

double TMCMC::sumUiVl(int curIndiv, long curLocus) {
	double sum = 0;
	for (int k = 0; k < numLatFac; k++) {
		sum += individuals->getUik(curIndiv, k) * loci->vs[k][curLocus];
	}
	return sum;
}

double TMCMC::getLogLikelihood(int i, long l, float thisFil) {
	return phredToGTLMap->getLogLikelihood(&genotypePhredScores[l][i * 3],
			thisFil);;
}

void TMCMC::gramSchmidt(int k, double * newUk, int K) {
	// modify vector by Gram-Schmidt to make new vector orthogonal
	double sum;
	for (int j = 0; j < K; j++) { // go over all columns (vectors) of U
		sum = 0;
		if (j != k) {
			for (int i = 0; i < numIndiv; i++) { // calculate dot product of (u_j^T * u_k)
				sum += individuals->getUik(i, j) * newUk[i];
			}
			for (int i = 0; i < numIndiv; i++) { // multiply dot product with u_j and subtract this from u_k
				newUk[i] -= sum * individuals->getUik(i, j);
			}
		}
	}

	// normalize to unit length
	double magUk = 0;
	for (int i = 0; i < numIndiv; i++) {
		magUk += newUk[i] * newUk[i];
	}
	magUk = sqrt(magUk);
	for (int i = 0; i < numIndiv; i++) {
		newUk[i] /= magUk;
	}
}

void TMCMC::addToHypers() {
	arma::mat fullM = 1. / loci->betas->m * loci->betas->MrrAndMrs;
	SigmaBetaSum += inv(fullM * fullM.t());

	alphaFSum += loci->alphaF;
	betaFSum += loci->betaF;
	if (linkageTrue) {
		lambda_1_sum += hmmUpdater->getLambda1();
		lambda_2_sum += hmmUpdater->getLambda2();
	} else
		piSum += loci->betas->pi;
}

void TMCMC::addToUV() {
	for (int k = 0; k < numLatFac; k++) {
		for (int i = 0; i < numIndiv; i++)
			sumU[k][i] += individuals->getUik(i, k);
	}
	for (int k = 0; k < numLatFac; k++) {
		for (int l = 0; l < numLoci; l++)
			sumV[k][l] += loci->getOneVkl(k, l);
	}
}

///////////////////////////////////
//////// acceptance rates /////////
///////////////////////////////////

double TMCMC::calculateAcceptanceRate(int numAccepted, int length) {
	if (numAccepted == 0)
		numAccepted = 1.;
	if (length == 0) {
		numAccepted = 0.;
		length = 1.;
	}
	return (double) numAccepted / (double) length;
}

void TMCMC::adjustProposalWidth(int numAccepted, int length,
		float & curProposalWidth) {
	curProposalWidth *= calculateAcceptanceRate(numAccepted, length) * 3;
}

void TMCMC::adjustAllProposalWidth(int length, bool RJMCMC) {
	adjustProposalWidth(numAcceptedAlphaF, length,
			widthProposalKernelLogAlphaF);
	adjustProposalWidth(numAcceptedBetaF, length, widthProposalKernelLogBetaF);
	adjustProposalWidth(numAcceptedVkl, numLoci * numLatFac * length,
			widthProposalKernelVkl);
	adjustProposalWidth(numAcceptedUk, numLatFac * length,
			widthProposalKernelUk);
	adjustProposalWidth(numAcceptedLittleM, length, widthProposalKernelLittleM);
	adjustProposalWidth(numAcceptedMrr, length * numEnvVar,
			widthProposalKernelMrr);
	adjustProposalWidth(numAcceptedMrs,
			length * numEnvVar * (numEnvVar + 1) / 2, widthProposalKernelMrs);
	if (linkageTrue) {
		adjustProposalWidth(numAcceptedLambda1, length,
				widthProposalKernelLambda1);
		adjustProposalWidth(numAcceptedLambda2, length,
				widthProposalKernelLambda2);
	} else {
		if (RJMCMC) // only update proposal width of pi when being in RJMCMC
			adjustProposalWidth(numAcceptedPi, length, widthProposalKernelPi);
	}

	for (long l = 0; l < numLoci; l++) {
		adjustProposalWidth(numAcceptedFPerLocus[l], length,
				widthProposalKernelF[l]);
		if (widthProposalKernelF[l] > 0.25) // avoid too large proposal widths
			widthProposalKernelF[l] = 0.25;
	}
	if (!RJMCMC) { // only update proposal width of betas when not in RJMCMC
		for (int d = 0; d < numEnvVar; d++) {
			for (long l = 0; l < numLoci; l++) {
				adjustProposalWidth(numAcceptedBetaPerLD[d][l],
						totalNumUpdatesBetaPerLocus[d][l] + 1,
						widthProposalKernelBetadl[d][l]);
			}
		}
	}
}

void TMCMC::concludeAcceptanceRate(int numAccepted, int length,
		std::string name) {
	double acceptanceRate = calculateAcceptanceRate(numAccepted, length);
	logfile->conclude(
			"Acceptance rate " + name + " = " + toString(acceptanceRate));
}

void TMCMC::concludeAcceptanceRates(int length) {
	concludeAcceptanceRate(numAcceptedAlphaF, length, "alphaF");
	concludeAcceptanceRate(numAcceptedBetaF, length, "betaF");

	concludeAcceptanceRate(numAcceptedVkl, numLoci * numLatFac * length,
			"v_kl");
	concludeAcceptanceRate(numAcceptedUk, numLatFac * length, "u_k");

	concludeAcceptanceRate(numAcceptedLittleM, length, "m");
	concludeAcceptanceRate(numAcceptedMrr, length * numEnvVar, "m_rr");
	concludeAcceptanceRate(numAcceptedMrs,
			length * numEnvVar * (numEnvVar + 1) / 2, "m_rs");
	concludeAcceptanceRate(numAcceptedGamma, gammaTotalNumTrialsCounter,
			"gamma");
	if (linkageTrue) {
		concludeAcceptanceRate(numAcceptedLambda1, length, "Lambda_1");
		concludeAcceptanceRate(numAcceptedLambda2, length, "Lambda_2");
	} else
		concludeAcceptanceRate(numAcceptedPi, length, "pi");
}

///////////////////////////////////
////////// output files ///////////
///////////////////////////////////

void TMCMC::processPostMeanSigmaBeta() {
	int numUpdates = numIterations / files.thinning; // integer division on purpose!
	postMeanSigmaBeta = SigmaBetaSum / (float) numUpdates;
}

///////////////////////////////////
/////////// storage ///////////////
///////////////////////////////////

void TMCMC::initializeStorageAcceptanceRates() {
	numAcceptedFPerLocus = new int[numLoci];
	numAcceptedBetaPerLD = new uint32_t*[numEnvVar];
	totalNumUpdatesBetaPerLocus = new uint32_t *[numEnvVar];
	for (int d = 0; d < numEnvVar; d++) {
		numAcceptedBetaPerLD[d] = new uint32_t[numLoci];
		totalNumUpdatesBetaPerLocus[d] = new uint32_t[numLoci];
	}
}

void TMCMC::initializeStorageBetas() {
	if (!storageBetasInitialized) {
		expValBetadl = new double*[numEnvVar]; // store expected value
		expValSquaredBetadl = new double*[numEnvVar]; // store expected value ^2 (to get variance)
		betaUpdateCounter = new uint32_t*[numEnvVar]; // store how many times a given beta_dl was updated
		for (int d = 0; d < numEnvVar; d++) {
			expValBetadl[d] = new double[numLoci];
			expValSquaredBetadl[d] = new double[numLoci];
			betaUpdateCounter[d] = new uint32_t[numLoci];
			for (long l = 0; l < numLoci; l++) {
				expValBetadl[d][l] = 0;
				expValSquaredBetadl[d][l] = 0;
				betaUpdateCounter[d][l] = 0;
			}
		}
		gammaCounter = new uint32_t[numLoci]; // store number of times a gamma could be updated
		gammaSum = new float[numLoci]; // store gamma posterior
		for (long l = 0; l < numLoci; l++) {
			gammaSum[l] = 0;
			gammaCounter[l] = 0;
		}
		storageBetasInitialized = true;
	}
}

void TMCMC::initializeStorageUV() {
	sumU = new double*[numLatFac];
	for (int k = 0; k < numLatFac; k++) {
		sumU[k] = new double[numIndiv];
		for (int i = 0; i < numIndiv; i++)
			sumU[k][i] = 0.0; // initialize to zero
	}
	sumV = new double*[numLatFac];
	for (int k = 0; k < numLatFac; k++) {
		sumV[k] = new double[numLoci];
		for (int l = 0; l < numLoci; l++)
			sumV[k][l] = 0.0; // initialize to zero
	}
}

void TMCMC::initializeStorageHypers() {
	SigmaBetaSum.zeros(numEnvVar, numEnvVar);

	alphaFSum = 0;
	betaFSum = 0;
	if (linkageTrue) {
		lambda_1_sum = 0;
		lambda_2_sum = 0;
	} else
		piSum = 0;
}

void TMCMC::initializeStorageMus() {
	if (!storageMusInitialized) {
		expValMul = new double[numLoci]; // store expected value
		expValSquaredMul = new double[numLoci]; // store expected value ^2 (to get variance)
		for (long l = 0; l < numLoci; l++) {
			expValMul[l] = 0;
			expValSquaredMul[l] = 0;
		}
		storageMusInitialized = true;
	}
}

void TMCMC::concludeExpAndVarBetas() {
	for (int d = 0; d < numEnvVar; d++) {
		for (long l = 0; l < numLoci; l++) {
			expValBetadl[d][l] /= ((float) betaUpdateCounter[d][l] + 1); // normalize E(X) (+1 in denominator to avoid division-by-zero error)
			expValSquaredBetadl[d][l] /= ((float) betaUpdateCounter[d][l] + 1); // normalize E(X^2)
			// var(x) = E(X^2) - E(X) * E(X)
			expValSquaredBetadl[d][l] -= (expValBetadl[d][l]
					* expValBetadl[d][l]); // store variance in same array as previously expSquared
			if (expValSquaredBetadl[d][l] < 0) {
				expValSquaredBetadl[d][l] = 0.00000000000000001; // impute very small value
			}
		}
	}
	for (long l = 0; l < numLoci; l++) {
		gammaSum[l] /= ((float) gammaCounter[l] + 1.);
	}
}

void TMCMC::concludeExpAndVarMus() {
	for (long l = 0; l < numLoci; l++) {
		expValMul[l] /= (float) numIterations; // normalize E(X)
		expValSquaredMul[l] /= (float) numIterations; // normalize E(X^2)
		// var(x) = E(X^2) - E(X) * E(X)
		expValSquaredMul[l] -= (expValMul[l] * expValMul[l]); // store variance in same array as previously expSquared
	}
}

void TMCMC::concludeExpValHypers() {
	// expected values
	int numUpdates = numIterations / files.thinning; // integer division on purpose!
	alphaFSum /= (float) numUpdates;
	betaFSum /= (float) numUpdates;
	if (linkageTrue) {
		lambda_1_sum /= (float) numUpdates;
		lambda_2_sum /= (float) numUpdates;
	} else
		piSum /= (float) numUpdates;
}

void TMCMC::concludeExpValUV() {
	int numUpdates = numIterations / files.thinning; // integer division on purpose!
	// expected values
	for (int k = 0; k < numLatFac; k++) {
		for (int i = 0; i < numIndiv; i++)
			sumU[k][i] /= (float) numUpdates;
	}
	// expected values
	for (int k = 0; k < numLatFac; k++) {
		for (int l = 0; l < numLoci; l++)
			sumV[k][l] /= (float) numUpdates;
	}
}

///////////////////////////////////
//////////// tidy up //////////////
///////////////////////////////////

void TMCMC::clearCounters() {
	numAcceptedAlphaF = 0;
	numAcceptedBetaF = 0;

	numAcceptedVkl = 0;
	numAcceptedUk = 0;

	if (linkageTrue) {
		numAcceptedLambda1 = 0;
		numAcceptedLambda2 = 0;
	} else
		numAcceptedPi = 0;

	numAcceptedLittleM = 0;
	numAcceptedMrr = 0;
	numAcceptedMrs = 0;
	numAcceptedGamma = 0;
	betaTotalNumTrialsCounter = 0;
	gammaTotalNumTrialsCounter = 0;
	for (long l = 0; l < numLoci; l++) {
		numAcceptedFPerLocus[l] = 0;
		for (int d = 0; d < numEnvVar; d++) {
			numAcceptedBetaPerLD[d][l] = 0;
			totalNumUpdatesBetaPerLocus[d][l] = 0;
		}
	}
}

///////////////////////////////////////////

TWriteMCMCFiles::TWriteMCMCFiles() :
		logfile(nullptr), loci(nullptr), individuals(nullptr), prefix(""), traceFileMu(
				nullptr), traceFileGamma(nullptr), traceFileHypers(nullptr), traceFileV(
				nullptr), traceFileU(nullptr), traceFileBeta(nullptr), betaProposalFile(
				nullptr), betaAcceptanceFile(nullptr), traceFileLL(nullptr), traceFilePosterior(
				nullptr), linkageTrue(true), thinning(0), writeMuTrace(false), writeGammaTrace(
				false), writeHyperTrace(false), writeBetaTrace(false), writeVTrace(
				false), writeUTrace(false), writeHyperFinal(false), writeUVFinal(
				false), writeMuFinal(false), writeLastIter(false), writeBetaProposal(
				false), writeBetaAcceptance(false), writeAfterInit(false), writeDuringBurnin(
				false), writeLLTrace(false), writePosteriorTrace(false) {
}

TWriteMCMCFiles::~TWriteMCMCFiles() {
	closeOutputFiles();
}

void TWriteMCMCFiles::initialize(TLog * Logfile, TLoci * Loci, bool LinkageTrue,
		TIndividuals * Individuals) {
	logfile = Logfile;
	loci = Loci;
	linkageTrue = LinkageTrue;
	individuals = Individuals;
}

void TWriteMCMCFiles::readFileParameters(TParameters& Parameters, int numLoci,
		int numIterations) {
	// outputname
	prefix = Parameters.getParameterStringWithDefault("out", "");
	if (prefix == "") {
		// guess from filename
		prefix = Parameters.getParameterString("vcf");
		prefix = extractBefore(prefix, ".");
	}

	logfile->list(
			"Will write MCMC output files with the tag '" + prefix + "'.");

	// write trace files?
	if (Parameters.getParameterStringWithDefault("writeMuTrace", "false")
			== "true") { // mu
		writeMuTrace = true;
		logfile->startIndent(
				"Will print the trace of mu of the following loci to file:");
		// which loci?
		std::vector<std::string> lociNamesToPrint;
		std::string tmpForLogfile; // only write loci names to logfile that are actually written to file
		std::string tmp = Parameters.getParameterStringWithDefault(
				"lociNamesToPrint", "");
		if (tmp == "" || tmp == "all") {
			logfile->list("Will print the trace of mu of all loci to file.");
			lociIndicesToPrint.resize(numLoci); // set size
			std::iota(std::begin(lociIndicesToPrint),
					std::end(lociIndicesToPrint), 0); // fill lociIndicesToPrint with all indices
		} else {
			fillVectorFromString(tmp, lociNamesToPrint, ',');
			// find indices of those loci names
			for (std::vector<std::string>::iterator locus =
					lociNamesToPrint.begin(); locus < lociNamesToPrint.end();
					locus++) {
				int indexLocus = std::distance(loci->names.begin(),
						find(loci->names.begin(), loci->names.end(), *locus));
				if (indexLocus >= (int) loci->names.size())
					logfile->list(
							"WARNING: Locus " + *locus
									+ " does not exist! Be sure to use format chr_posOnChr. It is possible that locus was filtered out.");
				else {
					lociIndicesToPrint.push_back(indexLocus);
					tmpForLogfile += *locus;
					tmpForLogfile += ", ";
				}
			}
			logfile->list(tmpForLogfile);
		}
		logfile->endIndent();
	}

	if (Parameters.getParameterStringWithDefault("writeHyperTrace", "false")
			== "true") { // hyperparameters
		writeHyperTrace = true;
		logfile->list("Will print the trace of the hyperparameters to file!");
	}

	if (Parameters.getParameterStringWithDefault("writeGammaTrace", "false")
			== "true") { // gamma
		writeGammaTrace = true;
		logfile->list("Will print the trace of gamma to file!");
	}

	if (Parameters.getParameterStringWithDefault("writeBetaTrace", "false")
			== "true") { // beta
		writeBetaTrace = true;
		logfile->list("Will print the trace of beta_{d=0} to file!");
	}

	if (Parameters.getParameterStringWithDefault("writeVTrace", "false")
			== "true") { // v
		writeVTrace = true;
		logfile->list("Will print the trace of v_{k=0} to file!");
	}

	if (Parameters.getParameterStringWithDefault("writeUTrace", "false")
			== "true") { // u
		writeUTrace = true;
		logfile->list("Will print the trace of u_{k=0} to file!");
	}

	if (Parameters.getParameterStringWithDefault("writeLLTrace", "false")
			== "true") { // write out the traces of parameters in burnins as well
		writeLLTrace = true;
		logfile->list("Will print the full log likelihood to file!");
	}

	if (Parameters.getParameterStringWithDefault("writePosteriorTrace", "false")
			== "true") { // write out the traces of parameters in burnins as well
		writePosteriorTrace = true;
		logfile->list("Will print the posterior to file!");
	}

	if (Parameters.getParameterStringWithDefault("writeAfterInit", "false")
			== "true") { // write out the traces of parameters right after initialization?
		writeAfterInit = true;
		logfile->list(
				"Will print out the traces of all specified parameters after initialization!");
	}

	if (Parameters.getParameterStringWithDefault("writeBurnins", "false")
			== "true") { // write out the traces of parameters in burnins as well
		writeDuringBurnin = true;
		logfile->list(
				"Will print out the traces of all specified parameters during the burnins as well!");
	}

	// write burnin files?
	if (Parameters.getParameterStringWithDefault("writeBetaProposal", "false")
			== "true") { // beta
		writeBetaProposal = true;
		logfile->list("Will print the proposal kernels of beta to file!");
	}
	if (Parameters.getParameterStringWithDefault("writeBetaAcceptance", "false")
			== "true") { // beta
		writeBetaAcceptance = true;
		logfile->list("Will print the acceptance rates of beta to file!");
	}

	// write final files?
	if (Parameters.getParameterStringWithDefault("writeMuFinal", "false")
			== "true") { // mu
		writeMuFinal = true;
		logfile->list("Will print the final expected values of mu to file!");
	}
	if (Parameters.getParameterStringWithDefault("writeUVFinal", "false")
			== "true") { // U
		writeUVFinal = true;
		logfile->list(
				"Will print the final expected values of U and V to file!");
	}
	if (Parameters.getParameterStringWithDefault("writeHyperFinal", "false")
			== "true") { // Hyper
		writeHyperFinal = true;
		logfile->list(
				"Will print the final expected values of the hyperparameters to file!");
	}
	if (Parameters.getParameterStringWithDefault("writeLastIter", "false")
			== "true") { // last iteration
		writeLastIter = true;
		logfile->list(
				"Will print the values of all parameters of the final iteration to file!");
	}

	// thinning?
	thinning = Parameters.getParameterIntWithDefault("thinning", 5);

	if (thinning < 1 || thinning > numIterations)
		throw "Thinning must be > 1 and < number iterations!";

	if (thinning > 1) {
		if (thinning == 2)
			logfile->list(
					"Will print every second iteration to the output file (thinning = 2).");
		else if (thinning == 3)
			logfile->list(
					"Will print every third iteration to the output file (thinning = 3).");
		else
			logfile->list(
					"Will print every " + toString(thinning)
							+ "th iterations to the output file (thinning = "
							+ toString(thinning) + ").");
	}
}

///// Trace files

void TWriteMCMCFiles::prepareTraceFiles(int numEnvVar, int numIndiv) {
	//open MCMC trace files
	if (writeMuTrace)
		prepareMuTraceFile();
	if (writeGammaTrace)
		prepareGammaTraceFile();
	if (writeHyperTrace)
		prepareHyperTraceFile(numEnvVar);
	if (writeBetaTrace)
		prepareBetaTraceFile();
	if (writeVTrace)
		prepareVTraceFile();
	if (writeUTrace)
		prepareUTraceFile(numIndiv);
	if (writeLLTrace)
		prepareLLTraceFile();
	if (writePosteriorTrace)
		preparePosteriorTraceFile();

	logfile->endIndent();
}

void TWriteMCMCFiles::prepareMuTraceFile() {
	traceFileMu = new TOutputFilePlain(prefix + "_traceMu.txt");

	// header
	std::vector<std::string> header { };
	for (std::vector<long>::iterator locus = lociIndicesToPrint.begin();
			locus < lociIndicesToPrint.end(); locus++) {
		header.push_back(loci->getName(*locus));
	}
	traceFileMu->writeHeader(header);
}

void TWriteMCMCFiles::prepareGammaTraceFile() {
	traceFileGamma = new TOutputFilePlain(prefix + "_traceGamma.txt");
	traceFileGamma->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::prepareBetaTraceFile() {
	traceFileBeta = new TOutputFilePlain(prefix + "_traceBeta.txt");
	traceFileBeta->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::prepareVTraceFile() {
	traceFileV = new TOutputFilePlain(prefix + "_traceV.txt");
	traceFileV->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::prepareUTraceFile(int numIndiv) {
	traceFileU = new TOutputFilePlain(prefix + "_traceU.txt");

	// header
	std::vector<std::string> header { };
	for (int i = 0; i < numIndiv; i++)
		header.push_back(individuals->getName_i(i));
	traceFileU->writeHeader(header);
}

void TWriteMCMCFiles::prepareHyperTraceFile(int numEnvVar) {
	traceFileHypers = new TOutputFilePlain(prefix + "_traceHypers.txt");
	std::vector<std::string> header { "m", "alphaF", "betaF" };

	if (linkageTrue) {
		header.push_back("lambda_1");
		header.push_back("lambda_2");
	} else
		header.push_back("pi");

	for (int r = 0; r < numEnvVar; r++) {
		for (int s = 0; s < numEnvVar; s++) {
			header.push_back("m" + toString(r) + toString(s));
		}
	}
	traceFileHypers->writeHeader(header);
}

void TWriteMCMCFiles::prepareLLTraceFile() {
	traceFileLL = new TOutputFilePlain(prefix + "_traceLL.txt");
	traceFileLL->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::preparePosteriorTraceFile() {
	traceFilePosterior = new TOutputFilePlain(prefix + "_tracePosterior.txt");
	traceFilePosterior->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::writeTraceFiles(int numEnvVar, THMMUpdater * hmmUpdater,
		int numLoci, int numIndiv, double * ll, double * posterior) {
	if (writeMuTrace)
		writeTraceMuToFile();
	if (writeGammaTrace)
		writeTraceGammaToFile(numLoci);
	if (writeHyperTrace)
		writeTraceHypersToFile(numEnvVar, hmmUpdater);
	if (writeBetaTrace)
		writeTraceBetaToFile(numLoci);
	if (writeVTrace)
		writeTraceVToFile(numLoci);
	if (writeUTrace)
		writeTraceUToFile(numIndiv);
	if (writeLLTrace)
		writeTraceLLToFile(numLoci, ll);
	if (writePosteriorTrace)
		writeTracePosteriorToFile(numLoci, posterior);
}

void TWriteMCMCFiles::writeTraceMuToFile() {
	for (std::vector<long>::iterator locus = lociIndicesToPrint.begin();
			locus < lociIndicesToPrint.end(); locus++)
		*(traceFileMu) << loci->mus[*locus];
	traceFileMu->endLine();
}

void TWriteMCMCFiles::writeTraceHypersToFile(int numEnvVar,
		THMMUpdater * hmmUpdater) {
	*(traceFileHypers) << loci->betas->m << loci->alphaF << loci->betaF;
	if (linkageTrue)
		*(traceFileHypers) << hmmUpdater->getLambda1()
				<< hmmUpdater->getLambda2();
	else
		*(traceFileHypers) << loci->betas->pi;
	for (int r = 0; r < numEnvVar; r++) {
		for (int s = 0; s < numEnvVar; s++)
			*(traceFileHypers) << loci->betas->MrrAndMrs(r, s);
	}
	traceFileHypers->endLine();
}

void TWriteMCMCFiles::writeTraceGammaToFile(int numLoci) {
	for (int locus = 0; locus < numLoci; locus++)
		*(traceFileGamma) << loci->betas->gammas[locus];
	traceFileGamma->endLine();
}

void TWriteMCMCFiles::writeTraceBetaToFile(int numLoci) {
	for (int locus = 0; locus < numLoci; locus++)
		*(traceFileBeta) << loci->betas->theBetas[0][locus];
	traceFileBeta->endLine();
}

void TWriteMCMCFiles::writeTraceVToFile(int numLoci) {
	for (int locus = 0; locus < numLoci; locus++)
		*(traceFileV) << loci->vs[0][locus];
	traceFileV->endLine();
}

void TWriteMCMCFiles::writeTraceUToFile(int numIndiv) {
	for (int i = 0; i < numIndiv; i++)
		*(traceFileU) << individuals->getUik(i, 0);
	traceFileU->endLine();
}

void TWriteMCMCFiles::writeTraceLLToFile(int numLoci, double * ll) {
	for (int locus = 0; locus < numLoci; locus++)
		*(traceFileLL) << ll[locus];
	traceFileLL->endLine();
}

void TWriteMCMCFiles::writeTracePosteriorToFile(int numLoci,
		double * posterior) {
	for (int locus = 0; locus < numLoci; locus++)
		*(traceFilePosterior) << posterior[locus];
	traceFilePosterior->endLine();
}

// burnin files
void TWriteMCMCFiles::prepareBetaAcceptanceFile() {
	betaAcceptanceFile = new TOutputFilePlain(prefix + "_betaAcceptance.txt");
	betaAcceptanceFile->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::prepareBurninFiles() {
	if (writeBetaProposal)
		prepareBetaProposalFile();
	if (writeBetaAcceptance)
		prepareBetaAcceptanceFile();
}

void TWriteMCMCFiles::prepareBetaProposalFile() {
	betaProposalFile = new TOutputFilePlain(prefix + "_betaProposal.txt");
	betaProposalFile->writeHeader(loci->getNames());
}

void TWriteMCMCFiles::writeBurninFiles(int numLoci,
		float ** widthProposalKernelBetadl, uint32_t ** numAcceptedBetaPerLD,
		uint32_t ** totalNumUpdatesBetaPerLocus) {
	if (writeBetaProposal)
		writeBetaProposalToFile(numLoci, widthProposalKernelBetadl);
	if (writeBetaAcceptance)
		writeBetaAcceptanceToFile(numLoci, numAcceptedBetaPerLD,
				totalNumUpdatesBetaPerLocus);
}

void TWriteMCMCFiles::writeBetaProposalToFile(int numLoci,
		float ** widthProposalKernelBetadl) {
	for (int locus = 0; locus < numLoci; locus++)
		*(betaProposalFile) << widthProposalKernelBetadl[0][locus];
	betaProposalFile->endLine();
}

void TWriteMCMCFiles::writeBetaAcceptanceToFile(int numLoci,
		uint32_t ** numAcceptedBetaPerLD,
		uint32_t ** totalNumUpdatesBetaPerLocus) {
	for (int locus = 0; locus < numLoci; locus++) {
		if (totalNumUpdatesBetaPerLocus[0][locus] == 0)
			*(betaAcceptanceFile)
					<< static_cast<double>(numAcceptedBetaPerLD[0][locus])
							/ static_cast<double>((totalNumUpdatesBetaPerLocus[0][locus]
									+ 1));
		else
			*(betaAcceptanceFile)
					<< static_cast<double>(numAcceptedBetaPerLD[0][locus])
							/ static_cast<double>(totalNumUpdatesBetaPerLocus[0][locus]);
	}
	betaAcceptanceFile->endLine();
}

/////// Final files

void TWriteMCMCFiles::writeToBetaGammaFile(int numEnvVar,
		std::vector<std::string> & envVarNames, int numLoci, float * gammaSum,
		uint32_t * gammaCounter, double ** expValBetadl,
		double ** expValSquaredBetadl, uint32_t ** betaUpdateCounter) {

	TOutputFilePlain finalFileBetaGamma(prefix + "_expValBetaGamma.txt");

	// header
	std::vector<std::string> header { "", "E(gamma_l)", "GammaCounter" };
	for (int d = 0; d < numEnvVar; d++)
		header.push_back("E(beta_l)_of_" + envVarNames[d]);
	for (int d = 0; d < numEnvVar; d++)
		header.push_back("Var(beta_l)_of_" + envVarNames[d]);
	for (int d = 0; d < numEnvVar; d++)
		header.push_back("BetaUpdateCounter_of_" + envVarNames[d]);
	finalFileBetaGamma.writeHeader(header);

	// data
	for (long l = 0; l < numLoci; l++) {
		finalFileBetaGamma << loci->getName(l) << gammaSum[l]
				<< gammaCounter[l];
		for (int d = 0; d < numEnvVar; d++)
			finalFileBetaGamma << expValBetadl[d][l];
		for (int d = 0; d < numEnvVar; d++)
			finalFileBetaGamma << expValSquaredBetadl[d][l];
		for (int d = 0; d < numEnvVar; d++)
			finalFileBetaGamma << betaUpdateCounter[d][l];
		finalFileBetaGamma.endLine();
	}
	finalFileBetaGamma.close();
}

void TWriteMCMCFiles::writeFinalMuToFile(int numLoci, double * expValMul,
		double * expValSquaredMul) {
	TOutputFilePlain finalFileMu(prefix + "_finalMu.txt");

	// header
	std::vector<std::string> header { "", "E(mu_l)", "Var(mu_l)" };
	finalFileMu.writeHeader(header);

	// exp and var
	for (long l = 0; l < numLoci; l++) {
		finalFileMu << loci->getName(l) << expValMul[l] << expValSquaredMul[l];
		finalFileMu.endLine();
	}
	finalFileMu.close();
}

void TWriteMCMCFiles::writeFinalHyperToFile(int numEnvVar,
		std::vector<std::string> & envVarNames, arma::mat & postMeanSigmaBeta,
		double alphaFSum, double betaFSum, double piSum, double lambda_1_sum,
		double lambda_2_sum) {
	TOutputFilePlain finalFileHypers(prefix + "_finalHypers.txt");

	// header
	std::vector<std::string> header { "" };
	for (int d = 0; d < numEnvVar; d++)
		header.push_back("Sigma_beta_" + envVarNames[d]);
	header.push_back("alpha_f");
	header.push_back("beta_f");
	if (linkageTrue) {
		header.push_back("lambda_1");
		header.push_back("lambda_2");
	} else
		header.push_back("pi");
	finalFileHypers.writeHeader(header);

	// data
	finalFileHypers << ("Sigma_beta_" + envVarNames[0]);
	for (int d1 = 0; d1 < numEnvVar; d1++) {
		finalFileHypers << postMeanSigmaBeta(0, d1);
	}
	finalFileHypers << alphaFSum << betaFSum;
	if (linkageTrue)
		finalFileHypers << lambda_1_sum << lambda_2_sum;
	else
		finalFileHypers << piSum;
	finalFileHypers.endLine();

	// other rows
	for (int d1 = 1; d1 < numEnvVar; d1++) {
		finalFileHypers << ("Sigma_beta_" + envVarNames[d1]);
		for (int d2 = 0; d2 < numEnvVar; d2++)
			finalFileHypers << postMeanSigmaBeta(d1, d2);
		if (linkageTrue)
			finalFileHypers << "" << "" << "" << ""; // to have same column number
		else
			finalFileHypers << "" << "" << ""; // to have same column number
		finalFileHypers.endLine();
	}
	finalFileHypers.close();
}

void TWriteMCMCFiles::writeFinalUVToFile(int numLatFac, int numIndiv,
		int numLoci, TIndividuals * individuals, TLoci * loci, double ** sumU,
		double ** sumV) {
	writeFinalUToFile(numLatFac, numIndiv, individuals, sumU);
	writeFinalVToFile(numLatFac, numLoci, loci, sumV);
}

void TWriteMCMCFiles::writeFinalUToFile(int numLatFac, int numIndiv,
		TIndividuals * individuals, double ** sumU) {
	TOutputFilePlain finalFileU(prefix + "_finalU.txt");

	// header
	std::vector<std::string> header { "" };
	for (int k = 0; k < numLatFac; k++) {
		header.push_back("k_" + toString(k));
	}
	finalFileU.writeHeader(header);

	// expected values
	for (int i = 0; i < numIndiv; i++) {
		finalFileU << individuals->getName_i(i);
		for (int k = 0; k < numLatFac; k++)
			finalFileU << sumU[k][i];
		finalFileU.endLine();
	}
	finalFileU.close();
}

void TWriteMCMCFiles::writeFinalVToFile(int numLatFac, int numLoci,
		TLoci * loci, double ** sumV) {
	TOutputFilePlain finalFileV(prefix + "_finalV.txt");

	// header
	std::vector<std::string> header { "" };
	std::vector<std::string> lociNames = loci->getNames();
	header.insert(header.end(), lociNames.begin(), lociNames.end());
	finalFileV.writeHeader(header);

	// expected values
	for (int k = 0; k < numLatFac; k++) {
		finalFileV << "k_" + toString(k);
		for (int l = 0; l < numLoci; l++)
			finalFileV << sumV[k][l];
		finalFileV.endLine();
	}
	finalFileV.close();
}

void TWriteMCMCFiles::constructHeaderLastIterToFile(
		std::vector<std::string> & header, int numEnvVar, int numLoci,
		int numLatFac, int numIndiv, bool linkage) {
	// header
	if (linkage) {
		header.push_back("lambda_1");
		header.push_back("lambda_2");
	} else
		header.push_back("pi");
	header.push_back("m");
	for (int r = 0; r < numEnvVar; r++) {
		for (int s = 0; s < numEnvVar; s++)
			header.push_back("m_r" + toString(r) + "_s" + toString(s));
	}
	header.push_back("alpha_f");
	header.push_back("beta_f");
	for (int l = 0; l < numLoci; l++)
		header.push_back("gamma_l" + loci->getName(l));
	for (int l = 0; l < numLoci; l++) {
		for (int d = 0; d < numEnvVar; d++)
			header.push_back("beta_l" + loci->getName(l) + "_d" + toString(d));
	}
	for (int l = 0; l < numLoci; l++)
		header.push_back("mu_l" + loci->getName(l));
	for (int l = 0; l < numLoci; l++) {
		for (int k = 0; k < numLatFac; k++)
			header.push_back("v_l" + loci->getName(l) + "_k" + toString(k));
	}
	for (int i = 0; i < numIndiv; i++) {
		for (int k = 0; k < numLatFac; k++)
			header.push_back(
					"u_i" + individuals->getName_i(i) + "_k" + toString(k));
	}
}

void TWriteMCMCFiles::writeLastIterToFile(int numEnvVar, int numLoci,
		int numLatFac, int numIndiv, double pi, THMMUpdater * hmmUpdater,
		double m, arma::mat MrrAndMrs, double alpha_f, double beta_f,
		unsigned short * gamma, float ** betas, float * mus, float ** vs,
		TIndividuals * individuals, bool linkage) {
	// file structure:
	// (linkage) lambda1 - lambda2 - m - m_r0_s0 - m_r0_s1 - ... - m_rD_sD - alpha_f - beta_f - gamma_l - beta_l0_d0 - beta_l0_d1 - ... - beta_lL_dD - mu_l - v_l0_k0 - v_l0_k1 - ... - v_lL_kK - ... - u_i0_k0 - u_i0_k1 - ... - u_iN_kK
	// (no linkage) pi - m - m_r0_s0 - m_r0_s1 - ... - m_rD_sD - alpha_f - beta_f - gamma_l - beta_l0_d0 - beta_l0_d1 - ... - beta_lL_dD - mu_l - v_l0_k0 - v_l0_k1 - ... - v_lL_kK - ... - u_i0_k0 - u_i0_k1 - ... - u_iN_kK

	TOutputFilePlain lastIterFile(prefix + "_lastIter.txt");

	// header
	std::vector<std::string> header;
	constructHeaderLastIterToFile(header, numEnvVar, numLoci, numLatFac,
			numIndiv, linkage);
	lastIterFile.writeHeader(header);

	// data
	if (linkage)
		lastIterFile << hmmUpdater->getLambda1() << hmmUpdater->getLambda2();
	else
		lastIterFile << pi;
	lastIterFile << m;
	for (int r = 0; r < numEnvVar; r++) {
		for (int s = 0; s < numEnvVar; s++)
			lastIterFile << MrrAndMrs(r, s);
	}
	lastIterFile << alpha_f << beta_f;
	for (int l = 0; l < numLoci; l++)
		lastIterFile << gamma[l];
	for (int l = 0; l < numLoci; l++) {
		for (int d = 0; d < numEnvVar; d++)
			lastIterFile << betas[d][l];
	}
	for (int l = 0; l < numLoci; l++)
		lastIterFile << mus[l];

	for (int l = 0; l < numLoci; l++) {
		for (int k = 0; k < numLatFac; k++)
			lastIterFile << vs[k][l];
	}
	for (int i = 0; i < numIndiv; i++) {
		for (int k = 0; k < numLatFac; k++)
			lastIterFile << individuals->getUik(i, k);
	}
	lastIterFile.close();
}

void TWriteMCMCFiles::closeOutputFiles() {
	if (writeMuTrace && traceFileMu != nullptr) {
		traceFileMu->close();
		delete traceFileMu;
	}
	if (writeHyperTrace && traceFileHypers != nullptr) {
		traceFileHypers->close();
		delete traceFileHypers;
	}
	if (writeGammaTrace && traceFileGamma != nullptr) {
		traceFileGamma->close();
		delete traceFileGamma;
	}
	if (writeBetaTrace && traceFileBeta != nullptr) {
		traceFileBeta->close();
		delete traceFileBeta;
	}
	if (writeVTrace && traceFileV != nullptr) {
		traceFileV->close();
		delete traceFileV;
	}
	if (writeUTrace && traceFileU != nullptr) {
		traceFileU->close();
		delete traceFileU;
	}
	if (writeBetaAcceptance && betaAcceptanceFile != nullptr) {
		betaAcceptanceFile->close();
		delete betaAcceptanceFile;
	}
	if (writeBetaProposal && betaProposalFile != nullptr) {
		betaProposalFile->close();
		delete betaProposalFile;
	}
	if (writeLLTrace && traceFileLL != nullptr) {
		traceFileLL->close();
		delete traceFileLL;
	}
	if (writePosteriorTrace && traceFilePosterior != nullptr) {
		traceFilePosterior->close();
		delete traceFilePosterior;
	}
}

///////////////////////////////////
///// functions for debug only ////
///////////////////////////////////

void TMCMC::assertThatVectorsAreOrthogonal() { // TODO: move this function to testing utilities!
	// idea: dot product of u_k * u_j should be zero if vectors are orthogonal
	// and this should be true for all possible combinations of k and j
	double sum;
	int counterApproxEqual = 0;
	int counterTotal = 0;
	for (int k = 0; k < numLatFac; k++) {
		for (int j = k + 1; j < numLatFac; j++) {
			sum = 0;
			for (int i = 0; i < numIndiv; i++) { // calculate dot product of (u_j^T * u_k)
				sum += individuals->getUik(i, j) * individuals->getUik(i, k);
			}
			if (std::fabs(sum) < 0.000000000001) {
				counterApproxEqual++;
			}
			counterTotal++;
		}
	}
	if (counterApproxEqual != counterTotal) {
		logfile->list("WARNING: the vectors u_k are not orthogonal anymore!");
	}
}
