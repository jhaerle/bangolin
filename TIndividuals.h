/*
 * TIndividuals.h
 *
 *  Created on: Oct 25, 2018
 *      Author: caduffm
 */

#ifndef TINDIVIDUALS_H_
#define TINDIVIDUALS_H_

#include <vector>
#include <string>
#include "TIndividual.h"
#include <iostream>

class TIndividuals {
private:
	std::vector<TIndividual> individuals;

public:
	void addIndividual(TIndividual);
	void initializeUi(int, std::vector<double>);
	void addUik(int indexIndividual, double u);
	void setUik(int, int, double);

	std::vector<float> getXi(int);
	float getXid(int, int);
	std::vector<double> getUi(int);
	double getUik(int, int);
	std::string getName_i(int indexIndividual);
};

#endif /* TINDIVIDUALS_H_ */
