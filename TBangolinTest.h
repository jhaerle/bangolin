/*
 * TBangolinTest.h
 *
 *  Created on: Dec 11, 2017
 *      Author: phaentu
 */

#ifndef TBANGOLINTEST_H_
#define TBANGOLINTEST_H_

#include <vector>
#include <map>
#include "allTasks.h"
#include "TTaskList.h"

//------------------------------------------
//TBangolinTest
//------------------------------------------
//Base class for individual tests.
//Tests can be combined to suites in TBangolinTesting

class TBangolinTest {
protected:
	TLog* logfile;
	TParameters _testParams;
	std::string _testingPrefix;
	std::string _name;

	bool runTGenomeFromInputfile(std::string task);

public:
	TBangolinTest(TParameters & params, TLog* Logfile);
	virtual ~TBangolinTest() {
	}
	;

	std::string name() {
		return _name;
	}
	;

	virtual bool run() {
		return true;
	}
	;
};

#endif /* TBANGOLINTEST_H_ */
