//
// Created by Madleina Caduff on 19.10.18.
//

#ifndef TDATA_H
#define TDATA_H

#include <iostream>
#include <armadillo>
#include <math.h>
#include "stringFunctions.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include "TVcfFile.h"
#include "TIndividuals.h"
#include "TLoci.h"
#include "utils.h"
#include "TPhredToGTLMap.h"
#include "THMM.h"

class TData {
private:
	// debug
	arma::mat Fil;

	TLog* logfile;
	TVcfFileSingleLine vcfFile;
	TPhredToGTLMap * phredToGTLMap;

	// about vcf-file
	bool vcfRead;

	// about env-file
	std::vector<std::string> individualNamesFromEnv;

	//data on loci
	long numAcceptedLoci;
	TLoci * loci;

	// data on individuals
	int relevantNumIndivs;
	int numIndividualsFromEnv;
	int numLatFac;
	TIndividuals *individuals;

	// store environmental measurements
	std::vector<std::string> envVarNames;
	int numEnvVar;

	// HMM
	bool m_bool_hmm;
	THMMUpdater * hmmUpdater;

	// read data from .env file
	void parseLineFromEnv(std::vector<std::string> &, arma::mat &);
	void readDataFromEnv(TParameters& Parameters);
	void standardizeX(arma::mat &);

	// read data from vcf-file
	void openVCF(std::string);
	void getParametersForReadingVCF(TParameters& Parameters, int &, int &,
			int &, double &, double &, int &);
	void readDataFromVCF(TParameters& Parameters);
	std::string getChrPosString(std::string, long);
	void printProgressFrequencyFiltering(long, long &, long &, long &,
			struct timeval &);
	void matchIndividualNames(TParameters& Parameters, int, std::vector<int> &);
	int findPosOfVcfIndivInEnv(std::string);
	void findExtraIndivInEnvFile(std::vector<std::string> &);

	// EM algorithm for genotype frequencies
	void fillInitialEstimateOfGenotypeFrequencies(double*, unsigned short*);
	void estimateGenotypeFrequenciesNullModel(double* genotypeFrequencies,
			unsigned short* phredScores, double epsilonF);

	// clean up
	void clean();

public:
	TData(TLog* Logfile, TPhredToGTLMap * PhredToGTLMap);
	~TData() {
		clean();
	}
	;

	// read all data (vcf and .env)
	void readDataAndInitializeParams(TParameters& Parameters);

	// get main constants (n, L, D, K) and names of environmental variables
	int getNumIndiv();
	long getNumLoci();
	int getNumEnvVar();
	int getNumLatFac();
	bool linkageTrue();
	std::vector<std::string> getEnvVarNames();

	// get data
	TLoci* getLoci();
	TIndividuals* getIndividuals();
	THMMUpdater * getHMM();

	// store genotype likelihoods
	std::vector<unsigned short *> genotypePhredScores;
};

#endif //TDATA_H
