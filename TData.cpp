//
// Created by Madleina Caduff on 19.10.18.
//

#include "TData.h"

TData::TData(TLog* Logfile, TPhredToGTLMap * PhredToGTLMap) {
	logfile = Logfile;
	phredToGTLMap = PhredToGTLMap;

	individuals = new TIndividuals;
	loci = new TLoci;

	hmmUpdater = nullptr;

	vcfRead = false;
	relevantNumIndivs = 0;
	numIndividualsFromEnv = 0;
	numLatFac = 0;
	numEnvVar = 0;
	numAcceptedLoci = 0;

	m_bool_hmm = false;
}
;

void TData::clean() {
	for (std::vector<unsigned short *>::iterator it =
			genotypePhredScores.begin(); it < genotypePhredScores.end(); it++)
		delete[] *it;
	if (hmmUpdater != nullptr)
		delete hmmUpdater;
	if (loci != nullptr)
		delete loci;
	if (individuals != nullptr)
		delete individuals;
}
;

//////////////////////////////////////////////////////////////////////////////////////////////////
// Read data from .env file                                                                     //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TData::parseLineFromEnv(std::vector<std::string> & vec, arma::mat & X) {
	// get first element of vector (= name of individual)
	individualNamesFromEnv.push_back(vec[0]);

	// transform the vector of strings to a vector of floats
	std::vector<float> floatVec;
	std::transform(vec.begin() + 1, vec.end(), std::back_inserter(floatVec),
			stringToFloat);

	// add vector to matrix X
	X.insert_rows(numIndividualsFromEnv,
			arma::conv_to<arma::rowvec>::from(floatVec));
}

void TData::readDataFromEnv(TParameters& Parameters) {

	//open input stream
	std::string envFilename = Parameters.getParameterString("envFile");
	logfile->startIndent(
			"Reading environmental measurements from file '" + envFilename
					+ "'.");
	std::ifstream EnvFile(envFilename.c_str());
	if (!EnvFile)
		throw "Failed to open file '" + envFilename + "!";

	//parse file
	numIndividualsFromEnv = -1;
	std::vector<std::string> vec;

	arma::mat X;

	while (EnvFile.good() && !EnvFile.eof()) {
		fillVectorFromLineWhiteSpaceSkipEmpty(EnvFile, vec);
		if (!vec.empty()) {
			// parse header
			if (numIndividualsFromEnv == -1) {
				// first line: contains the names of the environmental variables
				envVarNames = vec;
			} else {
				parseLineFromEnv(vec, X);
			}
			++numIndividualsFromEnv;
		}
	}
	numEnvVar = envVarNames.size();
	logfile->list(
			"Detected a total number of " + toString(numEnvVar)
					+ " environmental variables in .env-file.");

	// standardize X_d's to have mean 0 and variance 1
	standardizeX(X);

	// create a new object of class TIndividual, initialize with respective name and env. measurements
	for (int i = 0; i < numIndividualsFromEnv; i++) {
		std::vector<float> xs = arma::conv_to<std::vector<float>>::from(
				X.row(i));
		TIndividual individual(individualNamesFromEnv[i], xs);
		individuals->addIndividual(individual);
	}
	logfile->endIndent();
}

void TData::standardizeX(arma::mat &X) {
	arma::rowvec means = mean(X, 0);
	arma::rowvec sds = stddev(X, 0, 0);
	for (int d = 0; d < numEnvVar; d++) {
		X.col(d) = (X.col(d) - means(d)) / sds(d);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Read data from VCF-file                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TData::openVCF(std::string vcfFilename) {
	//open input stream
	bool isZipped = false;
	if (vcfFilename.find(".gz") == std::string::npos) {
		logfile->startIndent("Reading vcf from file '" + vcfFilename + "'.");
	} else {
		logfile->startIndent(
				"Reading vcf from gzipped file '" + vcfFilename + "'.");
		isZipped = true;
	}

	vcfFile.openStream(vcfFilename, isZipped);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();
}

void TData::getParametersForReadingVCF(TParameters& Parameters,
		int & limitLines, int & minDepth, int & maxPercentMissing,
		double & epsilonF, double & freqFilter, int & progressFrequency) {
	// do we limit the lines to read?
	limitLines = Parameters.getParameterIntWithDefault("limitLines", -1);
	if (limitLines > 0)
		logfile->list(
				"Will limit analysis to the first " + toString(limitLines)
						+ " lines of the VCF file.");

	// do we set a depth filter?
	minDepth = Parameters.getParameterIntWithDefault("minDepth", 0);
	logfile->list(
			"Will filter samples to a minimum depth of " + toString(minDepth)
					+ ".");

	// do we set a missingness filter?
	maxPercentMissing = Parameters.getParameterIntWithDefault(
			"maxPercentMissing", 100);
	if (maxPercentMissing < 0 || maxPercentMissing > 100)
		throw "percentMissingPerLocus not within allowed range (0, 100)!";
	logfile->list(
			"Will remove loci where more than " + toString(maxPercentMissing)
					+ "% of samples are missing.");

	// parameters to set a filter on the allele frequency?
	epsilonF = Parameters.getParameterDoubleWithDefault("epsF", 0.0001);
	freqFilter = Parameters.getParameterDoubleWithDefault("minMAF", 0.0); // MAF = minor allele frequency
	if (freqFilter < 0.0 || freqFilter >= 1.0)
		throw "MAF filter not within allowed range (0.0,1.0)!";
	logfile->list(
			"Will filter on an allele frequency of " + toString(freqFilter)
					+ ".");

	// progress frequency
	progressFrequency = Parameters.getParameterIntWithDefault("reportFreq",
			10000);
}

void TData::readDataFromVCF(TParameters& Parameters) {
	if (vcfRead)
		throw "VCF already read!";

	// open vcf file
	std::string vcfFilename = Parameters.getParameterString("vcf");
	openVCF(vcfFilename);

	int limitLines;
	int minDepth;
	int maxPercentMissing;
	double epsilonF;
	double freqFilter;
	int progressFrequency;
	getParametersForReadingVCF(Parameters, limitLines, minDepth,
			maxPercentMissing, epsilonF, freqFilter, progressFrequency);

	// run HMM?
	if (Parameters.getParameterStringWithDefault("linkage", "true") == "true") { // linkage
		m_bool_hmm = true;
		logfile->list("Will run a HMM to infer linkage among loci!");
		hmmUpdater = new THMMUpdater;
	}

	// initialize variables for vcf-file
	int numIndividualsFromVcf = vcfFile.numSamples();
	long lineCounter = 0;
	long validSNPCounter = 0;
	long numIndividualsWithMissingSNP = 0;
	long missingSNPCounter = 0;
	long lowFreqSNPCounter = 0;
	std::string previousChrName = "";
	unsigned int previousLocusPos = 0;
	struct timeval start;
	gettimeofday(&start, NULL);
	unsigned short* curLocus;

	// initialize variables for EM
	double* genotypeFrequencies = new double[3];
	double f = 0;

	//run through VCF file
	logfile->startIndent("Parsing VCF file:");
	std::vector<int> orderOfIndividuals;
	// store the data of individuals from the vcf-file in the same order as the individuals in the .env file
	matchIndividualNames(Parameters, numIndividualsFromVcf, orderOfIndividuals);

	while (vcfFile.next()) { // new line in vcf-file (= new locus)
		numIndividualsWithMissingSNP = 0;
		++lineCounter;

		//skip sites with != 2 alleles
		if (vcfFile.getNumAlleles() == 2) {
			++validSNPCounter;

			// create an array containing the genotype likelihoods of all non-missing individuals of current locus
			curLocus = new unsigned short[relevantNumIndivs * 3];
			for (int individual = 0; individual < numIndividualsFromVcf;
					++individual) {

				if (orderOfIndividuals[individual] >= 0) { // if individual should be included
					// depth filter: if a locus has < minDepth reads, flag locus as missing (set all genotype likelihoods = 1)
					if (vcfFile.sampleDepth(individual) < minDepth) {
						vcfFile.setSampleMissing(individual);
						numIndividualsWithMissingSNP++;
					}
					vcfFile.fillPhredScore(individual,
							&curLocus[3 * orderOfIndividuals[individual]]);
				}
			}

			// missingness filter: if > percentMissingPerLocus of individuals per locus have are missing, remove locus
			if ((double) (100 * numIndividualsWithMissingSNP)
					/ (double) relevantNumIndivs > maxPercentMissing) {
				missingSNPCounter += 1;
				continue; // skip rest of loop (don't store)
			}

			// estimate allele frequency (EM algorithm)
			estimateGenotypeFrequenciesNullModel(genotypeFrequencies, curLocus,
					epsilonF);
			f = genotypeFrequencies[0] + 0.5 * genotypeFrequencies[1];
			if (f > 0.5)
				f = 1.0 - f;

			// only store SNPs that pass the frequency filter
			if (f >= freqFilter) {

				// store the genotype likelihoods of the current locus
				genotypePhredScores.push_back(curLocus);

				// add name of locus to loci
				loci->initializeNames(
						getChrPosString(vcfFile.chr(), vcfFile.position() - 1)); // start naming at 1

				// if HMM: store distance to previous locus
				if (m_bool_hmm)
					hmmUpdater->enterOneDelta_l(previousChrName, vcfFile.chr(),
							previousLocusPos, vcfFile.position() - 1);
				// store values for next loop
				previousLocusPos = vcfFile.position() - 1;
				previousChrName = vcfFile.chr();

				++numAcceptedLoci;
			} else {
				lowFreqSNPCounter++;
			}

			//report progress
			if (lineCounter % progressFrequency == 0)
				printProgressFrequencyFiltering(lineCounter, numAcceptedLoci,
						missingSNPCounter, lowFreqSNPCounter, start);
		}

		// limit lines
		if (limitLines > 0 && lineCounter > limitLines)
			break;
	}

	printProgressFrequencyFiltering(lineCounter, numAcceptedLoci,
			missingSNPCounter, lowFreqSNPCounter, start);
	loci->initializeStorage(numAcceptedLoci, numLatFac, numEnvVar);
	if (m_bool_hmm)
		hmmUpdater->setNumLoci(numAcceptedLoci);
	logfile->endIndent();
	logfile->endIndent();

	// clean up
	delete[] genotypeFrequencies;

	//set that vcf was read
	vcfRead = true;
}

std::string TData::getChrPosString(std::string chromosomeName,
		long snpPosition) {
	return chromosomeName + "_" + toString(snpPosition);
}

void TData::printProgressFrequencyFiltering(long lines, long & numRetainedLoci,
		long & numLociMissing, long & lowFreqSNPCounter,
		struct timeval & start) {
	struct timeval end;
	gettimeofday(&end, NULL);
	float runtime = (end.tv_sec - start.tv_sec) / 60.0;
	logfile->startIndent(
			"Parsed " + toString(lines) + " lines and retained "
					+ toString(numRetainedLoci) + " loci in "
					+ toString(runtime) + " min");
	logfile->list(
			toString(numLociMissing)
					+ " loci were filtered out due to missingness.");
	logfile->list(
			toString(lowFreqSNPCounter)
					+ " loci were filtered out due to low allele frequencies.");
	logfile->endIndent();
}

void TData::matchIndividualNames(TParameters& Parameters,
		int numIndividualsFromVcf, std::vector<int> & orderOfIndividuals) {
	// save names of individuals
	std::string sampleName;
	std::vector<std::string> individualNamesFromVcf;
	for (int individual = 0; individual < numIndividualsFromVcf; ++individual) {
		sampleName = vcfFile.sampleName(individual);
		orderOfIndividuals.push_back(findPosOfVcfIndivInEnv(sampleName));
		individualNamesFromVcf.push_back(sampleName); // contains all individual names of vcf, also those that are ignored during analysis
	}
	// count the number of individuals present in vcf AND env file
	relevantNumIndivs = numIndividualsFromVcf
			- std::count(orderOfIndividuals.begin(), orderOfIndividuals.end(),
					-1);
	if (relevantNumIndivs < numIndividualsFromEnv) {
		findExtraIndivInEnvFile(individualNamesFromVcf);
	}

	logfile->list(
			"A total number of " + toString(relevantNumIndivs)
					+ " individuals will be considered for analysis.");
	// initialize K
	numLatFac = Parameters.getParameterInt("numLatFac");
	if ((numLatFac + numEnvVar + 1) > relevantNumIndivs)
		throw "There are more degrees of freedom ("
				+ toString(numLatFac + numEnvVar + 1) + ") than individuals ("
				+ toString(relevantNumIndivs)
				+ ")! Please increase the number of individuals or decrease numLatFac or numEnvVar.";
	logfile->list(
			"Will use K = " + toString(numLatFac)
					+ " latent factors for analysis.");
}

int TData::findPosOfVcfIndivInEnv(std::string individualNameVcf) {
	// find the position of the .vcf-individual in the vector of .env-individual names
	int position = 0;
	int numberOfMatches = 0;
	int matchPos;
	for (std::vector<std::string>::iterator it = individualNamesFromEnv.begin();
			it < individualNamesFromEnv.end(); it++, position++) {
		if (individualNameVcf == *it) {
			matchPos = position;
			numberOfMatches++;
		}
	}
	if (numberOfMatches == 0) {
		logfile->list(
				"Individual '" + individualNameVcf
						+ "' from vcf-file does not match any individual from env-file - will ignore it.");
		matchPos = -1;
	} else if (numberOfMatches >= 2) {
		throw "Individual '" + individualNameVcf + "' from vcf-file occurs "
				+ toString(numberOfMatches)
				+ "x in the env-file! Please change env-file accordingly.";
	}
	return matchPos;
}

void TData::findExtraIndivInEnvFile(
		std::vector<std::string> & individualNamesFromVcf) {
	// find the position of the .env-individual in the vector of .vcf-individual names
	for (std::vector<std::string>::iterator nameEnv =
			individualNamesFromEnv.begin();
			nameEnv < individualNamesFromEnv.end(); nameEnv++) {
		int numberOfMatches = 0;
		for (std::vector<std::string>::iterator nameVcf =
				individualNamesFromVcf.begin();
				nameVcf < individualNamesFromVcf.end(); nameVcf++) {
			if (*nameEnv == *nameVcf) {
				numberOfMatches++;
			}
		}
		if (numberOfMatches == 0) {
			throw "Individual '" + *nameEnv
					+ "' from env-file does not match any individual from vcf-file! Please change env-file accordingly.";
		} else if (numberOfMatches >= 2) {
			throw "Individual '" + *nameEnv + "' from env-file occurs "
					+ toString(numberOfMatches)
					+ "x in the vcf-file! Please change vcf-file accordingly.";
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Run EM algorithm to filter the loci on their frequency                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TData::fillInitialEstimateOfGenotypeFrequencies(double* genoFreq,
		unsigned short* phredScores) {
	//calculate by using MLE genotype for each individual
	genoFreq[0] = 0.0;
	genoFreq[1] = 0.0;
	genoFreq[2] = 0.0;
	for (int i = 0; i < 3 * relevantNumIndivs; i += 3) {
		if (phredScores[i + 1] < phredScores[i]) {
			if (phredScores[i + 2] < phredScores[i + 1])
				genoFreq[2] += 1.0;
			else
				genoFreq[1] += 1.0;
		} else {
			if (phredScores[i + 2] < phredScores[i])
				genoFreq[2] += 1.0;
			else
				genoFreq[0] += 1.0;
		}
	}

	double sum = 0.0;
	for (int g = 0; g < 3; ++g) {
		genoFreq[g] /= (double) relevantNumIndivs;
		if (genoFreq[g] <= 0.0)
			genoFreq[g] = 0.01;
		if (genoFreq[g] >= 1.0)
			genoFreq[g] = 0.99;
		sum += genoFreq[g];
	}
	for (int g = 0; g < 3; ++g) {
		genoFreq[g] /= sum;
	}
}

void TData::estimateGenotypeFrequenciesNullModel(double* genotypeFrequencies,
		unsigned short* phredScores, double epsilonF) {
	//prepare variables
	double sum;
	int i, g;
	double weightsNull[3];
	double genoFreq_old[3];

	//estimate initial frequencies from MLEs
	fillInitialEstimateOfGenotypeFrequencies(genotypeFrequencies, phredScores);

	//run EM for max 1000 steps
	for (int s = 0; s < 1000; ++s) {
		//set genofreq and calc P(g|f)
		for (g = 0; g < 3; ++g) {
			genoFreq_old[g] = genotypeFrequencies[g];
			genotypeFrequencies[g] = 0.0;
		}

		//estimate new genotype frequencies
		for (i = 0; i < 3 * relevantNumIndivs; i += 3) {
			sum = 0.0;
			for (g = 0; g < 3; ++g) {
				weightsNull[g] = (*phredToGTLMap)[phredScores[i + g]]
						* genoFreq_old[g];
				sum += weightsNull[g];
			}
			genotypeFrequencies[0] += weightsNull[0] / sum;
			genotypeFrequencies[2] += weightsNull[2] / sum;
		}

		genotypeFrequencies[0] /= (double) relevantNumIndivs;
		genotypeFrequencies[2] /= (double) relevantNumIndivs;
		genotypeFrequencies[1] = 1.0 - genotypeFrequencies[0]
				- genotypeFrequencies[2];

		//check if we stop
		if (fabs(genotypeFrequencies[0] - genoFreq_old[0]) < epsilonF
				&& fabs(genotypeFrequencies[2] - genoFreq_old[2]) < epsilonF)
			break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// get-functions                                                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////

int TData::getNumIndiv() {
	return relevantNumIndivs;
}

long TData::getNumLoci() {
	return numAcceptedLoci;
}

int TData::getNumEnvVar() {
	return numEnvVar;
}

int TData::getNumLatFac() {
	return numLatFac;
}

TLoci* TData::getLoci() {
	return loci;
}

TIndividuals* TData::getIndividuals() {
	return individuals;
}

std::vector<std::string> TData::getEnvVarNames() {
	return envVarNames;
}

bool TData::linkageTrue() {
	return m_bool_hmm;
}

THMMUpdater * TData::getHMM() {
	return hmmUpdater;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// run all                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TData::readDataAndInitializeParams(TParameters& Parameters) {
	readDataFromEnv(Parameters);
	readDataFromVCF(Parameters);
}
