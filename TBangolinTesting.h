/*
 * TBangolinTesting.h
 *
 *  Created on: Dec 11, 2017
 *      Author: phaentu
 */

#ifndef TBANGOLINTESTING_H_
#define TBANGOLINTESTING_H_

#include "TBangolinTestList.h"

//-----------------------------------
//TBangolinTesting
//-----------------------------------
class TBangolinTesting {
private:
	TLog* logfile;
	std::string outputName;
	TBangolinTestList testList;

	void parseTests(TParameters & params);

public:
	TBangolinTesting(TParameters & params, TLog* Logfile);
	~TBangolinTesting() {
	}
	;
	void printTests();
	void addTest(std::string & name, TParameters & params);
	void runTests();
};

#endif /* TBANGOLINTESTING_H_ */
