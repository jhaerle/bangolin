#make file for atlas

SRC = $(wildcard *.cpp) $(wildcard *.C) $(wildcard commonutilities/*.cpp)
GIT_HEADER = gitversion.cpp

OBJ = $(SRC:%.cpp=%.o)
BIN = bangolin

.PHONY : all
all : $(BIN)

ifeq ($(ARM),)
BINFLAG = -lz -larmadillo
OBJFLAG = -std=c++1y
else
BINFLAG = -lz -lblas -llapack
OBJFLAG = -Iarmadillo-8.400.0/include -DARMA_DONT_USE_WRAPPER -lblas -llapack -std=c++1y
endif

$(BIN): $(OBJ)
	$(CXX) -O3 -o $(BIN) $(OBJ) $(BINFLAG)

$(GIT_HEADER): .git/HEAD .git/COMMIT_EDITMSG
	echo "#include \"gitversion.h\"" > $@
	echo "std::string getGitVersion(){" >> $@
	echo "return \"$(shell git rev-parse HEAD)\";" >> $@
	echo "}" >> $@

.git/COMMIT_EDITMSG :
	touch $@

%.o: %.cpp
	$(CXX) -O3 -c -Icommonutilities $(OBJFLAG) $< -o $@


.PHONY : clean
clean:
	rm -rf $(BIN) $(OBJ)
