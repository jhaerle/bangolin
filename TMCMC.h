/*
 * TMCMC.h
 *
 *  Created on: Oct 26, 2018
 *      Author: caduffm
 */

#ifndef TMCMC_H_
#define TMCMC_H_
#define ARMA_DONT_PRINT_ERRORS

#include <iostream>
#include <armadillo>
#include <math.h>
#include <algorithm>
#include "stringFunctions.h"
#include "TParameters.h"
#include "TFile.h"
#include "TRandomGenerator.h"
#include "TIndividuals.h"
#include "TLoci.h"
#include "utils.h"
#include "TPhredToGTLMap.h"
#include "gzstream.h"
#include "THMM.h"
#include <string>

class TWeights {
private:
	int numLoci;
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	arma::vec cumWeights;

public:
	TWeights(TRandomGenerator*, TLog*, int);
	// input data: F values (to be filled by TData)
	arma::fvec FVals;
	arma::uvec ranks;

	// get cumulative weights
	void calculateCumulativeWeights(TParameters& Parameters);
	double calculateParamP(double, double);

	// selection of random locus according to weights
	long pickRandomLocus();
};

class TWriteMCMCFiles {
private:
	TLog * logfile;
	TLoci * loci;
	TIndividuals * individuals;

	std::string prefix;

	std::vector<long> lociIndicesToPrint;
	TOutputFilePlain * traceFileMu;
	TOutputFilePlain * traceFileGamma;
	TOutputFilePlain * traceFileHypers;
	TOutputFilePlain * traceFileV;
	TOutputFilePlain * traceFileU;
	TOutputFilePlain * traceFileBeta;

	TOutputFilePlain * betaProposalFile;
	TOutputFilePlain * betaAcceptanceFile;

	TOutputFilePlain * traceFileLL;
	TOutputFilePlain * traceFilePosterior;

	bool linkageTrue;

	// trace files
	void prepareMuTraceFile();
	void prepareGammaTraceFile();
	void prepareHyperTraceFile(int numEnvVar);
	void prepareBetaTraceFile();
	void prepareVTraceFile();
	void prepareUTraceFile(int numIndiv);
	void preparePosteriorTraceFile();
	void prepareLLTraceFile();
	void writeTraceMuToFile();
	void writeTraceGammaToFile(int numLoci);
	void writeTraceHypersToFile(int numEnvVar, THMMUpdater * hmmUpdater);
	void writeTraceBetaToFile(int numLoci);
	void writeTraceVToFile(int numLoci);
	void writeTraceUToFile(int numIndiv);
	void writeTraceLLToFile(int numLoci, double * ll);
	void writeTracePosteriorToFile(int numLoci, double * posterior);

	// burnin files
	void prepareBetaAcceptanceFile();
	void prepareBetaProposalFile();
	void writeBetaProposalToFile(int numLoci,
			float ** widthProposalKernelBetadl);
	void writeBetaAcceptanceToFile(int numLoci,
			uint32_t ** numAcceptedBetaPerLD,
			uint32_t ** totalNumUpdatesBetaPerLocus);

	// final files
	void writeFinalUToFile(int numLatFac, int numIndiv,
			TIndividuals * individuals, double ** sumU);
	void writeFinalVToFile(int numLatFac, int numLoci, TLoci * loci,
			double ** sumV);
	void constructHeaderLastIterToFile(std::vector<std::string> & header,
			int numEnvVar, int numLoci, int numLatFac, int numIndiv,
			bool linkage);

public:
	int thinning;

	// files to write to
	bool writeMuTrace;
	bool writeGammaTrace;
	bool writeHyperTrace;
	bool writeBetaTrace;
	bool writeVTrace;
	bool writeUTrace;
	bool writeHyperFinal;
	bool writeUVFinal;
	bool writeMuFinal;
	bool writeLastIter;
	bool writeBetaProposal;
	bool writeBetaAcceptance;
	bool writeAfterInit;
	bool writeDuringBurnin;
	bool writeLLTrace;
	bool writePosteriorTrace;

	TWriteMCMCFiles();
	~TWriteMCMCFiles();

	void initialize(TLog * logfile, TLoci * Loci, bool LinkageTrue,
			TIndividuals * individuals);

	void readFileParameters(TParameters& Parameters, int numLoci,
			int numIterations);

	// trace files
	void prepareTraceFiles(int numEnvVar, int numIndiv);
	void writeTraceFiles(int numEnvVar, THMMUpdater * hmmUpdater, int numLoci,
			int numIndiv, double * ll, double * posterior);

	// burnin files
	void prepareBurninFiles();
	void writeBurninFiles(int numLoci, float ** widthProposalKernelBetadl,
			uint32_t ** numAcceptedBetaPerLD,
			uint32_t ** totalNumUpdatesBetaPerLocus);

	// final files
	void writeFinalMuToFile(int numLoci, double * expValMul,
			double * expValSquaredMul);
	void writeFinalUVToFile(int numLatFac, int numIndiv, int numLoci,
			TIndividuals * individuals, TLoci * loci, double ** sumU,
			double ** sumV);
	void writeFinalHyperToFile(int numEnvVar,
			std::vector<std::string> & envVarNames,
			arma::mat & postMeanSigmaBeta, double alphaFSum, double betaFSum,
			double piSum, double lambda_1_sum, double lambda_2_sum);
	void writeToBetaGammaFile(int numEnvVar,
			std::vector<std::string> & envVarNames, int numLoci,
			float * gammaSum, uint32_t * gammaCounter, double ** expValBetadl,
			double ** expValSquaredBetadl, uint32_t ** betaUpdateCounter);
	void writeLastIterToFile(int numEnvVar, int numLoci, int numLatFac,
			int numIndiv, double pi, THMMUpdater * hmmUpdater, double m,
			arma::mat MrrAndMrs, double alpha_f, double beta_f,
			unsigned short * gamma, float ** betas, float * mus, float ** vs,
			TIndividuals * individuals, bool linkage);

	void closeOutputFiles();

};

class TMCMC {
private:
	TLog* logfile;
	std::vector<unsigned short *> genotypePhredScores;
	TRandomGenerator* randomGenerator;
	TPhredToGTLMap * phredToGTLMap;
	TWeights * weights;
	TLogisticLookup * logisticLookup;
	TWriteMCMCFiles files;

	// data on individuals
	int numIndiv;
	TIndividuals * individuals;

	// data on loci
	long numLoci;
	TLoci * loci;
	int initialNumLociWithGamma1;

	// values of D and K
	int numEnvVar;
	int numLatFac;
	std::vector<std::string> envVarNames;

	// info for MCMC
	double minAlleleFreq;
	double oneMinusminAlleleFreq;
	int numIterations;
	int numBurnins;
	int burnin;
	int numBetaUpdatesPerIter;
	double lambda_for_pi;
	bool fixBetaGammaToZero;
	bool fixU;
	bool fixV;

	// number of accepted updates
	int * numAcceptedFPerLocus;
	int numAcceptedAlphaF;
	int numAcceptedBetaF;
	int numAcceptedPi;
	int numAcceptedLambda1;
	int numAcceptedLambda2;
	int numAcceptedVkl;
	int numAcceptedUk;
	int numAcceptedLittleM;
	int numAcceptedMrr;
	int numAcceptedMrs;
	uint32_t ** numAcceptedBetaPerLD;
	uint32_t ** totalNumUpdatesBetaPerLocus;
	int numAcceptedGamma;
	int gammaTotalNumTrialsCounter;
	int betaTotalNumTrialsCounter;

	// proposal kernels
	float * widthProposalKernelF;
	float widthProposalKernelLogAlphaF;
	float widthProposalKernelLogBetaF;
	float widthProposalKernelPi;
	float widthProposalKernelLambda1;
	float widthProposalKernelLambda2;
	float widthProposalKernelVkl;
	float widthProposalKernelUk;
	float widthProposalKernelLittleM;
	float widthProposalKernelMrr;
	float widthProposalKernelMrs;
	float ** widthProposalKernelBetadl;
	float widthSdProposalKernelBetaL;
	float widthVarProposalKernelBetaL;
	float qOfgamma1Togamma0;
	float logqOfgamma1Togamma0;

	// priors
	float lambdaM;

	// data storage in MCMC
	float ** logit_f_il;
	double ** expValBetadl;
	double ** expValSquaredBetadl;
	uint32_t ** betaUpdateCounter;
	float * gammaSum;
	uint32_t * gammaCounter;
	bool storageBetasInitialized;
	bool storageMusInitialized;
	double * expValMul;
	double * expValSquaredMul;
	arma::mat SigmaBetaSum;
	arma::mat postMeanSigmaBeta;
	double alphaFSum;
	double betaFSum;
	double piSum;
	double lambda_1_sum;
	double lambda_2_sum;
	double ** sumU;
	double ** sumV;
	double * ll;
	double * posterior;

	// HMM
	bool linkageTrue;
	THMMUpdater * hmmUpdater;

	// initialize parameters
	void initializeParameters(TParameters& Parameters);
	// initialize U
	void initializeU(arma::mat &);
	void initializeUFromFile(arma::mat &U, std::string usfile);
	void calculateInitialU(arma::mat &);
	void addToExyAndEx(arma::fvec &, arma::mat &, arma::mat &, int);
	void fillVarCovarMat(arma::mat &, arma::mat &, arma::vec &, int);
	void fillFil(arma::fvec &, std::vector<unsigned short *>::iterator, double,
			double, bool, int);
	// initialize Beta, Gamma, V
	void initializeBetaVGamma(arma::mat &, TParameters& Parameters);
	void countTransitions(arma::mat & tau, double & counter);
	void initializeLambda();
	void fillXFromIndividuals(arma::mat &);
	float meanPosteriorGenotype(unsigned short *);
	float computeF(arma::mat &, arma::mat &, arma::mat &, arma::mat &,
			arma::fvec &, float, float);
	void selectLociWhereGammaIsOne(arma::uvec &, TParameters& Parameters);
	void compareResToUnresModel(arma::mat &, arma::mat &, arma::mat &);
	void setVFromFile(TParameters& Parameters, std::string vsfile);

	// initialize 2-round burnin
	void initializeGammaAndPiInsideBurnin(TParameters& Parameters);

	// initialize hyperparameters
	void initializeHyperparams();
	void initializeAlphaFBetaF();
	void computeSigmaBeta(arma::mat &);
	void computeM(arma::mat &);
	void initializeMrrMrsAndM();

	// run MCMC
	void prepareMCMC(TParameters& Parameters);
	void readMCMCParameters(TParameters& Parameters);
	void readProposalWidths(TParameters& Parameters);
	void readOtherParameters(TParameters& Parameters);
	void coordinateMCMC(TParameters & Parameters);
	void oneMCMCIteration(bool RJMCMC);
	void prepareChainWithSampling();
	void prepareBurnin();
	void concludeChain(struct timeval start);
	void concludeBurnin(bool RJMCMC);

	// update parameters
	void updateLoci(bool RJMCMC);
	void updateIndividuals();
	void updateF(long, double &);
	bool updateAlphaF();
	bool updateBetaF();
	bool updatePi();
	void updateVkl(int, long, double &);
	void updateUk(int, double &);
	bool updateLittleM();
	bool updateOneMrr(int);
	bool updateOneMrs(int, int);
	void updateSwitcherBetaGamma(long, double, double, bool RJMCMC);
	double updateBetadl(int, long, double);
	void updateBetaGamma(bool RJMCMC);
	void gammaToOne(long, double, double);
	void gammaToZero(long, double, double);
	bool updateLambda1();
	bool updateLambda2();

	// helper functions
	double initializeSumOverIndividuals(long l);
	double initializeSumOverAll();
	void calcSumOverHyperParamsBeta(double &, double &);
	bool coreUpdateParamLocus(long, double, double, double &);
	bool coreUpdateParamLocus(long, double *, double, double &);
	void initializeLogitFil();
	void initializeStorageBetas();
	void initializeStorageHypers();
	void initializeStorageUV();
	void computeCompleteLogLikelihoodF(long);
	void computeCompleteLogLikelihoodAlphaBetaF();
	void gramSchmidt(int, double *, int K);
	double sumBetalXi(int, long);
	double sumUiVl(int, long);
	void addToHypers();
	void addToUV();
	void addToV();
	inline double getLogLikelihood(int, long, float);
	void concludeAcceptanceRate(int, int, std::string);
	void concludeAcceptanceRates(int);
	void initializeStorageAcceptanceRates();
	void concludeExpAndVarBetas();
	void concludeExpAndVarMus();
	void concludeExpValHypers();
	void concludeExpValUV();
	double calculateAcceptanceRate(int, int);
	void adjustProposalWidth(int, int, float &);
	void adjustAllProposalWidth(int, bool RJMCMC);
	void clearCounters();
	void assertThatVectorsAreOrthogonal();
	void initializeStorageMus();
	void processPostMeanSigmaBeta();
	void computeFullLogLikelihood();
	void computePosterior();
	double computePriorBeta(int l);
	double computePriorMu(int l);

	// init
	void init(TParameters& Parameters, TLog* Logfile,
			TRandomGenerator * RandomGenerator,
			std::vector<unsigned short *> &phredScores, TLoci* locus_pointer,
			TIndividuals* individual_pointer, int n, int L, int D, int K,
			std::vector<std::string> &dnames, TPhredToGTLMap * PhredToGTLMap);
	// clean
	void clean();

public:
	TMCMC(TParameters& Parameters, TLog* Logfile,
			TRandomGenerator * RandomGenerator,
			std::vector<unsigned short *> &phredScores, TLoci* locus_pointer,
			TIndividuals* individual_pointer, int n, int L, int D, int K,
			std::vector<std::string> &dnames, TPhredToGTLMap * PhredToGTLMap);
	TMCMC(TParameters& Parameters, TLog* Logfile,
			TRandomGenerator * RandomGenerator,
			std::vector<unsigned short *> &phredScores, TLoci* locus_pointer,
			TIndividuals* individual_pointer, int n, int L, int D, int K,
			std::vector<std::string> &dnames, TPhredToGTLMap * PhredToGTLMap,
			THMMUpdater * HmmUpdater);
	~TMCMC() {
		clean();
	}
	;
	void runMCMC(TParameters& Parameters);
};

#endif /* TMCMC_H_ */
