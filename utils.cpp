/*
 * utils.cpp
 *
 *  Created on: Oct 16, 2018
 *      Author: caduffm
 */

#include "utils.h"

// Logit Function
double logit(double x) {
	if (x < 0.0 || x > 1.0) {
		throw("Input value " + toString(x)
				+ " for logit is not within allowed range (0.0,1.0)!");
	}
	return log(x / (1.0 - x));
}

// Logistic Function
double logistic(double x) {
	return 1.0 / (1.0 + exp(-x));
}

double phred(double x) {
	return -10.0 * log10(x);
}

///////////////////////////////////////////////////////////////////////////
// logistic lookup table										    	 //
///////////////////////////////////////////////////////////////////////////

// approximate the LOGISTIC (!!) function with a lookup table

// construct lookup table

TLogisticLookup::TLogisticLookup() {
	xsat = 15;
	delta_x = (double) xsat / lengthArray;
	constructLookupTable();
}

void TLogisticLookup::getInterpolation(double x1, double x2, double &I,
		double &a) {
	// gets two values of x and returns a vector of x1, the intercept and the slope
	double y1 = logistic(x1);
	double y2 = logistic(x2);

	a = (y2 - y1) / (x2 - x1);
	I = y1 - a * x1;
}

void TLogisticLookup::constructLookupTable() {
	// construct breakpoints
	double breakpoints[lengthArray + 1]; // 1 element longer than lookup, to allow for proper interpolation of last element
	for (int bp = 0; bp < lengthArray + 1; bp++) {
		breakpoints[bp] = bp * delta_x;
	}
	// get slope and intercept
	double I = 0;
	double a = 0;
	for (int bp = 0; bp < lengthArray; bp++) {
		getInterpolation(breakpoints[bp], breakpoints[bp + 1], I, a);
		lookup[bp][0] = breakpoints[bp];
		lookup[bp][1] = I;
		lookup[bp][2] = a;
	}
}

// perform approximation

double TLogisticLookup::approxLogistic(double x) {
	double y;
	double absx = fabs(x);
	if (absx >= xsat)
		y = 0.99999999;
	else {
		double * lookupIndexBreakpoint =
				lookup[(int) std::floor(absx / delta_x)]; // get next smaller breakpoint
		y = lookupIndexBreakpoint[1] + lookupIndexBreakpoint[2] * absx; // linear function
	}
	if (x < 0) // if negative: mirror at 0
		y = 1 - y;
	return y;
}

double TLogisticLookup::approxLogistic(float x) {
	double y;
	double absx = fabs(x);
	if (absx >= xsat)
		y = 0.99999999;
	else {
		double * lookupIndexBreakpoint =
				lookup[(int) std::floor(absx / delta_x)]; // get next smaller breakpoint
		y = lookupIndexBreakpoint[1] + lookupIndexBreakpoint[2] * absx; // linear function
	}
	if (x < 0) // if negative: mirror at 0
		y = 1 - y;
	return y;
}
