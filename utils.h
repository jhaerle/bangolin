/*
 * utils.h
 *
 *  Created on: Oct 16, 2018
 *      Author: caduffm
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <cmath>
#include "stringFunctions.h"
#include <iostream>

// logit
double logit(double x);

// logistic
double logistic(double x);
double phred(double);

class TLogisticLookup {

private:
	static const int numBreakpoints = 1000;
	static const int lengthArray = numBreakpoints + 1;
	double lookup[lengthArray][3];
	int xsat;
	double delta_x;

	void getInterpolation(double, double, double&, double&);
	void constructLookupTable();

public:
	TLogisticLookup();
	double approxLogistic(double);
	double approxLogistic(float);
};
#endif /* UTILS_H_ */
