/*
 * THMM.h
 *
 *  Created on: Apr 30, 2019
 *      Author: caduffm
 */

#ifndef THMM_H_
#define THMM_H_

#include <vector>
#include <iostream>
#include <math.h>
#include <string>

class THMM {
public:
	THMM();
	~THMM();
	void enterOneDelta_l(std::string chr_previous_l, std::string chr_this_l,
			unsigned int pos_previous_l, unsigned int pos_this_l);
	void setNumLoci(int L);
	void initializeTransProb();

	double getTau(int l, int first_gamma, int second_gamma);

	double getLambda1();
	double getLambda2();
	double getIndexDelta_l(int l);
	void setLambda(double lambda_1, double lambda_2);

	void printIndexDelta();
	void printTau();

protected:
	void initializeExpLambda(double lambda_1, double lambda_2, double *** tau);
	void fillTau(const double lambda_1, const double lambda_2, double *** tau);
	double fillStationary(const double lambda_1, const double lambda_2);

	double *** m_tau;

	std::vector<uint8_t> m_index_delta;

	double m_Lambda_1;
	double m_Lambda_2;

	int numLoci;

	uint8_t maxIndex;
};

class THMMUpdater: public THMM {
private:
	void initializeNewTau();

	double *** m_new_tau;

public:
	THMMUpdater();
	~THMMUpdater();

	void printNewTau();
	double getNewTau(int l, int first_gamma, int second_gamma);

	void updateTransProbWithNewLambda1(double lambda_1);
	void updateTransProbWithNewLambda2(double lambda_2);
	void acceptNewLambda1(double lambda_1);
	void acceptNewLambda2(double lambda_2);
};

#endif /* THMM_H_ */
