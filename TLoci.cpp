/*
 * TLoci.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: caduffm
 */

#include "TLoci.h"

TLoci::TLoci() {
	numLoci = 0;
	numLatFac = 0;
	numEnvVar = 0;

	alphaF = 0;
	logAlphaF = 0;
	betaF = 0;
	logBetaF = 0;

	betas = NULL;
	vs = NULL;
	mus = NULL;
}

void TLoci::clean() {
	if (muInitialized) {
		delete[] mus;
		muInitialized = false;
	}
	if (vInitialized) {
		for (int k = 0; k < numLatFac; k++) {
			delete[] vs[k];
		}
		delete[] vs;
		vInitialized = false;
	}
	if (betaInitialized) {
		delete betas;
		betaInitialized = false;
	}
}

///////////////////////////
// initialize            //
///////////////////////////

void TLoci::initializeStorage(long L, int K, int D) {
	numLoci = L;
	numLatFac = K;
	numEnvVar = D;

	if (!muInitialized) {
		mus = new float[numLoci];
		muInitialized = true;
	}

	if (!vInitialized) {
		// vs: pointer to K-dimensional array, where each element points to a L-dimensional array of v-values
		vs = new float*[numLatFac];
		for (int k = 0; k < numLatFac; k++) {
			vs[k] = new float[numLoci];
		}
		vInitialized = true;
	}

	if (!betaInitialized) {
		betas = new TBeta(numLoci, numEnvVar);
		betaInitialized = true;
	}
}

void TLoci::setOneMu(float mu_l, long locusCounter) {
	mus[locusCounter] = mu_l;
}

void TLoci::setOneVkl(float v_l, int kCounter, long locusCounter) {
	vs[kCounter][locusCounter] = v_l;
}

void TLoci::initializeNames(std::string name_l) {
	names.push_back(name_l);
}

// hyperparameters

void TLoci::setAlphaF(double a) {
	alphaF = a;
}

void TLoci::setBetaF(double b) {
	betaF = b;
}

void TLoci::setLogAlphaF(double a) {
	logAlphaF = a;
}

void TLoci::setLogBetaF(double b) {
	logBetaF = b;
}

///////////////////////////
// get-functions         //
///////////////////////////

float TLoci::getMu(int indexLocus) {
	return mus[indexLocus];
}

std::string TLoci::getName(int indexLocus) {
	return names[indexLocus];
}

std::vector<std::string> TLoci::getNames() {
	return names;
}

float TLoci::getOneVkl(int kCounter, long locusCounter) {
	return vs[kCounter][locusCounter];
}

///////////////////////////////////////////////////////////////////////////
// TBeta                                                                 //
///////////////////////////////////////////////////////////////////////////

TBeta::TBeta(long L, int D) {
	numLoci = L;
	numEnvVar = D;
	pi = 0;
	logPi = 0;
	m = 0;
	theBetasInitialized = false;
	gammaInitialized = false;

	theBetas = NULL;
	gammas = NULL;

	initializeStorage();
}

void TBeta::initializeStorage() {

	if (!gammaInitialized) {
		gammas = new unsigned short[numLoci];
		gammaInitialized = true;
	}

	MrrAndMrs.set_size(numEnvVar, numEnvVar); // matrix containing mrr and mrs
	MrrAndMrs.zeros();

	if (!theBetasInitialized) {
		// theBetas: pointer to D-dimensional array, where each element points to a L-dimensional array of beta-values
		theBetas = new float*[numEnvVar];
		for (int d = 0; d < numEnvVar; d++) {
			theBetas[d] = new float[numLoci];
		}
		theBetasInitialized = true;
	}
}

void TBeta::clean() {
	if (theBetasInitialized) {
		for (int d = 0; d < numEnvVar; d++) {
			delete[] theBetas[d];
		}
		delete[] theBetas;
	}
	if (gammaInitialized)
		delete[] gammas;
}

///////////////////////////
// initialize            //
///////////////////////////

void TBeta::setOneBetadl(float beta_dl, int envVarCounter, long locusCounter) {
	theBetas[envVarCounter][locusCounter] = beta_dl;
}

void TBeta::initializeGammas(unsigned short gamma_l, int locusCounter) {
	gammas[locusCounter] = gamma_l;
}

void TBeta::initializeIndexLociGammaOne(int locusCounter) {
	indexLociWhereGammaIsOne.push_back(locusCounter);
}

void TBeta::setOneMrrOrMrs(double oneMrsOrMrr, int r, int s) {
	MrrAndMrs(r, s) = oneMrsOrMrr;
}

void TBeta::setM(double myM) {
	m = myM;
}

void TBeta::setPi(double p) {
	if (p <= 0) // impute with very small value
		pi = 1. / numLoci;
	else if (p == 1) // impute with value which is almost 1
		pi = 1 - 1. / numLoci;
	else
		pi = p;
	logPi = log(pi);
}

unsigned short TBeta::getGamma(int l) {
	if (l < 0 || l >= numLoci)
		return 0; // not existing -> need to put 0 because otherwise I access elements that don't exist in HMM
	else
		return gammas[l];
}

