/*
 * TSimulator.cpp
 *
 *  Created on: Oct 16, 2018
 *      Author: caduffm
 */

#include "TSimulator.h"

void TSimulationManager::manageSimulations(TLog* Logfile,
		TRandomGenerator * RandomGenerator, TParameters & Parameters) {
	// linkage or not?
	if (Parameters.getParameterStringWithDefault("linkage", "true") == "true") {
		Logfile->list("Will simulate with linkage.");
		TSimulatorHMM simulatorHmm(Logfile, RandomGenerator);
		simulatorHmm.runSimulations(Parameters);
	} else {
		Logfile->list("Will simulate without linkage.");
		TSimulator simulator(Logfile, RandomGenerator);
		simulator.runSimulations(Parameters);
	}
}

TSimulator::TSimulator(TLog* Logfile, TRandomGenerator * RandomGenerator) {
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	varianceExplainedBeta = nullptr;
	n = 0;
	L = 0;
	D = 0;
	K = 0;
	pi = 0;
	m = 0;
	lambda_m = 0;
	alpha_f = 0;
	beta_f = 0;
	boolFixVarExplBeta = false;
	fixedVarExplBeta = 0;
	gt_error = 0;
	varianceX = 0;
	meanDepth = 0;
	varianceU = 0;
	varianceV = 0;
	varianceExplainedUV = nullptr;
	boolWriteSimParamFiles = false;
	readGenoFromFile = false;
	budgetMeanDepth = 0;
	budgetNumIndivs = 0;
}

void TSimulator::readingCommonParameters(TParameters& Parameters) {
	// read simulation parameters
	logfile->startIndent("Reading simulation parameters:");

	// get budget and mean depth and compute number of individuals that can be afforded based on this
	budgetNumIndivs = Parameters.getParameterIntWithDefault("budgetNumIndivs",
			100);
	budgetMeanDepth = Parameters.getParameterDoubleWithDefault(
			"budgetMeanDepth", 20);
	logfile->list(
			"Will consider a fixed budget for sequencing "
					+ toString(budgetNumIndivs) + " individuals at "
					+ toString(budgetMeanDepth) + "x.");
	meanDepth = Parameters.getParameterDoubleWithDefault("meanDepth", 20);
	n = computeNumIndiv(budgetNumIndivs, budgetMeanDepth, meanDepth);
	logfile->list(
			"Will simulate " + toString(n)
					+ " individuals with a mean depth of " + toString(meanDepth)
					+ "x.");
	gt_error = Parameters.getParameterDoubleWithDefault("error", 0.05);
	logfile->list(
			"Will use a per allele genotyping error rate of "
					+ toString(gt_error) + ".");

	// output
	prefix = Parameters.getParameterStringWithDefault("outname", "simulations");
	logfile->list("Will write output files with tag '" + prefix + "'.");

	logfile->endIndent();
}

void TSimulator::readBangolinSimulParameters(TParameters & Parameters) {
	logfile->startIndent(
			"Reading simulation parameters for bangolin simulation:");
	// loci
	L = Parameters.getParameterIntWithDefault("numLoci", 10000);
	logfile->list("Number of loci: " + toString(L));

	// environmental variables
	D = Parameters.getParameterIntWithDefault("numEnvVar", 7);
	logfile->list("Number of environmental variables: " + toString(D));

	// latent factors
	K = Parameters.getParameterIntWithDefault("numLatFac", 2);
	logfile->list("Number of latent factors: " + toString(K));

	// hyperparameters
	pi = Parameters.getParameterDoubleWithDefault("pi", 0.01);
	lambda_m = Parameters.getParameterDoubleWithDefault("lambda_m", 1);
	alpha_f = Parameters.getParameterDoubleWithDefault("alpha_f", 0.7);
	beta_f = Parameters.getParameterDoubleWithDefault("beta_f", 0.7);
	logfile->list("Value for pi: " + toString(pi));
	logfile->list("Value for lambda_m: " + toString(lambda_m));
	logfile->list("Value for alpha_f: " + toString(alpha_f));
	logfile->list("Value for beta_f: " + toString(beta_f));

	// beta's
	varianceX = Parameters.getParameterDoubleWithDefault("varianceX", 1.0); // variance of environmental variables
	logfile->list("Will simulate X with a variance of " + toString(varianceX));
	fixedVarExplBeta = Parameters.getParameterDoubleWithDefault(
			"fixedVarExplBeta", -1.0); // fixed variance explained
	if (fixedVarExplBeta >= 0) {
		boolFixVarExplBeta = true;
		logfile->list(
				"Will simulate with a fixed explained variance of "
						+ toString(fixedVarExplBeta));
	}

	varianceU = Parameters.getParameterDoubleWithDefault("varianceU", 1.0); // fixed variance of variance-covariance matrix
	varianceV = Parameters.getParameterDoubleWithDefault("varianceV", 1.0); // fixed variance of variance-covariance matrix
	logfile->list("Will simulate U with a variance of " + toString(varianceU));
	logfile->list("Will simulate V with a variance of " + toString(varianceV));

	// output
	if (Parameters.getParameterStringWithDefault("writeSimParamFiles", "false")
			== "true") {
		boolWriteSimParamFiles = true;
		logfile->list(
				"Will write the values of all simulation parameters to file.");
	}
	logfile->endIndent();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Simulate 						                                                            //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TSimulator::runSimulations(TParameters& Parameters) {
	readingCommonParameters(Parameters);

	if (Parameters.getParameterStringWithDefault("genoFromFile", "false")
			== "true") {
		// read genotypes from another simulator and add gentoype uncertainty only
		readGenoFromFile = true;
		logfile->list("Will read simulated genotypes from file.");
		simulateGenoFromFile(Parameters);
	} else {
		logfile->list("Will simulate genotypes according to bangolin model.");
		simulateGenoFromBangolinModel(Parameters);
	}
}

void TSimulator::simulateGenoFromFile(TParameters & Parameters) {
	if (Parameters.parameterExists("csv")) {
		// read csv
		TCsvReader csvReader(logfile);
		csvReader.readCsv(Parameters, Genotypes, Xs, n, D, L);
	} else if (Parameters.parameterExists("fstat")) {
		// read fstat
		TFstatReader fstatReader(logfile);
		fstatReader.readFstat(Parameters, Genotypes, Xs, sampleNames, n, D, L);
	} else
		throw "Unknown file format!";

	// write vcf and env file
	writeFiles();
}

void TSimulator::simulateGenoFromBangolinModel(TParameters& Parameters) {
	// read rest of parameters
	readBangolinSimulParameters(Parameters);
	// simulate parameters and genotypes
	simulateAllParameters(Parameters);
	// write to file
	writeFiles();
}

void TSimulator::simulateAllParameters(TParameters& Parameters) {
	simulateEnvMeasurements(); // environment

	simulateAllUVParams(Parameters); // uv
	simulateMus(); // mu

	simulateBetaParams(Parameters); // beta's

	simulateTrueGenotypes(); // genotypes
}

void TSimulator::simulateBetaParams(TParameters & Parameters) {
	Betas.zeros(D, L);
	betaIsNonZero.zeros(D, L);

	// 	are there any beta parameters to simulate?
	if (Parameters.getParameterStringWithDefault("withoutBX", "false")
			== "true") { // without env. selection
		logfile->list("Will set beta_l * x_i to zero.");
		for (int l = 0; l < L; l++)
			gammas.push_back(0);
		M.zeros(D, D);
		return;
	}

	// gammas: randomly distributed in genome or the last pi% ?
	if (Parameters.getParameterStringWithDefault("randomGamma", "true")
			== "true") // simulate randomly distributed Gamma's
		fillRandomGammas();
	else
		fillFixedGammas();

	// betas: fixed variance explained or random?
	if (boolFixVarExplBeta)
		simulateFixedVarExplBeta(Parameters);
	else
		simulateSigmaBetaAndBeta(Parameters);
}

void TSimulator::simulateFixedVarExplBeta(TParameters& Parameters) {
	logfile->startIndent(
			"Simulating beta's with a fixed variance explained of "
					+ toString(fixedVarExplBeta) + "...");
	// initialize variance explained
	varianceExplainedBeta = new TVarianceExplainedBeta(varianceU, K,
			randomGenerator, Parameters, logfile, varianceX, D);

	// find betas that fulfill variance explained condition, break after 10^5 iterations

	// when to stop?
	double error = 0.001;
	int maxNumIter = 10000;

	// go over rest of loci
	int counter = 0;
	for (std::vector<int>::iterator it = gammaIsOne.begin();
			it < gammaIsOne.end(); it++, counter++) {
		logfile->listFlush(
				"Sampling betas for locus " + toString(*it) + " ...");
		int iter = 0;
		arma::vec beta_l(D);
		double stdDevBeta = 1.0;

		gammas.push_back(1);
		proposeNewBetas(beta_l, stdDevBeta);
		arma::vec v_l = Vs.col(*it);
		double varExpl = varianceExplainedBeta->fillVarExplForOneLocus(mus[*it],
				beta_l, v_l);
		double sumVarExpl = 0;
		while (fabs(varExpl - fixedVarExplBeta) > error && iter <= maxNumIter) {
			if (iter % 100 == 0 && iter != 0) { // every 100'th iteration: adjust proposal stdDev of Beta
				sumVarExpl /= 100.0;
				if (sumVarExpl > fixedVarExplBeta)
					stdDevBeta -= stdDevBeta / 4.0;
				else
					stdDevBeta += stdDevBeta / 4.0;
				sumVarExpl = 0;
			}
			if (iter % 1000 == 0 && iter != 0) { // if it still doesn't work: tweak mu
				mus[*it] -= mus[*it] / 4.0; // mu is too big -> make it to be closer to zero
				sumVarExpl = 0;
				stdDevBeta = 1;
			}
			proposeNewBetas(beta_l, stdDevBeta);
			varExpl = varianceExplainedBeta->fillVarExplForOneLocus(mus[*it],
					beta_l, v_l);
			sumVarExpl += varExpl;
			iter++;
		}
		if (iter > maxNumIter)
			throw "Could not simulate any meaningful beta for locus "
					+ toString(*it) + "!";
		else
			Betas.col(*it) = beta_l;
		betaIsNonZero.col(counter) = Betas.col(*it);

		logfile->done();
	}
	logfile->endIndent();
}

void TSimulator::proposeNewBetas(arma::vec & betas, double stdDevBeta) {
	for (int d = 0; d < D; d++) {
		betas[d] = randomGenerator->getNormalRandom(0, stdDevBeta);
	}
}

void TSimulator::simulateSigmaBetaAndBeta(TParameters & Parameters) {
	if (Parameters.parameterExists("fixedVarBeta")) { // fixed variance of variance-covariance matrix
		double fixedVarBeta = Parameters.getParameterDoubleWithDefault(
				"fixedVarBeta", 1.0);
		double fixedCovarBeta = Parameters.getParameterDoubleWithDefault(
				"fixedCovarBeta", fixedVarBeta / 10.0);
		logfile->list(
				"Will use a fixed variance for beta of "
						+ toString(fixedVarBeta) + " and a fixed covariance of "
						+ toString(fixedCovarBeta));
		fillFixedSigmaBeta(fixedVarBeta, fixedCovarBeta);
	} else
		fillSigmaBeta();
	fillBetas();
}

void TSimulator::simulateAllUVParams(TParameters& Parameters) {
	// get matrices with U's and V's
	Us.set_size(n, K);
	Vs.set_size(K, L);
	if (Parameters.getParameterStringWithDefault("withoutUV", "false")
			== "true") { // without population structure)
		logfile->list("Set u_i * v_l to zero.");
		Us.zeros();
		Vs.zeros();
	} else {
		fillUs();
		fillVs();
	}
	if (boolWriteSimParamFiles) {
		varianceExplainedUV = new TVarianceExplainedUV(varianceU, K,
				randomGenerator, Parameters, logfile, varianceV, alpha_f,
				beta_f);
	}
}

void TSimulator::fillRandomGammas() {
	for (int l = 0; l < L; ++l) {
		gammas.push_back(randomGenerator->getBiomialRand(pi, 1));
		if (gammas[l] == 1)
			gammaIsOne.push_back(l);
	}
}

void TSimulator::fillFixedGammas() {
	for (int l = 0; l < L; l++) {
		if (l < ((1 - pi) * L))
			gammas.push_back(0); // fill first (1-pi)% of loci with 0
		else {
			gammas.push_back(1); // fill last pi% of loci with 1
			gammaIsOne.push_back(l);
		}
	}
}

void TSimulator::fillSigmaBeta() {
	M.set_size(D, D);
	M.zeros();
	m = randomGenerator->getExponentialRandom(lambda_m);

	// fill lower triangular matrix M according to prior distributions
	for (int s = 0; s < D; s++) {
		for (int r = s; r < D; r++) {
			if (r == s)
				M(r, s) = m * randomGenerator->getChisqRand(D - r);
			else
				M(r, s) = m * randomGenerator->getNormalRandom(0, 1);
		}
	}
	SigmaBeta = inv(M * M.t()); // perform matrix multiplication and take inverse
}

void TSimulator::fillFixedSigmaBeta(double fixedVarBeta,
		double fixedCovarBeta) {
	SigmaBeta.set_size(D, D);
	for (int s = 0; s < D; s++) {
		for (int r = 0; r < D; r++) {
			if (s == r) {
				SigmaBeta(r, s) = fixedVarBeta;
			} else
				SigmaBeta(r, s) = fixedCovarBeta;
		}
	}
	m = 1;
	M = chol(inv(SigmaBeta), "lower");
}

void TSimulator::fillBetas() {
	/* according to Numerical Recipes 3rd edition, pp.378: construct a vector deviate x with
	 * covariance Sigma and mean mu, starting with a vector y of independent random deviates
	 * of zero mean and unit variance. First, use Cholesky decomposition to factor Sigma
	 * into a left triangular matrix L times its transpose:
	 * Sigma = LL^T
	 * Next, whenever you want a new deviate x, fill y with independent deviates of unit
	 * variance and then construct
	 * betas = Ly + mu (where mu = 0 in our case)
	 */

	// get variance-covariance matrix SigmaBeta
	arma::mat LMat(D, D);
	LMat = chol(SigmaBeta, "lower");

	// generate matrix y and fill it with normal deviates (only if corresponding gamma is 1).
	arma::mat y(D, L, arma::fill::zeros);
	for (int dRow = 0; dRow < D; dRow++) {
		for (int lCol = 0; lCol < L; lCol++) {
			if (gammas[lCol] == 1)
				y(dRow, lCol) = randomGenerator->getNormalRandom(0, 1);
		}
	}
	// generate deviates Betas (matrix multiplication)
	Betas = LMat * y;

	// fill vector of betas where gamma is one
	int numGammaIsOne = 0;
	for (int l = 0; l < L; l++) {
		if (gammas[l] == 1) {
			betaIsNonZero.col(numGammaIsOne) = Betas.col(l);
			numGammaIsOne++;
		}
	}
}

void TSimulator::simulateMus() {
	for (int l = 0; l < L; ++l)
		mus.push_back(logit(randomGenerator->getBetaRandom(alpha_f, beta_f)));
}

void TSimulator::fillUs() {
	for (int iRow = 0; iRow < n; iRow++) {
		for (int kCol = 0; kCol < K; kCol++)
			Us(iRow, kCol) = randomGenerator->getNormalRandom(0,
					sqrt(varianceU));
	}
}

void TSimulator::fillVs() {
	for (int kRow = 0; kRow < K; kRow++) {
		for (int lCol = 0; lCol < L; lCol++)
			Vs(kRow, lCol) = randomGenerator->getNormalRandom(0,
					sqrt(varianceV));
	}
}

void TSimulator::simulateTrueGenotypes() {
	double tilde_f_il;
	Genotypes.set_size(n, L);
	Fil.set_size(n, L);

	for (int iRow = 0; iRow < n; iRow++) {
		for (int lCol = 0; lCol < L; lCol++) {
			// get allele frequency by logistic regression
			tilde_f_il = logistic(
					mus[lCol] + dot(Betas.col(lCol), Xs.row(iRow))
							+ dot(Us.row(iRow), Vs.col(lCol)));
			Fil(iRow, lCol) = tilde_f_il;

			// draw genotypes from the allele frequencies
			Genotypes(iRow, lCol) = randomGenerator->getBiomialRand(tilde_f_il,
					2);
		}
	}
}

void TSimulator::simulateEnvMeasurements() {
	Xs.set_size(n, D);
	for (int iRow = 0; iRow < n; iRow++) {
		for (int dCol = 0; dCol < D; dCol++)
			Xs(iRow, dCol) = randomGenerator->getNormalRandom(0,
					sqrt(varianceX));
	}
}

void TSimulator::createSampleNames() {
	if (sampleNames.empty()) { // in case sampleNames has not been filled before: fill it
		for (int i = 0; i < n; ++i)
			sampleNames.push_back("Sample_" + toString(i));
	}
}

void TSimulator::simulateAndWriteObsGenotype(int trueGenotype) {
	int depth = pickDepth();
	double gtl[3];
	fillGenotypeLikelihood(depth, trueGenotype, gtl);
	writePhredGTLToVCF(gtl, depth);
}

int TSimulator::pickDepth() {
	return randomGenerator->getPoissonRandom(meanDepth);
}

void TSimulator::fillGenotypeLikelihood(int depth, int trueGenotype,
		double * gtl) {
	int numRef = countNumRefReads(depth, trueGenotype);
	int numAlt = depth - numRef;

	gtl[0] = pow(1 - gt_error, numRef) * pow(gt_error, numAlt);
	gtl[1] = pow(0.5, numRef + numAlt);
	gtl[2] = pow(gt_error, numRef) * pow(1 - gt_error, numAlt);
}

int TSimulator::countNumRefReads(double depth, int trueGenotype) {
	int numRef = 0; // binomial random variable
	if (trueGenotype == 0)
		numRef = randomGenerator->getBiomialRand(1 - gt_error, depth);
	else if (trueGenotype == 1)
		numRef = randomGenerator->getBiomialRand(0.5, depth);
	else
		numRef = randomGenerator->getBiomialRand(gt_error, depth);
	return numRef;
}

void TSimulator::writePhredGTLToVCF(double * gtl, int depth) {
	// find genotype with largest likelihood -> this is the observed genotype!
	int observedGenotype;
	double gtlObservedGenotype = 0;
	for (int genotype = 0; genotype < 3; genotype++) {
		if (gtl[genotype] > gtlObservedGenotype) {
			observedGenotype = genotype;
			gtlObservedGenotype = gtl[genotype];
		}
	}
	// find genotype with second largest likelihood (needed for genotype quality)
	int secondBestGenotype;
	double gtlSecondBestGenotype = 0;
	for (int genotype = 0; genotype < 3; genotype++) {
		if (gtl[genotype] > gtlSecondBestGenotype) {
			if (gtl[genotype] == gtlObservedGenotype) {
				continue;
			} else {
				secondBestGenotype = genotype;
				gtlSecondBestGenotype = gtl[genotype];
			}
		}
	}

	// convert to phred
	int phredgtl[3];
	for (int genotype = 0; genotype < 3; genotype++) {
		phredgtl[genotype] = std::round(phred(gtl[genotype]));
	}

	// find min
	double min = phredgtl[0];
	if (phredgtl[1] < min)
		min = phredgtl[1];
	if (phredgtl[2] < min)
		min = phredgtl[2];

	// normalize
	for (int genotype = 0; genotype < 3; genotype++) {
		phredgtl[genotype] -= min;
	}
	// write to vcf
	vcf.writeObservedGenotype(observedGenotype, secondBestGenotype, phredgtl,
			depth);
}

double TSimulator::computeDepth(int n1, int n2, double s1) {
	long unsigned int numBpFlowCell = 2700 * pow(10, 9); // (source: Daniel, NovaSeq S4)
	int costFlowCell = 30000; // (source: Daniel, NovaSeq S4, in dollar)
	long unsigned int genomeSize = 3.1 * pow(10, 9); // example mammal genome (bp)
	int costLibrary = 10; // 1 library per sample

	double costFlowCellPerBp = (float) costFlowCell / (float) numBpFlowCell;
	double a = (float) costLibrary / (float) (costFlowCellPerBp * genomeSize); // 1 library costs 1/3 as 1x depth

	double depth = ((float) n1 / (float) n2) * (s1 + a) - a;
	return depth;
}

int TSimulator::computeNumIndiv(int n1, double s1, double s2) {
	long unsigned int numBpFlowCell = 2700 * pow(10, 9); // (source: Daniel, NovaSeq S4)
	int costFlowCell = 30000; // (source: Daniel, NovaSeq S4, in dollar)
	long unsigned int genomeSize = 3.1 * pow(10, 9); // example mammal genome (bp)
	int costLibrary = 10; // 1 library per sample

	double costFlowCellPerBp = (float) costFlowCell / (float) numBpFlowCell;
	double a = (float) costLibrary / (float) (costFlowCellPerBp * genomeSize); // 1 library costs 1/3 as 1x depth
	int n2 = std::floor((n1 * (s1 + a)) / (s2 + a));
	return n2;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Files		                                                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////

void TSimulator::writeFiles() {
	createSampleNames();

	writeToVcfFile();
	writeToEnvFile(Xs);
	if (!readGenoFromFile) {
		writeBetaGammaFile();
		if (boolWriteSimParamFiles)
			writeAllSimParamFiles();
	}
}

void TSimulator::writeAllSimParamFiles() {
	writeFileUiVl();
	writeFileMul();
	writeToHyperFile();
	writeFileFil();
	writeFileVarExplUV();
	if (boolFixVarExplBeta)
		writeVarExplToFile();
}

void TSimulator::writeToVcfFile() {
	openVCF();
	logfile->listFlush("Writing vcf-file...");

	// header
	vcf.writeHeader(sampleNames);

	for (long l = 0; l < L; ++l) { // loop over loci
		vcf.newSite();
		for (long i = 0; i < n; ++i) // loop over individuals
			simulateAndWriteObsGenotype(Genotypes(i, l));
	}
	logfile->done();
}

void TSimulator::openVCF() {
	std::string filename = prefix + ".vcf.gz";
	if (std::ifstream(filename)) // if file already exists, delete it
		remove(filename.c_str());
	vcf.openVCF(filename, logfile);
}
;

void TSimulator::getHeaderLoci(std::vector<std::string> & header) {
	for (long l = 0; l < L; l++) {
		header.push_back("1_" + toString(l));
	}
}

void TSimulator::writeToHyperFile() {
	TOutputFilePlain out(prefix + "_hyperparams_simulated.txt");

	// header
	std::vector<std::string> header = { "pi", "m", "alphaF", "betaF" };
	if (!boolFixVarExplBeta) {
		for (int s = 0; s < D; s++) {
			for (int r = s; r < D; r++) {
				header.push_back("m" + toString(r) + toString(s));
			}
		}
	}
	out.writeHeader(header);

	// data
	out << pi << m << alpha_f << beta_f;
	if (!boolFixVarExplBeta) {
		for (int s = 0; s < D; s++) {
			for (int r = s; r < D; r++)
				out << M(r, s);
		}
	}
	out.close();
}

void TSimulator::writeFileUiVl() {
	TOutputFilePlain out(prefix + "_uv_simulated.txt");

	// header
	std::vector<std::string> header { "" };
	getHeaderLoci(header);
	out.writeHeader(header);

	// data
	double sumUiVl;
	int i = 0;
	for (std::vector<std::string>::iterator sample = sampleNames.begin();
			sample != sampleNames.end(); ++sample, i++) {
		out << *sample;
		for (long l = 0; l < L; l++) {
			sumUiVl = 0;
			for (int k = 0; k < K; k++) {
				sumUiVl += Us(i, k) * Vs(k, l);
			}
			out << sumUiVl;
		}
		out.endLine();
	}
	out.close();
}

void TSimulator::writeFileMul() {
	TOutputFilePlain out(prefix + "_mus_simulated.txt");

	// header
	std::vector<std::string> header;
	getHeaderLoci(header);
	out.writeHeader(header);

	// data
	for (std::vector<double>::iterator mu = mus.begin(); mu < mus.end(); mu++)
		out << *mu;
	out.close();
}

void TSimulator::writeBetaGammaFile() {
	if (gammaIsOne.empty())
		return;

	TOutputFilePlain out(prefix + "_beta_gamma_simulated.txt");

	// header
	std::vector<std::string> header;
	for (std::vector<int>::iterator gamma = gammaIsOne.begin();
			gamma < gammaIsOne.end(); gamma++)
		header.push_back("1_" + toString(*gamma));
	out.writeHeader(header);

	// data
	for (int d = 0; d < D; d++) {
		for (unsigned int l = 0; l < gammaIsOne.size(); l++) {
			out << betaIsNonZero(d, l);
		}
		out.endLine();
	}
	out.close();
}

void TSimulator::writeFileFil() {
	TOutputFilePlain out(prefix + "_f_il_simulated.txt");

	// header
	std::vector<std::string> header { "" };
	getHeaderLoci(header);
	out.writeHeader(header);

	// data
	for (int i = 0; i < n; i++) {
		out << sampleNames[i];
		for (long l = 0; l < L; l++)
			out << Fil(i, l);
		out.endLine();
	}
	out.close();
}

void TSimulator::writeVarExplToFile() {
	TOutputFilePlain out(prefix + "_varExplBeta_simulated.txt");

	// header
	std::vector<std::string> header;
	getHeaderLoci(header);
	out.writeHeader(header);

	// data
	for (long l = 0; l < L; l++) {
		arma::vec beta_l = Betas.col(l);
		arma::vec v_l = Vs.col(l);
		out
				<< varianceExplainedBeta->fillVarExplForOneLocus(mus[l], beta_l,
						v_l);
	}
	out.close();
}

void TSimulator::writeFileVarExplUV() {
	TOutputFilePlain out(prefix + "_varExplUV_simulated.txt");

	// header
	out.noHeader(1);
	// data
	out << varianceExplainedUV->compute_VarExpl();

	out.close();
}

void TSimulator::writeToEnvFile(arma::mat envMeasurements) {
	logfile->listFlush("Writing env-file...");
	TOutputFilePlain out(prefix + ".env");

	// header
	std::vector<std::string> envVarNames { "" };
	for (int i = 0; i < D; ++i)
		envVarNames.push_back("Variable_" + toString(i));
	out.writeHeader(envVarNames);

	//data
	int rowNum = 0;
	for (std::vector<std::string>::iterator sample = sampleNames.begin();
			sample != sampleNames.end(); ++sample, rowNum++) {
		out << *sample;
		for (int d = 0; d < D; d++)
			out << envMeasurements(rowNum, d);
		out.endLine();
	}
	logfile->done();
}
;

void TSimulator::clean() {
	if (varianceExplainedBeta != nullptr)
		delete varianceExplainedBeta;
	if (varianceExplainedUV != nullptr)
		delete varianceExplainedUV;

	vcf.closeVcf();
}
;

//////////////////////////////////////////////////////////////////////////////////////////////////
// compute variance explained           														//
//////////////////////////////////////////////////////////////////////////////////////////////////

TVarianceExplained::TVarianceExplained(double varU, int numLatFac,
		TRandomGenerator * rg, TParameters& Parameters, TLog * Logfile) {
	varianceU = varU;
	K = numLatFac;

	randomGenerator = rg;
	logfile = Logfile;

	NInner = Parameters.getParameterIntWithDefault("NQuasiMC_Inner", 500);
	NOuter = Parameters.getParameterIntWithDefault("NQuasiMC_Outer", 1000);

	gridPointsInner = new double[NInner];
	gridPointsOuter = new double[NOuter];
}

void TVarianceExplained::initializeGridPointsNormalDistrQuasiMC(
		double * gridPoints, int N, double mu, double sigma2) {
	double h = 1.0 / (float) N;
	double quantile = h / 2.0;
	for (int n = 0; n < N; n++) {
		gridPoints[n] =
				randomGenerator->normalInvCumulativeDistributionFunction(
						quantile, mu, sqrt(sigma2));
		quantile += h;
	}
}

void TVarianceExplained::quasiMonteCarloOfF(double mu, double sigma2,
		double & E_F, double & E_F2) {
	double f = 0;
	double f2 = 0;
	for (int n = 0; n < NInner; n++) {
		double tmp = logistic(mu + sqrt(sigma2) * gridPointsInner[n]);
		f += tmp;
		f2 += tmp * tmp;
	}
	E_F = (1.0 / (float) NInner) * f; // changes E_F and E_F2 internally
	E_F2 = (1.0 / (float) NInner) * f2;
}

void TVarianceExplained::clean() {
	delete[] gridPointsOuter;
	delete[] gridPointsInner;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// compute variance explained of beta    														//
//////////////////////////////////////////////////////////////////////////////////////////////////

TVarianceExplainedBeta::TVarianceExplainedBeta(double varU, double numLatFac,
		TRandomGenerator * RandomGenerator, TParameters& Parameters,
		TLog * Logfile, double varX, int numEnvVar) :
		TVarianceExplained(varU, numLatFac, RandomGenerator, Parameters,
				Logfile) {
	varianceX = varX;
	D = numEnvVar;

	gridPointsThisZ = new double[NOuter];

	initializeGridPointsNormalDistrQuasiMC(gridPointsInner, NInner, 0, 1);
	initializeGridPointsNormalDistrQuasiMC(gridPointsOuter, NOuter, 0, 1); // initialize once with sd = 1, then just multiply with sigma_z
}

double TVarianceExplainedBeta::fillVarExplForOneLocus(double & mu_l,
		arma::vec & beta_l, arma::vec & v_l) {
	// compute sigma2_z and sigma2_yGivenZ
	double sigma2_z = computeSigma2_z(beta_l);
	double sigma2_yGivenZ = computeSigma2_yGivenZ(v_l);
	// initialize grid points for quasi MC of z
	for (int n = 0; n < NOuter; n++) {
		gridPointsThisZ[n] = gridPointsOuter[n] * sqrt(sigma2_z);
	}

	// compute variance explained
	return compute_VarExpl(mu_l, sigma2_yGivenZ);
}

double TVarianceExplainedBeta::computeSigma2_z(arma::vec & beta_l) {
	double sigma2_z = 0;
	for (int d = 0; d < D; d++)
		sigma2_z += beta_l[d] * beta_l[d];
	sigma2_z *= varianceX;
	return sigma2_z;
}

double TVarianceExplainedBeta::computeSigma2_yGivenZ(arma::vec v_l) {
	double sigma2_yGivenZ = 0;
	for (int k = 0; k < K; k++)
		sigma2_yGivenZ += v_l[k] * v_l[k];
	sigma2_yGivenZ *= varianceU;
	return sigma2_yGivenZ;
}

double TVarianceExplainedBeta::compute_VarExpl(double mu_0,
		double sigma2_yGivenZ) {
	// initialize
	double E_FGivenZ;
	double E_F2GivenZ;
	double E_GGivenZ_sum = 0;
	double E_GGivenZ2_sum = 0;
	double sum_var_GGivenZ = 0;

	// go over all z
	for (int n = 0; n < NOuter; n++) {
		// compute var(G|Z) (by integrating over all f)
		double mu_yGivenZ = gridPointsThisZ[n] + mu_0;
		quasiMonteCarloOfF(mu_yGivenZ, sigma2_yGivenZ, E_FGivenZ, E_F2GivenZ);
		double varGGivenZ = 2 * E_FGivenZ * (1 - 2 * E_FGivenZ)
				+ 2 * E_F2GivenZ; // return variance var(G|Z[n])
		sum_var_GGivenZ += varGGivenZ;

		// sum E_FGivenZ
		E_GGivenZ_sum += 2 * E_FGivenZ;
		E_GGivenZ2_sum += 4 * E_FGivenZ * E_FGivenZ;
	}

	// integrate over all z (quasi Monte Carlo over z) to compute E(var(G|Z))
	double E_Var_GGivenZ = (1.0 / (float) NOuter) * sum_var_GGivenZ;

	// compute empirical variance of E(G|Z)
	double tmp = (1.0 / (float) NOuter) * E_GGivenZ_sum;
	double var_E_GGivenZ = (1.0 / (float) NOuter) * E_GGivenZ2_sum - tmp * tmp;

	// compute var(G)
	double var_G = E_Var_GGivenZ + var_E_GGivenZ;

	// compute variance explained
	if (fabs(var_G - E_Var_GGivenZ) < pow(10, -10))
		return 0; // if var_G should be equal to var_E_GGivenZ, but isn't due to rounding errors: impute 0
	else
		return 1 - E_Var_GGivenZ / var_G;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// HMM																						    //
//////////////////////////////////////////////////////////////////////////////////////////////////

TSimulatorHMM::TSimulatorHMM(TLog* Logfile, TRandomGenerator * RandomGenerator) :
		TSimulator(Logfile, RandomGenerator), THMM() // initializes tau, lambda1, lambda2, stationary distribution
{
}

void TSimulatorHMM::simulateDistances() {
	// simulate loci on 5 chromosomes
	int numChrom = 5;
	int numLociPerChr = L / numChrom;

	int chr_previous_l = 0;
	int chr_this_l = 0;
	unsigned int pos_previous_l = 1;
	for (int l = 0; l < L; l++) {
		if (l % numLociPerChr == 0) { // new chromosome
			chr_this_l += 1;
			pos_previous_l = 1;
		}
		// get a random position (from exponential distribution)
		unsigned int pos_this_l = pos_previous_l
				+ static_cast<int>(randomGenerator->getExponentialRandom(1))
				+ 1; // + 1 because position 0 is not valid
		// enter distance
		enterOneDelta_l(toString(chr_previous_l), toString(chr_this_l),
				pos_previous_l, pos_this_l);
		// update previous
		pos_previous_l = pos_this_l;
		chr_previous_l = chr_this_l;
	}
}

void TSimulatorHMM::runSimulations(TParameters& Parameters) {
	// read parameters
	readingCommonParameters(Parameters);
	readBangolinSimulParameters(Parameters);
	double lambda_1 = Parameters.getParameterDoubleWithDefault("lambda_1",
			0.05);
	double lambda_2 = Parameters.getParameterDoubleWithDefault("lambda_2", 8.);
	logfile->list(
			"Value for lambda_1: " + toString(lambda_1) + " and lambda_2: "
					+ toString(lambda_2));
	setLambda(lambda_1, lambda_2);

	// simulate
	setNumLoci(L);
	simulateDistances();
	simulateAllParameters(Parameters);

	// write to file
	writeFiles();
}

void TSimulatorHMM::simulateAllParameters(TParameters& Parameters) {
	simulateEnvMeasurements(); // environment

	simulateAllUVParams(Parameters); // uv
	simulateMus(); // mu

	simulateBetaParams(Parameters); // beta's

	simulateTrueGenotypes(); // genotypes
}

void TSimulatorHMM::simulateBetaParams(TParameters & Parameters) {
	Betas.zeros(D, L);
	betaIsNonZero.zeros(D, L);

	// 	are there any beta parameters to simulate?
	if (Parameters.getParameterStringWithDefault("withoutBX", "false")
			== "true") { // without env. selection
		logfile->list("Will set beta_l * x_i to zero.");
		for (int l = 0; l < L; l++)
			gammas.push_back(0);
		M.zeros(D, D);
		return;
	}

	// gammas: randomly distributed in genome or the last pi% ?
	fillLinkedGammas();

	// betas: fixed variance explained or random?
	if (boolFixVarExplBeta)
		simulateFixedVarExplBeta(Parameters);
	else
		simulateSigmaBetaAndBeta(Parameters);
}

void TSimulatorHMM::fillLinkedGammas() {
	for (int l = 0; l < L; ++l) {
		double prob;
		if (l == 0)
			prob = getTau(l, 0, 1);
		else
			prob = getTau(l, gammas[l - 1], 1);
		gammas.push_back(randomGenerator->getBiomialRand(prob, 1));
		if (gammas[l] == 1)
			gammaIsOne.push_back(l);
	}
	logfile->list(
			"Simulated " + toString(gammaIsOne.size())
					+ " loci under selection.");
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// compute variance explained of uv    															//
//////////////////////////////////////////////////////////////////////////////////////////////////

TVarianceExplainedUV::TVarianceExplainedUV(double varU, double numLatFac,
		TRandomGenerator * RandomGenerator, TParameters& Parameters,
		TLog * Logfile, double varV, double alpha_f, double beta_f) :
		TVarianceExplained(varU, numLatFac, RandomGenerator, Parameters,
				Logfile) {
	varianceV = varV;
	sigma2_y = numLatFac * varU * varV;

	initializeGridPointsNormalDistrQuasiMC(gridPointsInner, NInner, 0, 1);
	initializeGridPointsBetaDistrQuasiMC(gridPointsOuter, NOuter, alpha_f,
			beta_f);
}

void TVarianceExplainedUV::initializeGridPointsBetaDistrQuasiMC(
		double * gridPoints, int N, double alpha, double beta) {
	double h = 1.0 / (float) N;
	double quantile = h / 2.0;
	for (int n = 0; n < N; n++) {
		gridPoints[n] = randomGenerator->inverseIncompleteBeta(quantile, alpha,
				beta);
		quantile += h;
	}
}

double TVarianceExplainedUV::compute_VarExpl() {
	// initialize
	double E_F;
	double E_F2;
	double sum_varExpl = 0;

	for (int n = 0; n < NOuter; n++) { // integrate over F_0
		double mu = logit(gridPointsOuter[n]);
		quasiMonteCarloOfF(mu, sigma2_y, E_F, E_F2); // integrate over F
		double E_var_GGivenF = 2 * (E_F - E_F2);
		double var_E_GGivenF = 4 * (E_F2 - E_F * E_F);
		double var_G = E_var_GGivenF + var_E_GGivenF;
		sum_varExpl += 1 - (E_var_GGivenF / var_G);
	}
	return (1.0 / (float) NOuter) * sum_varExpl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// read csv files (output of simuPOP)  															//
//////////////////////////////////////////////////////////////////////////////////////////////////

TFileReader::TFileReader(TLog * Logfile) {
	logfile = Logfile;
}

void TFileReader::openFile(std::ifstream & file, TParameters & parameters,
		std::string flag) {
	//open input stream
	std::string filename = parameters.getParameterString(flag);
	logfile->startIndent("Reading file '" + filename + "'...");
	file.open(filename.c_str());
	if (!file)
		throw "Failed to open file '" + filename + "!";
}

TCsvReader::TCsvReader(TLog * Logfile) :
		TFileReader(Logfile), logfile(Logfile) {
}

void TCsvReader::parseLineOfCsv(std::vector<std::string> & vec,
		arma::umat & Genotypes, std::vector<double> & tmpEnvMeasurements,
		const int & curIndividual) {
	// get second element of vector (= environmental measurements for this individual)
	tmpEnvMeasurements.push_back(stringToDouble(vec[1]));

	// transform the vector of strings to a vector of ints
	std::vector<int> intVec;
	std::transform(vec.begin() + 2, vec.end(), std::back_inserter(intVec),
			stringToInt);

	// add vector to matrix X
	Genotypes.insert_rows(curIndividual,
			arma::conv_to<arma::urowvec>::from(intVec));
}

void TCsvReader::readCsv(TParameters & parameters, arma::umat & Genotypes,
		arma::mat & envMeasurements, int & n, int & D, int & L) {
	// open input file
	std::ifstream csvFile;
	openFile(csvFile, parameters, "csv");

	//parse file
	n = 0;
	std::vector<std::string> thisLine;
	std::vector<double> tmpEnvMeasurements;

	while (csvFile.good() && !csvFile.eof()) {
		fillVectorFromLineAny(csvFile, thisLine, ",");
		if (!thisLine.empty()) {
			parseLineOfCsv(thisLine, Genotypes, tmpEnvMeasurements, n);
			++n;
		}
	}

	// add vector of environmental measurements to armadillo matrix
	envMeasurements = arma::conv_to<arma::mat>::from(tmpEnvMeasurements);
	L = Genotypes.n_cols;
	D = envMeasurements.n_cols;

	logfile->list(
			"Detected a total number of " + toString(n)
					+ " individuals in csv-file.");
	logfile->list(
			"Detected a total number of " + toString(L) + " loci in csv-file.");
	logfile->list(
			"Detected a total number of " + toString(D)
					+ " environmental variables in csv-file.");

	logfile->endIndent();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// read FSTAT files (output of quantiNemo)														//
//////////////////////////////////////////////////////////////////////////////////////////////////

TFstatReader::TFstatReader(TLog * Logfile) :
		TFileReader(Logfile), logfile(Logfile) {
}

void TFstatReader::parseLineOfFstat(std::vector<std::string> & vec,
		arma::umat & Genotypes, const int & curIndividual,
		std::vector<std::string> & popNames) {
	// first number is the population
	popNames.push_back("pop_" + vec[0]);

	// transform the vector of strings to a vector of ints
	std::vector<int> intVec;
	std::transform(vec.begin() + 1, vec.end(), std::back_inserter(intVec),
			stringToInt);

	// re-code genotypes
	std::vector<int>::iterator first = intVec.begin();
	std::vector<int>::iterator last = intVec.end();

	while (first != last) {
		if (*first == 22)
			*first = 2;
		else if (*first == 12 || *first == 21)
			*first = 1;
		else
			*first = 0;
		++first;
	}

	// add vector to matrix X
	Genotypes.insert_rows(curIndividual,
			arma::conv_to<arma::urowvec>::from(intVec));
}

void TFstatReader::readFstat(TParameters & parameters, arma::umat & Genotypes,
		arma::mat & envMeasurements, std::vector<std::string> & popNames,
		int & n, int & D, int & L) {
	// open input file
	std::ifstream fstatFile;
	openFile(fstatFile, parameters, "fstat");

	std::vector<std::string> thisLine;

	// first: find out how many loci there are (second number on first line)
	fillVectorFromLineAny(fstatFile, thisLine, " ");
	L = stringToUnsignedInt(thisLine[1]);
	n = stringToUnsignedInt(thisLine[0]); // assume that first number represent the number of individuals -> ATTENTION: this
	// is not always the case (it is the number of patches), only applies to custom R script!!
	Genotypes.set_size(n, L);

	//parse file
	n = 0;
	int line = 1;

	while (fstatFile.good() && !fstatFile.eof()) {
		fillVectorFromLineAny(fstatFile, thisLine, " ");
		if (!thisLine.empty() && line > L) { // first L + 1 lines only contain info and loci names
			parseLineOfFstat(thisLine, Genotypes, n, popNames);
			++n;
		}
		++line;
	}

	// Now also read the environmental values from another file
	D = 1;
	TEnvironmentReader envReader(logfile);
	envReader.readEnvPatchFile(parameters, envMeasurements, popNames);

	logfile->list(
			"Detected a total number of " + toString(n)
					+ " individuals in fstat-file.");
	logfile->list(
			"Detected a total number of " + toString(L)
					+ " loci in fstat-file.");

	logfile->endIndent();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// read selection coefficient files (input of quantiNemo)										//
//////////////////////////////////////////////////////////////////////////////////////////////////

TEnvironmentReader::TEnvironmentReader(TLog * Logfile) :
		TFileReader(Logfile), logfile(Logfile) {
}

void TEnvironmentReader::parseLineOfEnvPatchFile(std::vector<std::string> & vec,
		std::vector<double> & envVec,
		std::vector<std::string> & whichPopsFromEnv) {
	whichPopsFromEnv.push_back("pop_" + vec[0]);
	envVec.push_back(stringToDouble(vec[1]));
}

void TEnvironmentReader::readEnvPatchFile(TParameters & parameters,
		arma::mat & envMeasurements,
		std::vector<std::string> & whichPopsFromDat) {
	// open input file
	std::ifstream envPatchFile;
	openFile(envPatchFile, parameters, "envPatchFile");

	std::vector<std::string> thisLine;
	std::vector<double> envVec;
	std::vector<std::string> whichPopsFromEnv;

	// read selection coefficients and transform them to environmental values
	while (envPatchFile.good() && !envPatchFile.eof()) {
		fillVectorFromLineAny(envPatchFile, thisLine, " ");
		if (!thisLine.empty()) {
			parseLineOfEnvPatchFile(thisLine, envVec, whichPopsFromEnv);
		}
	}

	// check if the two population vectors are equal
	if (whichPopsFromDat != whichPopsFromEnv)
		throw "The populations from the dat and from the env file are not identical!";

	// add vector of environmental measurements to armadillo matrix
	envMeasurements = arma::conv_to<arma::mat>::from(envVec);

	logfile->endIndent();
}

