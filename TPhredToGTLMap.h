/*
 * TPhredToGTLMap.h
 *
 *  Created on: Nov 13, 2018
 *      Author: caduffm
 */

#ifndef TPHREDTOGTLMAP_H_
#define TPHREDTOGTLMAP_H_

#include <math.h>

class TPhredToGTLMap {

private:

	inline double phredToGTL(double phred) {
		return pow(10.0, -phred / 10.0);
	}
	;

	double * phredIntToGTLMap;
	int max = 255;
	double gtlAboveMax; //only up to phred = max (e.g. 255), else always return max + 1

public:

	TPhredToGTLMap() {
		// gtl is genotype likelihood (between 0 and 1)
		// phred is phred-scaled gtl as phred = -10 * log10(gtl)
		// phredInt is (int) phred
		phredIntToGTLMap = new double[max];
		for (int phred = 0; phred < max; phred++) {
			phredIntToGTLMap[phred] = phredToGTL(phred);
		}
		gtlAboveMax = phredToGTL(max);
	}
	;

	~TPhredToGTLMap() {
		delete[] phredIntToGTLMap;
	}
	;

	double& operator[](int phredInt) {
		if (phredInt >= max) {
			return gtlAboveMax;
		} else
			return phredIntToGTLMap[phredInt];
	}
	;

	double getLogLikelihood(unsigned short * phredScore, double thisFil) {
		double OneMinThisFil = 1 - thisFil;

		double g = (*this)[phredScore[0]] * OneMinThisFil * OneMinThisFil;
		g += (*this)[phredScore[1]] * 2 * thisFil * OneMinThisFil;
		g += (*this)[phredScore[2]] * thisFil * thisFil;

		return log(g);
	}
	;
};

#endif /* TPHREDTOGTLMAP_H_ */
